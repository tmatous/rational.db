﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rational.DB;

namespace Rational.DB.Tests
{
    [TestClass]
    public class SqlGenerationTests
    {

        [TestMethod]
        public void ParametersWithUnspecifiedDb()
        {
            var target = "SELECT * FROM Test WHERE fld = @0\r\n";
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld", new { fld = "test" });
            var actual = stmt.GetSql();
            Assert.AreEqual(actual, target);
        }


        [TestMethod]
        public void ParametersWithSqlServer()
        {
            var target = "SELECT * FROM Test WHERE fld = @0\r\n";
            var db = new Database(new DbConnectionInfo("", eDbType.SqlServer));
            var stmt = db.CreateStatement<DbStatement>();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld", new { fld = "test" });
            var actual = stmt.GetSql();
            Assert.AreEqual(actual, target);
        }


        [TestMethod]
        public void ParametersWithOracle()
        {
            var target = "SELECT * FROM Test WHERE fld = :0\r\n";
            var db = new Database(new DbConnectionInfo("", eDbType.Oracle));
            var stmt = db.CreateStatement<DbStatement>();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld", new { fld = "test" });
            var actual = stmt.GetSql();
            Assert.AreEqual(actual, target);
        }


        private DbSelectPager CreateSqlPager()
        {
            var db = new Database(new DbConnectionInfo("", eDbType.SqlServer));
            var stmt = db.CreateSelectStatement("Test", "*");
            stmt.OrderBy.Add("fld", SqlFragmentOrderBy.eOrderByDirection.DESC);
            stmt.OrderBy.Add("fld2", SqlFragmentOrderBy.eOrderByDirection.ASC);
            var pager = new DbSelectPager(stmt);
            pager.ResultMax = 5;
            pager.ResultPage = 2;

            return pager;
        }


        [TestMethod]
        public void PagingWithNoOrder()
        {
            try
            {
                var pager = CreateSqlPager();
                pager.SelectStatement.OrderBy.Clear();
                var sql = pager.GetSql();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "ORDER clause required");
            }
        }


        [TestMethod]
        public void PagingWithNoMax()
        {
            try
            {
                var pager = CreateSqlPager();
                pager.ResultMax = 0;
                var sql = pager.GetSql();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Invalid ResultMax");
            }
        }


        [TestMethod]
        public void PagingWithSqlServer()
        {
            var target = @"SELECT
*

FROM 
Test

ORDER BY 
[fld] DESC, [fld2] ASC

OFFSET 5 ROWS FETCH NEXT 5 ROWS ONLY
";
            var pager = CreateSqlPager();
            var actual = pager.GetSql();
            Assert.AreEqual(actual, target);
        }

        [TestMethod]
        public void PagingWithSqlServerOld()
        {
            var target = @"WITH
__PAGING_CTE__ AS (
SELECT
*
, ROW_NUMBER() OVER (ORDER BY [fld] DESC, [fld2] ASC) AS __PAGING_ROWNUMBER__
FROM 
Test


) SELECT * FROM __PAGING_CTE__
WHERE __PAGING_ROWNUMBER__ BETWEEN 6 AND 10
";
            var pager = CreateSqlPager();
            pager.PagingMethod = DbSelectPager.ePagingMethod.CTE;
            var actual = pager.GetSql();
            Assert.AreEqual(actual, target);
        }

        [TestMethod]
        public void AllClauses()
        {
            var target = @"SELECT
[Field1], [a].[Field1] AS [Field1Copy], [BirthDate] AS [BirthDate], [a].[BirthDate] AS [BirthDateCopy], [b].[Id] AS [Id], [b].[str] AS [str], [b].[num] AS [num], [b].[dt] AS [dt]
FROM 
Table1 a
LEFT JOIN Table2 b ON a.ID = b.FK

WHERE 
Field1 = @0
AND Field2 = @1
AND Field3 = @2
AND Field4 = @3
AND (Field1 = @4 OR Field2 = @5 OR Field3 = @6 OR Field4 = @7)

GROUP BY 
[Score], [Salary]
ORDER BY 
[Id] ASC, [Name] ASC
";


            var db = new Database(new DbConnectionInfo("", eDbType.SqlServer));
            var stmt = db.CreateSelectStatement();
            stmt.From.Append("Table1 a");
            stmt.From.Append("LEFT JOIN Table2 b ON a.ID = b.FK");

            stmt.Fields.Add("Field1");
            stmt.Fields.Add("Field1", "a", "Field1Copy");
            stmt.Fields.Add(Tables.ePerson.BirthDate);
            stmt.Fields.Add(Tables.ePerson.BirthDate, "a", "BirthDateCopy");
            stmt.Fields.Add<Tables.eBigTable>("b");

            stmt.Where.And("Field1 = @0", 5);
            stmt.Where.And("Field2 = @0", "test");
            stmt.Where.And("Field3 = @0", DateTime.Now);
            stmt.Where.And("Field4 = @0", true);
            var parms = new {
                Field1 = 5,
                Field2 = "test",
                Field3 = DateTime.Now,
                Field4 = true
            };
            stmt.Where.And("(Field1 = @Field1 OR Field2 = @Field2 OR Field3 = @Field3 OR Field4 = @Field4)", parms);

            stmt.OrderBy.Add(Tables.ePerson.Id, SqlFragmentOrderBy.eOrderByDirection.ASC);
            stmt.OrderBy.Add("Name", SqlFragmentOrderBy.eOrderByDirection.ASC);

            stmt.GroupBy.Add(Tables.ePerson.Score);
            stmt.GroupBy.Add("Salary");

            var actual = stmt.GetSql();
            Assert.AreEqual(actual, target);
        }

    }
}

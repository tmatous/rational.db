﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rational.DB;
using System.Diagnostics;
using System.Collections.Generic;
using Rational.DB.Tests.Tables;

namespace Rational.DB.Tests
{
    [TestClass]
    public class BigTableTests
    {

        private Database CreateDb()
        {
            return Utility.CreateDbSqlServer();
        }




        public IList<BigTable> Insert_BigTable(int pCount)
        {
            var added = new List<BigTable>();
            var db = CreateDb();
            using (var trans = db.BeginTransaction())
            {
                var rnd = new Random();
                for (int i = 1; i <= pCount; i++)
                {
                    var obj = new BigTable
                    {
                        str = string.Format("insert test {0}", rnd.Next(1000, 9999)),
                        num = rnd.Next(0, 100),
                        dt = DateTime.Today.AddDays(-rnd.Next(300, 20000))
                    };
                    db.Insert<eBigTable>(obj);
                    added.Add(obj);
                }

                trans.Commit();
            }
            return added;
        }



        [TestMethod]
        public void InsertMany()
        {
            var batchSize = 500;
            var toInsert = 200000;
            var insertedCount = 0;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            while (insertedCount < toInsert)
            {
                Insert_BigTable(batchSize);
                insertedCount += batchSize;
                if (insertedCount % 1000 == 0)
                    Debug.Print(string.Format("Inserted {0} ({1})", insertedCount, sw.Elapsed));
            }
            sw.Stop();
            Debug.Print(string.Format("Done, inserted {0} records ({1}).", insertedCount, sw.Elapsed));

            var db = CreateDb();
            int total = db.SelectScalar<int>(db.CreateStatement("SELECT COUNT(*) FROM BigTable"));
            Debug.Print(string.Format("Total {0} records in BigTable.", total));
        }





    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rational.DB;
using System.Diagnostics;
using System.Collections.Generic;
using Rational.DB.Tests.Tables;

namespace Rational.DB.Tests
{
    [TestClass]
    public class PerformanceTests
    {

        private Database CreateDbSqlServer()
        {
            return Utility.CreateDbSqlServer();
        }







        [TestMethod]
        public void Perf_CastVsConvert()
        {
            //1M
            var sampleCount = 1000000;


            //speed test for typecasting decimals
            var sw1 = Stopwatch.StartNew();
            for (var x = 0; x < sampleCount; x++)
            {
                var orig = 100m;
                object origObj = orig;
                var newval = (decimal)origObj;
            }
            sw1.Stop();


            //speed test for converting decimals into decimal
            var sw2 = Stopwatch.StartNew();
            for (var x = 0; x < sampleCount; x++)
            {
                var orig = 100m;
                object origObj = orig;
                var newval = (decimal)Convert.ChangeType(origObj, typeof(decimal));
            }
            sw2.Stop();


            //speed test for converting ints into decimals
            var sw3 = Stopwatch.StartNew();
            for (var x = 0; x < sampleCount; x++)
            {
                var orig = 100;
                object origObj = orig;
                var newval = (decimal)Convert.ChangeType(origObj, typeof(decimal));
            }
            sw3.Stop();


            //speed test for checking decimals and just casting into decimal (success)
            var sw4 = Stopwatch.StartNew();
            for (var x = 0; x < sampleCount; x++)
            {
                var orig = 100m;
                object origObj = orig;
                decimal newval;
                if (!origObj.GetType().Equals(typeof(decimal)))
                    newval = (decimal)Convert.ChangeType(origObj, typeof(decimal));
                else
                    newval = (decimal)origObj;
            }
            sw4.Stop();


            //speed test for checking ints and requiring a convert into decimal (fail)
            var sw5 = Stopwatch.StartNew();
            for (var x = 0; x < sampleCount; x++)
            {
                var orig = 100;
                object origObj = orig;
                decimal newval;
                if (!origObj.GetType().Equals(typeof(decimal)))
                    newval = (decimal)Convert.ChangeType(origObj, typeof(decimal));
                else
                    newval = (decimal)origObj;
            }
            sw5.Stop();


            Assert.Inconclusive(string.Format("{0} samples, typecast decimal: {1}ms, convert decimal to decimal: {2}ms, convert int to decimal: {3}ms, success check with cast: {4}ms, fail check with convert: {5}ms", sampleCount, sw1.ElapsedMilliseconds, sw2.ElapsedMilliseconds, sw3.ElapsedMilliseconds, sw4.ElapsedMilliseconds, sw5.ElapsedMilliseconds));

        }



        [TestMethod]
        public void Perf_CustomMapping()
        {
            //speed test for loading many records with custom mapping

            var sql = "SELECT TOP 2000 * FROM Person union all SELECT TOP 2000 * FROM Person union all SELECT TOP 2000 * FROM Person union all SELECT TOP 2000 * FROM Person union all SELECT TOP 2000 * FROM Person";

            var db0 = CreateDbSqlServer();
            var stmt0 = db0.CreateStatement(sql);
            var res0 = db0.SelectObjectList<Person>(stmt0);
            Assert.IsTrue(res0.Count > 1000, "Fewer than 1000 records, invalid perf test");



            var db2 = CreateDbSqlServer();
            db2.MapDbToClrType(System.Data.DbType.AnsiString, (x) => string.Format("This is an ansi string. Original value: {0}", x));

            var sw2 = Stopwatch.StartNew();
            var stmt2 = db2.CreateStatement(sql);
            var res2 = db2.SelectObjectList<Person>(stmt2);
            sw2.Stop();




            var db1 = CreateDbSqlServer();

            var sw1 = Stopwatch.StartNew();
            var stmt1 = db1.CreateStatement(sql);
            var res1 = db1.SelectObjectList<Person>(stmt1);
            sw1.Stop();






            Assert.IsFalse(res1[0].Status.StartsWith("This is an ansi string."));
            Assert.IsTrue(res2[0].Status.StartsWith("This is an ansi string."));

            Assert.Inconclusive(string.Format("{0} records, Without mapping: {1}ms, With mapping: {2}ms", res0.Count, sw1.ElapsedMilliseconds, sw2.ElapsedMilliseconds));
        }



    }
}

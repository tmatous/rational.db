﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rational.DB;

namespace Rational.DB.Tests
{
    [TestClass]
    public class ParameterTests
    {

        private Dictionary<string, DbParameter> getParamDict(DbParameterCollection coll)
        {
            var dict = new Dictionary<string, DbParameter>(StringComparer.OrdinalIgnoreCase);
            foreach (DbParameter item in coll)
            {
                dict.Add(item.ParameterName, item);
            }
            return dict;
        }

        [TestMethod]
        public void NumberedParameters1()
        {
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @0", "abc");
            Assert.AreEqual(1, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual("abc", dict["0"].Value);
        }

        [TestMethod]
        public void NumberedParameters2()
        {
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @0 OR fld2 = @1", "abcd", 3);
            Assert.AreEqual(2, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual("abcd", dict["0"].Value);
            Assert.AreEqual(3, dict["1"].Value);
        }

        [TestMethod]
        public void NumberedParameters3()
        {
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @0 OR fld2 = @1 OR fld3 = @2", "abcde", 5, DateTime.Today);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual("abcde", dict["0"].Value);
            Assert.AreEqual(5, dict["1"].Value);
            Assert.AreEqual(DateTime.Today, dict["2"].Value);
        }

        [TestMethod]
        public void NumberedParameters3Multi()
        {
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @0 OR fld2 IN ( @1 ) OR fld3 = @2", "abcde", new int[] { 3, 4, 5 }, DateTime.Today);
            Assert.AreEqual(5, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual("abcde", dict["0"].Value);
            Assert.AreEqual(3, dict["1"].Value);
            Assert.AreEqual(4, dict["2"].Value);
            Assert.AreEqual(5, dict["3"].Value);
            Assert.AreEqual(DateTime.Today, dict["4"].Value);
            Assert.AreEqual("SELECT * FROM Test WHERE fld = @0 OR fld2 IN ( @1,@2,@3 ) OR fld3 = @4\r\n", stmt.GetSql());
        }

        [TestMethod]
        public void NamedParametersDict()
        {
            var sourceParams = new Dictionary<string, object>();
            sourceParams["fld"] = "abc";
            sourceParams["fld2"] = 5;
            sourceParams["fld3"] = DateTime.Today;
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 = @fld2 OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams["fld"], dict["0"].Value);
            Assert.AreEqual(sourceParams["fld2"], dict["1"].Value);
            Assert.AreEqual(sourceParams["fld3"], dict["2"].Value);
        }

        [TestMethod]
        public void NamedParametersObj()
        {
            var sourceParams = new
            {
                fld = "abc",
                fld2 = 12,
                fld3 = DateTime.Today
            };
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 = @fld2 OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams.fld, dict["0"].Value);
            Assert.AreEqual(sourceParams.fld2, dict["1"].Value);
            Assert.AreEqual(sourceParams.fld3, dict["2"].Value);
        }

        [TestMethod]
        public void NamedParametersObjDbParameter()
        {
            var sourceParams = new
            {
                fld = "abc",
                fld2 = 45,
                fld3 = new DbParameter("fld3", DateTime.Today, System.Data.DbType.Date)
            };
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 = @fld2 OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams.fld, dict["0"].Value);
            Assert.AreEqual(sourceParams.fld2, dict["1"].Value);
            Assert.AreEqual(sourceParams.fld3.Value, dict["2"].Value);
            Assert.AreEqual(System.Data.DbType.Date, dict["2"].DbType);
        }

        [TestMethod]
        public void NamedParametersObjIDataParameter()
        {
            var sourceParams = new
            {
                fld = "abc",
                fld2 = 45,
                fld3 = new System.Data.SqlClient.SqlParameter("fld3", System.Data.SqlDbType.Date)
            };
            sourceParams.fld3.Value = DateTime.Today;
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 = @fld2 OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams.fld, dict["0"].Value);
            Assert.AreEqual(sourceParams.fld2, dict["1"].Value);
            Assert.AreEqual(sourceParams.fld3.Value, dict["2"].Value);
            Assert.AreEqual(System.Data.DbType.Date, dict["2"].DbType);
        }

        [TestMethod]
        public void NamedParametersObjIDataParameterMulti()
        {
            var sourceParams = new
            {
                fld = "abc",
                fld2 = new List<System.Data.IDbDataParameter>(),
                fld3 = new System.Data.SqlClient.SqlParameter("fld3", System.Data.SqlDbType.Date)
            };
            sourceParams.fld2.Add(new System.Data.SqlClient.SqlParameter("fld2", System.Data.SqlDbType.BigInt));
            sourceParams.fld2[0].Value = 4;
            sourceParams.fld2.Add(new System.Data.SqlClient.SqlParameter("fld2", System.Data.SqlDbType.BigInt));
            sourceParams.fld2[1].Value = 5;
            sourceParams.fld2.Add(new System.Data.SqlClient.SqlParameter("fld2", System.Data.SqlDbType.BigInt));
            sourceParams.fld2[2].Value = 6;
            sourceParams.fld3.Value = DateTime.Today;
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 IN ( @fld2 ) OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(5, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams.fld, dict["0"].Value);

            Assert.AreEqual(4, dict["1"].Value);
            Assert.AreEqual(System.Data.DbType.Int64, dict["1"].DbType);
            Assert.AreEqual(5, dict["2"].Value);
            Assert.AreEqual(System.Data.DbType.Int64, dict["2"].DbType);
            Assert.AreEqual(6, dict["3"].Value);
            Assert.AreEqual(System.Data.DbType.Int64, dict["3"].DbType);

            Assert.AreEqual(sourceParams.fld3.Value, dict["4"].Value);
            Assert.AreEqual(System.Data.DbType.Date, dict["4"].DbType);

            Assert.AreEqual("SELECT * FROM Test WHERE fld = @0 OR fld2 IN ( @1,@2,@3 ) OR fld3 = @4\r\n", stmt.GetSql());
        }

        [TestMethod]
        public void NamedParametersDictDbParameter()
        {
            var sourceParams = new Dictionary<string, object>();
            sourceParams["fld"] = "abc";
            sourceParams["fld2"] = 5;
            sourceParams["fld3"] = new DbParameter("fld3", DateTime.Today, System.Data.DbType.Date);
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 = @fld2 OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams["fld"], dict["0"].Value);
            Assert.AreEqual(sourceParams["fld2"], dict["1"].Value);
            Assert.AreEqual(DateTime.Today, dict["2"].Value);
            Assert.AreEqual(System.Data.DbType.Date, dict["2"].DbType);
        }

        [TestMethod]
        public void NamedParametersDictIDataParameter()
        {
            var sourceParams = new Dictionary<string, object>();
            sourceParams["fld"] = "abc";
            sourceParams["fld2"] = 5;
            var p3 = new System.Data.SqlClient.SqlParameter("fld", System.Data.SqlDbType.Date);
            p3.Value = DateTime.Today;
            sourceParams["fld3"] = p3;
            var stmt = new DbStatement();
            stmt.Statement.Append("SELECT * FROM Test WHERE fld = @fld OR fld2 = @fld2 OR fld3 = @fld3", sourceParams);
            Assert.AreEqual(3, stmt.Parameters.Count);
            var dict = getParamDict(stmt.Parameters);
            Assert.AreEqual(sourceParams["fld"], dict["0"].Value);
            Assert.AreEqual(sourceParams["fld2"], dict["1"].Value);
            Assert.AreEqual(DateTime.Today, dict["2"].Value);
            Assert.AreEqual(System.Data.DbType.Date, dict["2"].DbType);
        }


    }
}

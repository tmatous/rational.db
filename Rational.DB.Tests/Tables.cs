﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rational.DB.Tests.Tables
{
    [Table(SqlName = "TestFields")]
    public enum eTestFields
    {
        [Column(PrimaryKey = true)]
        Id,
        str,
        ustr,
        dec,
        dt,
        boo,
        [Column(SqlName = "int")]
        clrint,
        [Column(NotNull = true)]
        strnn,
        [Column(NotNull = true)]
        ustrnn,
        [Column(NotNull = true)]
        decnn,
        [Column(NotNull = true)]
        dtnn,
        [Column(NotNull = true)]
        boonn,
        [Column(NotNull = true)]
        intnn,
        bin,
        chr,
        [Column(NotNull = true)]
        chrnn

    }

    public class TestFields
    {
        public int Id { get; set; }
        public string str { get; set; }
        public string ustr { get; set; }
        public decimal? dec { get; set; }
        public DateTime? dt { get; set; }
        public Boolean? boo { get; set; }
        public Int32? clrint { get; set; }
        public string strnn { get; set; }
        public string ustrnn { get; set; }
        public decimal decnn { get; set; }
        public DateTime dtnn { get; set; }
        public Boolean boonn { get; set; }
        public Int32 intnn { get; set; }
        public byte[] bin { get; set; }
        public string chr { get; set; }
        public string chrnn { get; set; }
    }





    [Table(SqlName = "Person")]
    public enum ePerson
    {
        [Column(PrimaryKey = true, AutoIncrement = true)]
        Id,
        [Column(NotNull = true)]
        Status,
        [Column(NotNull = true)]
        Name,
        [Column(NotNull = true)]
        Score,
        [Column(NotNull = true)]
        Salary,
        [Column(NotNull = true)]
        BirthDate
    }

    public class Person
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public Int32 Score { get; set; }
        public decimal Salary { get; set; }
        public DateTime BirthDate { get; set; }
    }

    [Table(SqlName = "BigTable")]
    public enum eBigTable
    {
        [Column(PrimaryKey = true, AutoIncrement = true)]
        Id,
        str,
        num,
        dt
    }

    public class BigTable
    {
        public int Id { get; set; }
        public string str { get; set; }
        public Int32 num { get; set; }
        public DateTime dt { get; set; }
    }


}

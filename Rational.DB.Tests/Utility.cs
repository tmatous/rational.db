﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Rational.DB.Tests
{
    class Utility
    {

        static Utility()
        {
            //in .NET core, providers are not registered automatically. must manually register the factories we are using
            System.Data.Common.DbProviderFactories.RegisterFactory("System.Data.SqlClient", System.Data.SqlClient.SqlClientFactory.Instance);
        }


        public static Database CreateDbSqlServer()
        {
            var ci = new DbConnectionInfo("Server=(local)\\SQLEXPRESS;Database=TestDb;Trusted_Connection=true", eDbType.SqlServer);
            var db = new Database(ci);
            db.OnExecuteComplete += db_ExecuteComplete;
            db.OnBeforeExecute += db_BeforeExecute;
            db.OnValidationWarning += db_ValidationWarning;
            return db;
        }

        public static Database CreateDbMySql()
        {
            var ci = new DbConnectionInfo("Server=mysqltest.cwldi7xwsp7t.us-east-1.rds.amazonaws.com;Port=3306;Database=TestDb;Uid=admin;Pwd=;", eDbType.MySql);
            var db = new Database(ci);
            db.OnExecuteComplete += db_ExecuteComplete;
            db.OnBeforeExecute += db_BeforeExecute;
            db.OnValidationWarning += db_ValidationWarning;
            return db;
        }

        public static void db_ExecuteComplete(object sender, Database.ExecuteCompleteEventArgs args)
        {
            //Debug.Print(args.Sql);
        }

        public static void db_BeforeExecute(object sender, Database.BeforeExecuteEventArgs args)
        {
            //Debug.Print(args.Sql);
        }

        private static void db_ValidationWarning(object sender, Database.ValidationWarningEventArgs args)
        {
            Debug.Print(string.Format("{0} : {1}", args.WarningType, args.Summary));
        }


    }
}

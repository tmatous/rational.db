﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rational.DB;

namespace Rational.DB.Tests
{
    [TestClass]
    public class OtherTests
    {

        [TestMethod]
        public void IsSimpleType()
        {
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(int)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Int64)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(decimal)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Boolean)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(string)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(String)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(DateTime)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(DateTimeOffset)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Guid)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(double)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Tables.ePerson)));

            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(int?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Int64?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(decimal?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Boolean?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(DateTime?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(DateTimeOffset?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Guid?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(double?)));
            Assert.IsTrue(Rational.DB.Utility.IsSimpleType(typeof(Tables.ePerson?)));


            Assert.IsFalse(Rational.DB.Utility.IsSimpleType(typeof(object)));
            Assert.IsFalse(Rational.DB.Utility.IsSimpleType(typeof(Tables.Person)));
            Assert.IsFalse(Rational.DB.Utility.IsSimpleType(typeof(OtherTests)));
            Assert.IsFalse(Rational.DB.Utility.IsSimpleType(typeof(System.Text.StringBuilder)));
            Assert.IsFalse(Rational.DB.Utility.IsSimpleType(typeof(byte[])));
            Assert.IsFalse(Rational.DB.Utility.IsSimpleType(typeof(System.Collections.ArrayList)));
        }



    }
}

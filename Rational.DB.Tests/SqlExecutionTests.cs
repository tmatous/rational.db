﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rational.DB;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Rational.DB.Tests.Tables;

namespace Rational.DB.Tests
{
    [TestClass]
    public class SqlExecutionTests
    {

        private Database CreateDb()
        {
            //return Utility.CreateDbMySql();
            return Utility.CreateDbSqlServer();
        }


        private TestFields CreateTestFields(DbRow r)
        {
            var res = new TestFields();
            res.Id = r.GetInt32("Id");
            res.str = r.GetString("str");
            res.ustr = r.GetString("ustr");
            res.dec = r.GetDecimalNullable("dec");
            res.dt = r.GetDateTimeNullable("dt");
            res.boo = r.GetBooleanNullable("boo");
            res.clrint = r.GetInt32Nullable("int");
            res.strnn = r.GetString("strnn");
            res.ustrnn = r.GetString("ustrnn");
            res.decnn = r.GetDecimal("decnn");
            res.dtnn = r.GetDateTime("dtnn");
            res.boonn = r.GetBoolean("boonn");
            res.intnn = r.GetInt32("intnn");
            res.bin = r.GetByteArray("bin");
            return res;
        }




        public Int32 GetRowCount()
        {
            var db = CreateDb();

            var stmt = new DbStatement("SELECT COUNT(*) FROM TestFields");
            var cnt = db.SelectScalar<Int32>(stmt);
            return cnt;
        }

        public IList<TestFields> Insert_TestFields(IEnumerable<TestFields> pToCreate)
        {
            var db = CreateDb();
            var delStmt = db.CreateStatement("DELETE FROM TestFields");
            var deleted = db.Execute(delStmt);

            var curId = 1;
            foreach (var item in pToCreate)
            {
                if (item.dtnn == DateTime.MinValue) item.dtnn = DateTime.UtcNow;
                if (item.Id == 0) item.Id = curId;
                db.Insert<eTestFields>(item);
                curId++;
            }

            var selStmt = db.CreateSelectStatement<eTestFields>();
            return db.SelectObjectList<TestFields>(selStmt);
        }

        public TestFields Insert_TestFields(TestFields pToCreate)
        {
            var result = Insert_TestFields(new[] { pToCreate });
            return result[0];
        }

        public IList<TestFields> Insert_TestFields(int pCount)
        {
            var toCreate = new List<TestFields>();
            for (int i = 0; i < pCount; i++)
            {
                toCreate.Add(new TestFields
                {
                    str = string.Format("test record {0}", i)
                });
            }

            return Insert_TestFields(toCreate);
        }

        public int EnsureCreatedWithAnsiStr(String pToCreate)
        {
            var obj = new TestFields { str = pToCreate };
            var res = Insert_TestFields(new[] { obj });
            return res[0].Id;
        }

        public int EnsureCreatedWithUStr(String pToCreate)
        {
            var obj = new TestFields { ustr = pToCreate };
            var res = Insert_TestFields(new[] { obj });
            return res[0].Id;
        }


        public IList<Person> Insert_Person(int pCount)
        {
            var added = new List<Person>();
            var db = CreateDb();
            var rnd = new Random();
            for (int i = 1; i <= pCount; i++)
            {
                var obj = new Person
                {
                    Name = string.Format("insert test {0}", rnd.Next(1000, 9999)),
                    Score = rnd.Next(0, 100),
                    Salary = Convert.ToDecimal(rnd.Next(30, 200) * 1000),
                    BirthDate = DateTime.Today.AddDays(-rnd.Next(300, 20000))
                };
                db.Insert<ePerson>(obj);
                added.Add(obj);
            }
            return added;
        }



        [TestMethod]
        public void Inserts()
        {
            //test values
            var obj1 = new TestFields
            {
                str = "insert test values",
                ustr = "blah",
                boonn = true,
                clrint = 100,
                dec = 100.5m,
                decnn = 5.4m,
                intnn = 250,
                strnn = "test",
                ustrnn = "blah2",
                boo = true,
                dt = DateTime.UtcNow,
                dtnn = DateTime.UtcNow,
                bin = new byte[] { 0x10, 0x13 }
            };
            var res1 = Insert_TestFields(obj1);

            //test nulls
            var obj2 = new TestFields
            {
                str = null,
                ustr = null,
                boonn = true,
                clrint = null,
                dec = null,
                decnn = 5.4m,
                intnn = 250,
                strnn = "test",
                ustrnn = "blah2",
                boo = null,
                dt = null,
                dtnn = DateTime.UtcNow,
                bin = null
            };
            var res2 = Insert_TestFields(obj2);
        }

        [TestMethod]
        public void InsertMultiple()
        {
            Insert_Person(100);
        }

        [TestMethod]
        public void SelectDataTable()
        {
            var db = CreateDb();
            var dtstmt = db.CreateSelectStatement();
            dtstmt.From.Append(dtstmt.TableIdentifier<ePerson>());
            dtstmt.Fields.Add<ePerson>();
            dtstmt.ResultMax = 10;
            var dt = db.SelectDataTable(dtstmt);

            Assert.AreEqual(10, dt.Rows.Count);
        }

        [TestMethod]
        public void SelectDataTableWithNulls()
        {
            var db = CreateDb();
            db.MapDbToClrType(System.Data.DbType.AnsiString, (x) => null);
            db.MapDbToClrType(System.Data.DbType.DateTime, (x) => null);

            var dtstmt = db.CreateStatement("SELECT cast('val1' as varchar) as str, cast('val2' as nvarchar) as ustr, getdate() as dt, cast(5 as int) as num");
            var dt = db.SelectDataTable(dtstmt);

            Assert.AreEqual(1, dt.Rows.Count);
            Assert.AreEqual("val2", dt.Rows[0]["ustr"]);
            Assert.AreEqual(5, dt.Rows[0]["num"]);
            Assert.AreEqual(DBNull.Value, dt.Rows[0]["str"]);
            Assert.AreEqual(DBNull.Value, dt.Rows[0]["dt"]);
        }

        [TestMethod]
        public void SelectDataSet()
        {
            var db = CreateDb();
            var dtstmt = db.CreateStatement("SELECT * FROM Person where id <= 10; SELECT Id, Name FROM Person where id <= 20;");
            var ds = db.SelectDataSet(dtstmt);

            Assert.AreEqual(2, ds.Tables.Count);
            Assert.AreEqual(10, ds.Tables[0].Rows.Count);
            Assert.AreEqual(20, ds.Tables[1].Rows.Count);
            Assert.AreEqual(2, ds.Tables[1].Columns.Count);
        }

        [TestMethod]
        public void SelectObject()
        {
            var db = CreateDb();
            var stmt = db.CreateSelectStatement<eTestFields>();
            stmt.ResultMax = 1;
            var res = db.SelectObject<TestFields>(stmt);
        }

        [TestMethod]
        public void SelectObjectList()
        {
            var db = CreateDb();
            var stmt = db.CreateSelectStatement<eTestFields>();
            stmt.ResultMax = 10;
            var res = db.SelectObjectList<TestFields>(stmt);
        }

        [TestMethod]
        public void Search()
        {
            var db = CreateDb();
            var stmt = db.CreateSelectStatement<eTestFields>();
            stmt.Where.AppendWithIdentifiers("{Id} = @Id", new { Id = eTestFields.Id }, new { Id = 1 });
            stmt.OrderBy.Add("str");
            stmt.ResultMax = 10;
            var res = db.SelectObjectList<TestFields>(stmt);
        }


        [TestMethod]
        public void MVP()
        {
            var db = CreateDb();

            var created = Insert_TestFields(10);

            var created_ids_enum = (from c in created select c.Id);
            var stmt1 = db.CreateSelectStatement<eTestFields>();
            stmt1.Where.And("Id IN (@0)", created_ids_enum);
            var objs1 = db.SelectObjectList<TestFields>(stmt1);
            Assert.AreEqual(10, objs1.Count);

            var created_ids_list = created_ids_enum.ToList();
            var stmt2 = db.CreateSelectStatement<eTestFields>();
            stmt2.Where.And("Id IN (@0)", created_ids_list);
            var objs2 = db.SelectObjectList<TestFields>(stmt2);
            Assert.AreEqual(10, objs2.Count);

            var created_ids_arr = created_ids_enum.ToArray();
            var stmt3 = db.CreateSelectStatement<eTestFields>();
            stmt3.Where.And("Id IN (@0)", created_ids_arr);
            var objs3 = db.SelectObjectList<TestFields>(stmt3);
            Assert.AreEqual(10, objs3.Count);

            var created_strs_enum = (from c in created select c.str);
            var stmt4 = db.CreateSelectStatement<eTestFields>();
            stmt4.Where.And("str IN (@0)", created_strs_enum);
            var objs4 = db.SelectObjectList<TestFields>(stmt4);
            Assert.AreEqual(10, objs4.Count);
        }


        [TestMethod]
        public void StaticCopyFunc()
        {
            var db = CreateDb();

            var stmt = db.CreateStatement<DbStatement>();
            stmt.Statement.Append("SELECT * FROM TestFields WHERE Id < @0", 100);
            var objs = db.SelectObjectList(stmt, CreateTestFields);
        }

        public class TestDynamicCopyFunc
        {
            public int Idtest { get; set; }
            public string strtest { get; set; }
        }

        [TestMethod]
        public void DynamicCopyFunc()
        {
            var db = CreateDb();

            var stmt = db.CreateStatement<DbStatement>();
            stmt.Statement.Append("SELECT * FROM TestFields WHERE Id < @0", 100);
            var objs = db.SelectObjectList<TestFields>(stmt);

            var stmt2 = db.CreateStatement<DbStatement>();
            stmt2.Statement.Append("SELECT Id AS IDTEST, str AS STRTEST FROM TestFields WHERE Id < @0", 100);
            var objs2 = db.SelectObjectList<TestDynamicCopyFunc>(stmt2);
        }


        [TestMethod]
        public void Parameters()
        {
            var db = CreateDb();

            var orig = new List<TestFields>();
            orig.Add(new TestFields { ustr = "test 1" });
            orig.Add(new TestFields { ustr = "test 2" });
            orig.Add(new TestFields { ustr = "test 3" });

            var newRecords = Insert_TestFields(orig);

            var targetStrArr = (from i in orig select i.ustr).ToArray();
            var targetIdArr = (from i in orig select i.Id).ToArray();
            var targetStrList = new List<string>(targetStrArr);
            var targetIdList = new List<int>(targetIdArr);


            //single parameter
            try
            {
                var stmt0 = db.CreateSelectStatement<eTestFields>();
                stmt0.Where.And("ustr = @0", orig[0].ustr);
                var objs0 = db.SelectObjectList<TestFields>(stmt0);
                Assert.IsTrue(objs0.Count > 0);
                Assert.AreEqual(orig[0].ustr, objs0[0].ustr);
                Assert.AreEqual(orig[0].Id, objs0[0].Id);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //multiple MVP
            try
            {
                var stmt1 = db.CreateSelectStatement<eTestFields>();
                stmt1.Where.And("ustr IN (@0) AND Id IN (@1)",
                    targetStrArr,
                    new int[] { orig[0].Id, orig[1].Id }
                );
                var objs1 = db.SelectObjectList<TestFields>(stmt1);
                Assert.IsTrue(objs1.Count == 2);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //multiple MVP with List, Array
            try
            {
                var stmt2 = db.CreateSelectStatement<eTestFields>();
                stmt2.Where.And("ustr IN (@0) AND Id IN (@1)",
                    targetStrArr,
                    targetIdList
                );
                var objs2 = db.SelectObjectList<TestFields>(stmt2);
                Assert.IsTrue(objs2.Count == 3);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //multiple MVP to scalar list
            try
            {
                var stmt2 = db.CreateSelectStatement("TestFields", "Id");
                stmt2.Where.And("Id IN (@0)", targetIdList);
                var res2 = db.SelectScalarList<int>(stmt2);
                Assert.IsTrue(res2.Count == 3);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //named parameters
            try
            {
                var stmt3 = db.CreateSelectStatement<eTestFields>();
                stmt3.Where.And("ustr IN (@strs) AND Id = @id", new
                {
                    strs = targetStrList,
                    id = orig[0].Id
                });
                var objs3 = db.SelectObjectList<TestFields>(stmt3);
                Assert.IsTrue(objs3.Count == 1);
                Assert.AreEqual(orig[0].ustr, objs3[0].ustr);
                Assert.AreEqual(orig[0].Id, objs3[0].Id);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //named parameters with dictionary
            try
            {
                var parms = new Dictionary<string, object>();
                parms.Add("id", orig[0].Id);
                parms.Add("strs", targetStrList);
                var stmt3 = db.CreateSelectStatement<eTestFields>();
                stmt3.Where.And("ustr IN (@strs) AND Id = @id", parms);
                var objs3 = db.SelectObjectList<TestFields>(stmt3);
                Assert.IsTrue(objs3.Count == 1);
                Assert.AreEqual(orig[0].ustr, objs3[0].ustr);
                Assert.AreEqual(orig[0].Id, objs3[0].Id);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //named parameters with multiple arguments, should throw error
            try
            {
                var stmt4 = db.CreateSelectStatement<eTestFields>();
                stmt4.Where.And("ustr = @ustr AND Id = @id", orig[0].ustr, orig[0].Id);
                Assert.Fail("named parameters with multiple arguments, should throw error");
            }
            catch (UnitTestAssertException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message.Contains("Named parameters require a single data object"));
            }


            //named parameter doesn't exist, should throw error
            try
            {
                var stmt4 = db.CreateSelectStatement<eTestFields>();
                stmt4.Where.And("ustr = @ustr", new { ustrx = "blah" });
                Assert.Fail("named parameter doesn't exist, should throw error");
            }
            catch (UnitTestAssertException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message.Contains("Could not find data for parameter"));
            }


            //numbered parameter doesn't exist, should throw error
            try
            {
                var stmt4 = db.CreateSelectStatement<eTestFields>();
                stmt4.Where.And("ustr = @3", "test", "junk");
                Assert.Fail("numbered parameter doesn't exist, should throw error");
            }
            catch (UnitTestAssertException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message.Contains("Could not find data for parameter"));
            }

        }


        [TestMethod]
        public void CustomParameters()
        {
            var db = CreateDb();

            var targetStr = "str -- Test for CustomParameters";
            var targetUStr = "ustr -- Test for CustomParameters";
            var targetDate = DateTime.Today;

            var orig = new List<TestFields>();
            orig.Add(new TestFields { str = targetStr });
            orig.Add(new TestFields { str = "junk" });
            orig.Add(new TestFields { ustr = targetUStr });
            orig.Add(new TestFields { ustr = "junk2" });
            orig.Add(new TestFields { dtnn = targetDate });

            var newRecs = Insert_TestFields(orig);

            var stmt0 = db.CreateStatement<DbStatement>();
            stmt0.Statement.Append("SELECT * FROM TestFields WHERE str = @0", targetStr);
            var objs0 = db.SelectObjectList<TestFields>(stmt0);
            Assert.AreEqual(1, objs0.Count);

            db.MapClrToDbType(typeof(string), System.Data.DbType.AnsiString);
            var objs0_2 = db.SelectObjectList<TestFields>(stmt0);
            Assert.AreEqual(1, objs0_2.Count);

            var stmt = db.CreateStatement<DbStatement>();
            stmt.Statement.Append("SELECT * FROM TestFields WHERE str = @0", new Rational.DB.DbParameter("", targetStr, System.Data.DbType.AnsiString));
            var objs = db.SelectObjectList<TestFields>(stmt);
            Assert.AreEqual(1, objs.Count);



            var stmt2 = db.CreateStatement<DbStatement>();
            stmt2.Statement.Append("SELECT * FROM TestFields WHERE ustr = @0", new Rational.DB.DbParameter("", targetUStr, System.Data.DbType.String));
            var objs2 = db.SelectObjectList<TestFields>(stmt2);
            Assert.AreEqual(1, objs2.Count);

            //try to map from a string to a provider type
            var stmt3 = db.CreateStatement<DbStatement>();
            stmt3.Statement.Append("SELECT * FROM TestFields WHERE dtnn = @0", new Rational.DB.DbParameter("", targetDate, "Date"));
            var objs3 = db.SelectObjectList<TestFields>(stmt3);
            Assert.AreEqual(1, objs3.Count);
        }


        [TestMethod]
        public void StoredProcs()
        {
            var db = CreateDb();

            var testString = "test string";
            var testString2 = "another string";
            var testInt = 56;

            var stmt0 = db.CreateProcedureCall("dbo.uspTest");
            db.Execute(stmt0);

            var stmt1 = db.CreateProcedureCall("dbo.uspTestRetVal");
            var p1_1 = stmt1.Parameters.AddReturnValue("@RetVal");
            db.Execute(stmt1);
            Assert.AreEqual(100, p1_1.GetOutputValue<int>());

            var stmt2 = db.CreateProcedureCall("dbo.uspTestOutParams");
            stmt2.Parameters.AddUString("@instring", testString);
            stmt2.Parameters.AddInt32("@inint", testInt);
            var p2_1 = stmt2.Parameters.AddOutput("@outstring", System.Data.DbType.String, 100);
            var p2_2 = stmt2.Parameters.AddOutput("@outint", System.Data.DbType.Int32, 4);
            var p2_3 = stmt2.Parameters.AddOutput("@outdt", System.Data.DbType.DateTime, 4);
            var p2_4 = stmt2.Parameters.AddOutput("@outnum", System.Data.DbType.Decimal, 5, 2);
            var p2_5 = stmt2.Parameters.AddInputOutput("@inoutstring", testString2, System.Data.DbType.String, 100);
            db.Execute(stmt2);
            Assert.AreEqual(string.Format("instring: {0}", testString), p2_1.GetOutputValue<string>());
            Assert.AreEqual((testInt + 100), p2_2.GetOutputValue<int>());
            Assert.AreEqual(DateTime.Today, p2_3.GetOutputValue<DateTime>().Date);
            Assert.AreEqual((decimal)(testInt + 100.5), p2_4.GetOutputValue<decimal>());
            Assert.AreEqual(string.Format("{0} out", testString2), p2_5.GetOutputValue<string>());

            var stmt3 = db.CreateProcedureCall("dbo.uspTestScalar");
            stmt3.Parameters.AddUString("@instring", testString);
            var res3 = db.SelectScalar<string>(stmt3);
            Assert.AreEqual(string.Format("instring: {0}", testString), res3);

            var targetStr = "str -- Test for StoredProcs";
            EnsureCreatedWithAnsiStr(targetStr);
            var stmt4 = db.CreateProcedureCall("dbo.uspTestRecords");
            stmt4.Parameters.AddAnsiString("@str", targetStr);
            var res4 = db.SelectObjectList<TestFields>(stmt4);
            Assert.IsTrue(res4.Count > 0);

        }

        [TestMethod]
        public void ConvertTypesScalar()
        {
            var db = CreateDb();

            var orig = new TestFields
            {
                dtnn = DateTime.Today,
                strnn = "2010-02-04",
                ustr = "a long test string",
                ustrnn = "a"
            };
            var newRec = Insert_TestFields(orig);

            //convert date to string
            try
            {
                var stmt0 = db.CreateStatement("SELECT dtnn FROM TestFields");
                var res0 = db.SelectScalar<string>(stmt0);
                Assert.AreEqual(orig.dtnn.ToString(), res0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //convert string to date
            try
            {
                var stmt0 = db.CreateStatement("SELECT strnn FROM TestFields");
                var res0 = db.SelectScalar<DateTime>(stmt0);
                Assert.AreEqual(new DateTime(2010, 2, 4), res0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //convert string to char
            try
            {
                var stmt0 = db.CreateStatement("SELECT ustrnn FROM TestFields");
                var res0 = db.SelectScalar<char>(stmt0);
                Assert.AreEqual('a', res0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //convert long string to char, error
            try
            {
                var stmt0 = db.CreateStatement("SELECT ustr FROM TestFields");
                var res0 = db.SelectScalar<char>(stmt0);
                Assert.Fail("convert long test string to char, error");
            }
            catch (UnitTestAssertException)
            {
                throw;
            }
            catch (FormatException)
            {
                //ok
            }


        }


        public class ConvertTypesObject
        {
            public int myint { get; set; }
            public string mystr { get; set; }
            public DateTime mydt { get; set; }
            public decimal mydec { get; set; }
            public bool mybool { get; set; }

            public int? myintn { get; set; }
            public DateTime? mydtn { get; set; }
            public decimal? mydecn { get; set; }
            public bool? mybooln { get; set; }

            public int notwritable
            {
                get { return 0; }
            }
        }

        [TestMethod]
        public void ConvertTypesRecord()
        {
            var db = CreateDb();

            var orig = new TestFields
            {
                str = "str1",
                ustr = "ustr1",
                intnn = 5,
                dec = 5.3m,
                decnn = 5.5m,
                dtnn = DateTime.Today
            };
            var newRec = Insert_TestFields(orig);

            //convert date to string
            try
            {
                var stmt0 = db.CreateStatement("SELECT dtnn AS mystr FROM TestFields");
                var res0 = db.SelectObject<ConvertTypesObject>(stmt0);
                Assert.AreEqual(DateTime.Today.ToString(), res0.mystr);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //convert dec to int (5.3 rounds down)
            try
            {
                var stmt0 = db.CreateSelectStatement();
                stmt0.From.Append("TestFields");
                stmt0.Fields.Add("dec", null, "myint");
                var res0 = db.SelectObject<ConvertTypesObject>(stmt0);
                Assert.AreEqual(5, res0.myint);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //convert dec to int (5.5 rounds up)
            try
            {
                var stmt0 = db.CreateStatement("SELECT decnn AS myint FROM TestFields");
                var res0 = db.SelectObject<ConvertTypesObject>(stmt0);
                Assert.AreEqual(6, res0.myint);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //convert dec to nullable int
            try
            {
                var stmt0 = db.CreateSelectStatement();
                stmt0.From.Append("TestFields");
                stmt0.Fields.Add("dec", null, "myintn");
                var res0 = db.SelectObject<ConvertTypesObject>(stmt0);
                Assert.AreEqual(5, res0.myintn);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


            //null to nullable int
            try
            {
                var stmt0 = db.CreateStatement("SELECT NULL AS myintn");
                var res0 = db.SelectObject<ConvertTypesObject>(stmt0);
                Assert.AreEqual(null, res0.myintn);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


        }

        [TestMethod]
        public void ReadonlyProperty()
        {
            var db = CreateDb();

            //query to a readonly property
            try
            {
                var stmt0 = db.CreateStatement("SELECT 123 AS notwritable");
                var res0 = db.SelectObject<ConvertTypesObject>(stmt0);
                Assert.AreEqual(0, res0.notwritable);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }



        [TestMethod]
        public void ClrDbTypeConversion()
        {
            var db = CreateDb();

            db.MapDbToClrType(System.Data.DbType.AnsiString, (x) => string.Format("This is an ansi string. Original value: {0}", x));
            db.MapDbToClrType(System.Data.DbType.AnsiStringFixedLength, (x) => string.Format("This is a fixed length ansi string. Original value: {0}", x));

            var orig = new TestFields
            {
                strnn = "test ansi string",
                ustrnn = "test string",
                chrnn = "test chr string"
            };
            var newRec = Insert_TestFields(orig);

            var stmt1 = db.CreateSelectStatement<eTestFields>();
            stmt1.ResultMax = 1;
            var res1 = db.SelectObject<TestFields>(stmt1);
            Assert.IsTrue(res1.strnn.StartsWith("This is an ansi string."));

            var stmt2 = db.CreateStatement("SELECT 'junk'");
            var res2 = db.SelectScalar<string>(stmt2);
            Assert.IsTrue(res2.StartsWith("This is an ansi string."));

            var stmt3 = db.CreateStatement("SELECT N'junk'");
            var res3 = db.SelectScalar<string>(stmt3);
            Assert.IsFalse(res3.StartsWith("This is an ansi string."));

            var stmt4 = db.CreateStatement("SELECT cast ('junk' as char(100))");
            var res4 = db.SelectScalar<string>(stmt4);
            Assert.IsTrue(res4.StartsWith("This is a fixed length ansi string."));



            //try some statements before and after the mapping

            var stmt5_1a = db.CreateStatement("SELECT cast (' junk    ' as nchar(20))");
            var res5_1a = db.SelectScalar<string>(stmt5_1a);
            Assert.AreEqual(" junk               ", res5_1a);

            var stmt5_2a = db.CreateStatement("SELECT cast (NULL as nchar)");
            var res5_2a = db.SelectScalar<string>(stmt5_2a);
            Assert.AreEqual(null, res5_2a);


            db.MapDbToClrType(System.Data.DbType.StringFixedLength, (x) => (x ?? "").ToString().TrimEnd());


            var stmt5_1b = db.CreateStatement("SELECT cast (' junk    ' as nchar(20))");
            var res5_1b = db.SelectScalar<string>(stmt5_1b);
            Assert.AreEqual(" junk", res5_1b);


            var stmt5_2b = db.CreateStatement("SELECT cast (NULL as nchar)");
            var res5_2b = db.SelectScalar<string>(stmt5_2b);
            Assert.AreEqual("", res5_2b);

            var stmt6 = db.CreateStatement("SELECT cast (' junk    ' as nchar(20)) as field1, 100 as field2 UNION ALL SELECT cast (' junk2    ' as nchar(20)) as field1, 200 as field2;");
            var res6 = db.SelectDataTable(stmt6);
            Assert.AreEqual(2, res6.Rows.Count);
            Assert.AreEqual(2, res6.Columns.Count);
            Assert.AreEqual(" junk", res6.Rows[0]["field1"]);

            var stmt7 = db.CreateStatement("SELECT cast (' junk    ' as nchar(20)) as field1; SELECT cast (' junk    ' as nchar(20)) as field1, 100 as field2;");
            var res7 = db.SelectDataSet(stmt7);
            Assert.AreEqual(2, res7.Tables.Count);
            Assert.AreEqual(1, res7.Tables[0].Columns.Count);
            Assert.AreEqual(2, res7.Tables[1].Columns.Count);
            Assert.AreEqual(" junk", res7.Tables[0].Rows[0]["field1"]);
            Assert.AreEqual(100, res7.Tables[1].Rows[0]["field2"]);


            //test fill datatable with different field order
            var stmt8 = db.CreateStatement("SELECT cast (' junk    ' as nchar(20)) as field1, 100 as field2");
            var res8 = new DataTable();
            res8.Columns.Add("field2");
            res8.Columns.Add("field1");
            db.SelectIntoDataTable(stmt8, res8);
            Assert.AreEqual(1, res8.Rows.Count);
            Assert.AreEqual(2, res8.Columns.Count);
            Assert.AreEqual(" junk", res8.Rows[0]["field1"]);

        }


        [TestMethod]
        public void ClrSystemTypeConversion()
        {
            var db = CreateDb();

            db.MapDbToClrType(typeof(string), (x) => string.Format("Translated string. Original value: {0}", x));

            var orig = new TestFields
            {
                strnn = "test ansi string",
                ustrnn = "test string",
                chrnn = "test chr string"
            };
            var newRec = Insert_TestFields(orig);

            var stmt1 = db.CreateSelectStatement<eTestFields>();
            stmt1.ResultMax = 1;
            var res1 = db.SelectObject<TestFields>(stmt1);
            Assert.IsTrue(res1.strnn.StartsWith("Translated string."));
            Assert.IsTrue(res1.ustrnn.StartsWith("Translated string."));
            Assert.IsTrue(res1.chrnn.StartsWith("Translated string."));

        }

        class ValidationWarningsObj
        {
            public int int_1 { get; set; }
            public string str1 { get; set; }
            public DateTime dt1 { get; set; }
            public DateTime? dt2 { get; set; }
        }

        class ValidationWarningsDupObj
        {
            public int int_1 { get; set; }
            public string str1 { get; set; }
            public string STR1 { get; set; }
            public DateTime dt1 { get; set; }
            public DateTime? dt2 { get; set; }
        }

        [TestMethod]
        public void ValidationWarnings()
        {
            var db = CreateDb();

            var orig = new TestFields
            {
                strnn = "test string",
                intnn = 5,
                dtnn = DateTime.Today
            };
            var newRec = Insert_TestFields(orig);

            var trackWarnings = new List<string>();
            db.OnValidationWarning += (object sender, Rational.DB.Database.ValidationWarningEventArgs args) => { trackWarnings.Add(args.Summary); };

            //different type
            var stmt1 = db.CreateStatement("SELECT 100");
            var res1 = db.SelectScalar<decimal>(stmt1);
            Assert.AreEqual(1, trackWarnings.Count);
            Assert.AreEqual(100m, res1);

            trackWarnings.Clear();

            //different case should not be error
            var stmt2 = db.CreateStatement("SELECT intnn as InT_1, strnn as StR1, dtnn as dT1, dtnn as dt2 FROM TestFields");
            var res2 = db.SelectObject<ValidationWarningsObj>(stmt2);
            Assert.AreEqual(0, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res2.str1);

            trackWarnings.Clear();

            //if nulls are typed, should not be warning
            var stmt3 = db.CreateStatement("SELECT intnn as int_1, strnn as StR1, dt as dT1, dt as dt2 FROM TestFields");
            var res3 = db.SelectObject<ValidationWarningsObj>(stmt3);
            Assert.AreEqual(0, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res3.str1);
            Assert.AreEqual(DateTime.MinValue, res3.dt1);
            Assert.AreEqual(null, res3.dt2);

            trackWarnings.Clear();

            //different type
            var stmt4 = db.CreateStatement("SELECT intnn as int_1, dtnn as StR1, dtnn as dT1, dtnn as dt2 FROM TestFields");
            var res4 = db.SelectObject<ValidationWarningsObj>(stmt4);
            Assert.AreEqual(1, trackWarnings.Count);
            Assert.AreNotEqual(orig.strnn, res4.str1);

            trackWarnings.Clear();

            //extra column
            var stmt5 = db.CreateStatement("SELECT intnn as int_1, strnn as StR1, dtnn as dT1, dtnn as dt2, strnn as junk FROM TestFields");
            var res5 = db.SelectObject<ValidationWarningsObj>(stmt5);
            Assert.AreEqual(1, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res5.str1);

            trackWarnings.Clear();

            //missing column
            var stmt6 = db.CreateStatement("SELECT strnn as StR1, dtnn as dT1, dtnn as dt2 FROM TestFields");
            var res6 = db.SelectObject<ValidationWarningsObj>(stmt6);
            Assert.AreEqual(1, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res6.str1);

            trackWarnings.Clear();

            //different order not warning
            var stmt7 = db.CreateStatement("SELECT dtnn as dT1, strnn as StR1, dtnn as dt2, intnn as int_1 FROM TestFields");
            var res7 = db.SelectObject<ValidationWarningsObj>(stmt7);
            Assert.AreEqual(0, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res7.str1);

            trackWarnings.Clear();

            //dup CLR
            var stmt8 = db.CreateStatement("SELECT dtnn as dT1, strnn as StR1, dtnn as dt2, intnn as int_1 FROM TestFields");
            var res8 = db.SelectObject<ValidationWarningsDupObj>(stmt8);
            Assert.AreEqual(1, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res8.str1);
            Assert.AreEqual(orig.strnn, res8.STR1);

            trackWarnings.Clear();

            //dup DB
            var stmt9 = db.CreateStatement("SELECT dtnn as dT1, strnn as StR1, strnn as StR1, dtnn as dt2, intnn as int_1 FROM TestFields");
            var res9 = db.SelectObject<ValidationWarningsObj>(stmt9);
            Assert.AreEqual(1, trackWarnings.Count);
            Assert.AreEqual(orig.strnn, res9.str1);
        }




        [TestMethod]
        public void SelectIntoDataTable()
        {
            var created = Insert_Person(10);
            var db = CreateDb();

            var trackWarnings = new List<string>();
            db.OnValidationWarning += (object sender, Rational.DB.Database.ValidationWarningEventArgs args) => { trackWarnings.Add(args.Summary); };

            //warnings for missing clr fields
            var stmt1 = db.CreateSelectStatement<ePerson>();
            stmt1.ResultMax = 10;
            var dt1 = new DataTable();
            db.SelectIntoDataTable(stmt1, dt1);
            Assert.AreEqual(10, dt1.Rows.Count);
            Assert.AreEqual(6, dt1.Columns.Count);
            Assert.AreEqual(6, trackWarnings.Count);

            trackWarnings.Clear();

            //warning for extra clr field
            var stmt2 = db.CreateSelectStatement<ePerson>();
            stmt2.ResultMax = 10;
            var dt2 = new DataTable();
            dt2.Columns.Add("junk");
            dt2.Columns.Add("Id", typeof(Int32));
            dt2.Columns.Add("Status", typeof(string));
            dt2.Columns.Add("Name", typeof(string));
            dt2.Columns.Add("Score", typeof(Int32));
            dt2.Columns.Add("Salary", typeof(decimal));
            dt2.Columns.Add("BirthDate", typeof(DateTime));
            db.SelectIntoDataTable(stmt2, dt2);
            Assert.AreEqual(10, dt2.Rows.Count);
            Assert.AreEqual(7, dt2.Columns.Count);
            Assert.AreEqual(1, trackWarnings.Count);

            trackWarnings.Clear();

            //all good
            var stmt3 = db.CreateSelectStatement<ePerson>();
            stmt3.ResultMax = 10;
            var dt3 = new DataTable();
            dt3.Columns.Add("Id", typeof(Int32));
            dt3.Columns.Add("Status", typeof(string));
            dt3.Columns.Add("Name", typeof(string));
            dt3.Columns.Add("Score", typeof(Int32));
            dt3.Columns.Add("Salary", typeof(decimal));
            dt3.Columns.Add("BirthDate", typeof(DateTime));
            db.SelectIntoDataTable(stmt3, dt3);
            Assert.AreEqual(10, dt3.Rows.Count);
            Assert.AreEqual(6, dt3.Columns.Count);
            Assert.AreEqual(0, trackWarnings.Count);

            trackWarnings.Clear();

            //different case, still good
            var stmt4 = db.CreateSelectStatement<ePerson>();
            stmt4.ResultMax = 10;
            var dt4 = new DataTable();
            dt4.Columns.Add("Id", typeof(Int32));
            dt4.Columns.Add("Status", typeof(string));
            dt4.Columns.Add("NAME", typeof(string));
            dt4.Columns.Add("SCore", typeof(Int32));
            dt4.Columns.Add("salARY", typeof(decimal));
            dt4.Columns.Add("BirtHDAte", typeof(DateTime));
            db.SelectIntoDataTable(stmt4, dt4);
            Assert.AreEqual(10, dt4.Rows.Count);
            Assert.AreEqual(6, dt4.Columns.Count);
            Assert.AreEqual(0, trackWarnings.Count);

            trackWarnings.Clear();

            //warning for incorrect type on Id
            var stmt5 = db.CreateSelectStatement<ePerson>();
            stmt5.ResultMax = 10;
            var dt5 = new DataTable();
            dt5.Columns.Add("Id", typeof(string));
            dt5.Columns.Add("Status", typeof(string));
            dt5.Columns.Add("Name", typeof(string));
            dt5.Columns.Add("Score", typeof(Int32));
            dt5.Columns.Add("Salary", typeof(decimal));
            dt5.Columns.Add("BirthDate", typeof(DateTime));
            db.SelectIntoDataTable(stmt5, dt5);
            Assert.AreEqual(10, dt3.Rows.Count);
            Assert.AreEqual(6, dt3.Columns.Count);
            Assert.AreEqual(1, trackWarnings.Count);

        }



        [Table(SqlName = "Person")]
        public enum ePerson_ObjectTranslation
        {
            [Column(PrimaryKey = true, AutoIncrement = true, ClrName = "Id_new")]
            Id,
            [Column(NotNull = true, ClrName = "Id_new")]
            Status,
            [Column(NotNull = true, ClrName = "Name_new")]
            Name,
            [Column(NotNull = true, ClrName = "Score_new")]
            Score,
            [Column(NotNull = true, ClrName = "Salary_new")]
            Salary,
            BirthDate
        }

        public class Person_ObjectTranslation
        {
            public int Id_new { get; set; }
            public string Status_new { get; set; }
            public string Name_new { get; set; }
            public Int32 Score_new { get; set; }
            public decimal Salary_new { get; set; }
            public DateTime BirthDate { get; set; }
        }

        [TestMethod]
        public void ObjectTranslation()
        {
            var created = Insert_Person(3);
            var db = CreateDb();

            //standard, no translation
            var stmt1 = db.CreateSelectStatement<ePerson>();
            stmt1.Where.And("Id = @0", created[0].Id);
            var res1 = db.SelectObject<Person>(stmt1);
            Assert.AreEqual(created[0].Name, res1.Name);
            Assert.AreEqual(created[0].BirthDate, res1.BirthDate);


            //translate in select
            var stmt2 = db.CreateSelectStatement<ePerson_ObjectTranslation>();
            stmt2.Where.And("Id = @0", created[1].Id);
            var res2 = db.SelectObject<Person_ObjectTranslation>(stmt2);
            Assert.AreEqual(created[1].Name, res2.Name_new);
            Assert.AreEqual(created[1].BirthDate, res2.BirthDate);


            //translate in CreateObject
            var stmt3 = db.CreateStatement("SELECT * FROM Person WHERE Id = @0", created[2].Id);
            var res3 = db.SelectObject<Person_ObjectTranslation, ePerson_ObjectTranslation>(stmt3);
            Assert.AreEqual(created[2].Name, res3.Name_new);
            Assert.AreEqual(created[2].BirthDate, res3.BirthDate);


            //translate in CreateObjectList
            var stmt4 = db.CreateStatement("SELECT * FROM Person WHERE Id = @0", created[0].Id);
            var res4 = db.SelectObjectList<Person_ObjectTranslation, ePerson_ObjectTranslation>(stmt4);
            Assert.AreEqual(created[0].Name, res4[0].Name_new);
            Assert.AreEqual(created[0].BirthDate, res4[0].BirthDate);
        }




        #region "CTE"

        public class PersonWithValedictorian : Person
        {
            public string ValedictorianName { get; set; }
            public int ValedictorianScore { get; set; }
        }


        [TestMethod]
        public void CTEs()
        {
            var created = Insert_Person(7);
            var createdIds = (from i in created select i.Id).ToList();
            var db = CreateDb();
            var stmt1 = db.CreateSelectStatement();

            stmt1.WithCte.Add("PersonWithScoreRankByBirthYear",
                db.CreateStatement("SELECT *, ROW_NUMBER() OVER (PARTITION BY YEAR(BirthDate) ORDER BY Score DESC) AS rn FROM Person"));

            stmt1.From.AppendRaw(@"
Person p
LEFT JOIN PersonWithScoreRankByBirthYear pwr 
	ON YEAR(pwr.BirthDate) = YEAR(p.BirthDate)
	AND pwr.rn = 1
");

            stmt1.Fields.Add<ePerson>("p");
            stmt1.Fields.Add("Name", "pwr", "ValedictorianName");
            stmt1.Fields.Add("Score", "pwr", "ValedictorianScore");
            stmt1.Where.And("p.Id IN (@0)", createdIds);
            stmt1.OrderBy.Add(ePerson.Id, SqlFragmentOrderBy.eOrderByDirection.ASC, "p");


            stmt1.ResultMax = 3;

            var res1 = db.SelectObjectList<PersonWithValedictorian>(stmt1);
            Assert.AreEqual(3, res1.Count);
            Assert.AreEqual(created[0].Id, res1[0].Id);
            Assert.IsTrue(res1[0].ValedictorianScore >= res1[0].Score);

            var stmt2 = new Rational.DB.DbSelectPager(stmt1);
            stmt2.ResultMax = 3;
            stmt2.ResultPage = 1;

            var res2 = db.SelectObjectList<PersonWithValedictorian>(stmt2);
            Assert.AreEqual(3, res2.Count);
            Assert.AreEqual(created[0].Id, res2[0].Id);
            Assert.IsTrue(res2[0].ValedictorianScore >= res2[0].Score);

            var stmt3 = new Rational.DB.DbSelectPager(stmt1);
            stmt3.ResultMax = 3;
            stmt3.ResultPage = 2;
            var res3 = db.SelectObjectList<PersonWithValedictorian>(stmt3);
            Assert.AreEqual(3, res3.Count);
            Assert.AreEqual(created[3].Id, res3[0].Id);
            Assert.IsTrue(res3[0].ValedictorianScore >= res3[0].Score);


        }


        #endregion



        #region "Case sensitivity"



        [Table(SqlName = "Person")]
        public enum ePerson_ChangeCase
        {
            [Column(PrimaryKey = true, AutoIncrement = true)]
            ID,
            [Column(NotNull = true)]
            status,
            [Column(NotNull = true)]
            NaMe,
            [Column(NotNull = true)]
            sCORE,
            [Column(NotNull = true)]
            SaLARy,
            [Column(NotNull = true)]
            BirthdatE
        }

        public class Person_ChangeCase
        {
            public int id { get; set; }
            public string sTaTus { get; set; }
            public string nAMe { get; set; }
            public Int32 scorE { get; set; }
            public decimal SALARY { get; set; }
            public DateTime birthDATE { get; set; }
        }


        [TestMethod]
        public void CaseInsensitiveFields()
        {
            var created = Insert_Person(1);
            var db = CreateDb();

            //check that fields still map with different case
            var stmt1 = db.CreateSelectStatement<ePerson_ChangeCase>();
            stmt1.Where.And("Id = @0", created[0].Id);
            var res1 = db.SelectObject<Person>(stmt1);
            Assert.AreEqual(created[0].Name, res1.Name);
            Assert.AreEqual(created[0].BirthDate, res1.BirthDate);


            //check the other way
            var stmt2 = db.CreateSelectStatement<ePerson>();
            stmt2.Where.And("Id = @0", created[0].Id);
            var res2 = db.SelectObject<Person_ChangeCase>(stmt2);
            Assert.AreEqual(created[0].Name, res2.nAMe);
            Assert.AreEqual(created[0].BirthDate, res2.birthDATE);
        }


        [TestMethod]
        public void CaseInsensitiveParameters()
        {
            var created = Insert_Person(1);
            var db = CreateDb();

            //check SelectByKey with different case
            var res1 = db.SelectByKey<ePerson_ChangeCase, Person>(new Person { Id = created[0].Id });
            Assert.AreEqual(created[0].Name, res1.Name);
            Assert.AreEqual(created[0].BirthDate, res1.BirthDate);


            //check the other way
            var res2 = db.SelectByKey<ePerson, Person_ChangeCase>(new Person_ChangeCase { id = created[0].Id });
            Assert.AreEqual(created[0].Name, res2.nAMe);
            Assert.AreEqual(created[0].BirthDate, res2.birthDATE);


            //with anonymous
            var res3 = db.SelectByKey<ePerson, Person>(new { iD = created[0].Id });
            Assert.AreEqual(created[0].Name, res3.Name);
            Assert.AreEqual(created[0].BirthDate, res3.BirthDate);


            //more fields
            var stmt4 = db.CreateSelectStatement<ePerson>();
            stmt4.Where.And("Id = @id AND Name = @nAMe AND BirthDate = @bIrthDAtE", created[0]);
            var res4 = db.SelectObject<Person>(stmt4);
            Assert.AreEqual(created[0].Name, res4.Name);
            Assert.AreEqual(created[0].BirthDate, res4.BirthDate);


            //duplicate fields with anonymous, should take correct case first
            var res5 = db.SelectByKey<ePerson, Person>(new { Id = created[0].Id, ID = -1 });
            Assert.AreEqual(created[0].Name, res5.Name);
            Assert.AreEqual(created[0].BirthDate, res5.BirthDate);

            //duplicate fields with anonymous 2, should take correct case first
            var res6 = db.SelectByKey<ePerson, Person>(new { id = -1, Id = created[0].Id });
            Assert.AreEqual(created[0].Name, res6.Name);
            Assert.AreEqual(created[0].BirthDate, res6.BirthDate);

            //duplicate fields with anonymous 3, correct case is incorrect value
            var res7 = db.SelectByKey<ePerson, Person>(new { id = created[0].Id, Id = -2 });
            Assert.AreEqual(null, res7);


            //with dictionary
            var dict8 = new Dictionary<string, object>();
            dict8.Add("ID", created[0].Id);
            var res8 = db.SelectByKey<ePerson, Person>(dict8);
            Assert.AreEqual(created[0].Name, res8.Name);
            Assert.AreEqual(created[0].BirthDate, res8.BirthDate);


            //duplicate with dictionary, correct case is correct value
            var dict9 = new Dictionary<string, object>();
            dict9.Add("ID", -1);
            dict9.Add("Id", created[0].Id);
            var res9 = db.SelectByKey<ePerson, Person>(dict9);
            Assert.AreEqual(created[0].Name, res9.Name);
            Assert.AreEqual(created[0].BirthDate, res9.BirthDate);

            //duplicate with dictionary, correct case is incorrect value
            var dict10 = new Dictionary<string, object>();
            dict10.Add("ID", created[0].Id);
            dict10.Add("Id", -1);
            var res10 = db.SelectByKey<ePerson, Person>(dict10);
            Assert.AreEqual(null, res10);

        }


        #endregion



    }
}

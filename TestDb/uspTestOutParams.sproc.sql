CREATE PROCEDURE uspTestOutParams
	@instring nvarchar(30),
	@inint int,
	@outstring nvarchar(100) OUT,
	@outint int OUT,
	@outdt datetime OUT,
	@outnum decimal(5,2) OUT,
	@inoutstring nvarchar(100) OUT
AS
BEGIN

SELECT @outstring = 'instring: ' + @instring;
SELECT @inoutstring = @inoutstring + ' out';
SELECT @outint = @inint + 100;
SELECT @outdt = GETDATE();
SELECT @outnum = @inint + 100.5;

END;

CREATE TABLE Person(
	Id int IDENTITY(1,1) NOT NULL,
	Status varchar(5) NOT NULL,
	Name nvarchar(100) NOT NULL,
	Score int NOT NULL,
	Salary decimal(9,2) NOT NULL,
	BirthDate datetime NOT NULL,
 CONSTRAINT PK_Person PRIMARY KEY ( Id ASC )
);

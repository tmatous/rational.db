CREATE TABLE TestFields(
	Id int NOT NULL,
	str varchar(50) NULL,
	ustr nvarchar(50) NULL,
	`dec` decimal(18, 0) NULL,
	dt datetime NULL,
	boo bit NULL,
	`int` int NULL,
	strnn varchar(50) NOT NULL,
	ustrnn nvarchar(50) NOT NULL,
	decnn decimal(18, 0) NOT NULL,
	dtnn datetime NOT NULL,
	boonn bit NOT NULL,
	intnn int NOT NULL,
	bin varbinary(50) NULL,
	chr char(100) NULL,
	chrnn char(100) NOT NULL,
 PRIMARY KEY ( Id )
);

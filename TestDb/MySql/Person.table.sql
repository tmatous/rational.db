CREATE TABLE Person(
	Id int NOT NULL AUTO_INCREMENT,
	Status varchar(5) NOT NULL,
	Name nvarchar(100) NOT NULL,
	Score int NOT NULL,
	Salary decimal(9,2) NOT NULL,
	BirthDate datetime NOT NULL,
 PRIMARY KEY ( Id )
);

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Rational.DB;

namespace TestApp
{
    public class TestMethods : ITestMethods
    {

        public string TestCollectionName { get; set; }
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();


        public void InitTestEnvironment()
        {
        }

        public void CleanupTestEnvironment()
        {
        }

        private Database CreateDbSqlServer()
        {
            var ci = new DbConnectionInfo("Server=(local)\\SQLEXPRESS;Database=TestDb;Trusted_Connection=true", eDbType.SqlServer);
            var db = new Database(ci);
            db.OnExecuteComplete += db_ExecuteComplete;
            db.OnBeforeExecute += db_BeforeExecute;
            return db;
        }

        private Database CreateDbOracle()
        {
            var ci = new DbConnectionInfo("Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.106)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=xe)));User Id=system;Password=Password1;", eDbType.Oracle);
            var db = new Database(ci);
            db.OnExecuteComplete += db_ExecuteComplete;
            db.OnBeforeExecute += db_BeforeExecute;
            return db;
        }

        private Database CreateDbSqlCe()
        {
            var ci = new DbConnectionInfo("Data Source=SqlCeTest.sdf;Persist Security Info=False;", eDbType.SqlServerCe);
            var db = new Database(ci);
            db.OnExecuteComplete += db_ExecuteComplete;
            db.OnBeforeExecute += db_BeforeExecute;
            return db;
        }

        private void db_ExecuteComplete(object sender, Database.ExecuteCompleteEventArgs args)
        {
            Debug.Print(args.Sql);
        }

        private void db_BeforeExecute(object sender, Database.BeforeExecuteEventArgs args)
        {
            Debug.Print(args.Sql);
        }


        class StatementsTestData
        {
            public string test1 { get; set; }
            public DateTime test2;
        }

        public void Statements()
        {
            var sql = "UPDATE Junk SET [Test1] = @test1, [Test2] = @test2";
            var stmt = new Rational.DB.DbStatement();
            var dat = new Dictionary<string, object>();
            dat.Add("test1", "junk");
            dat.Add("test2", DateTime.UtcNow);
            stmt.Statement.Append(sql, dat);


            stmt = new Rational.DB.DbStatement();
            var dat2 = new StatementsTestData
            {
                test1 = "junk2",
                test2 = DateTime.UtcNow
            };
            stmt.Statement.Append(sql, dat2);
        }

        public void Selects()
        {
            var db = CreateDbSqlServer();

            var dtstmt = db.CreateSelectStatement();
            dtstmt.From.Append(dtstmt.TableIdentifier<Tables.eTestFields>());
            dtstmt.Fields.Add<Tables.eTestFields>();
            dtstmt.ResultMax = 10;
            var dt = db.SelectDataTable(dtstmt);
            log.Debug("Count: {0}", dt.Rows.Count);

            var stmt = new DbStatement("SELECT COUNT(*) FROM TestFields");
            var cnt = db.SelectScalar<Int32>(stmt);
            log.Debug("Count: {0}", cnt);

            var stmt2 = new DbStatement("SELECT TOP 1 * FROM TestFields");
            var obj = db.SelectObject<BO.TestFields>(stmt2, r =>
            {
                var res = new BO.TestFields();
                res.Id = r.GetInt32("Id");
                res.str = r.GetString("str");
                res.ustr = r.GetString("ustr");
                return res;
            });
            log.Debug(XSharper.Core.Dump.ToDump(obj, "first row #1"));

            var obj2 = db.SelectObject<BO.TestFields>(stmt2, CreateTestFields);
            log.Debug(XSharper.Core.Dump.ToDump(obj2, "first row #2"));

            var obj3 = db.SelectObject<BO.TestFields>(stmt2);
            log.Debug(XSharper.Core.Dump.ToDump(obj3, "first row #3"));

            var stmt3 = new DbStatement("SELECT * FROM TestFields");
            var objs = db.SelectObjectList<BO.TestFields>(stmt3, CreateTestFields);
            log.Debug("Selected {0} objects #1", objs.Count);
            var objs2 = db.SelectObjectList<BO.TestFields>(stmt3);
            log.Debug("Selected {0} objects #2", objs2.Count);

            var stmt4 = new DbSelectStatement();
            stmt4.Fields.Add<Tables.eTestFields>();
            stmt4.From.Append("TestFields");
            stmt4.OrderBy.Add("str");
            log.Debug(stmt4.GetSql());

            var stmt5 = new DbSelectStatement();
            stmt5.Fields.Add<Tables.eTestFields>();
            stmt5.From.Append("TestFields");
            stmt5.Where.AppendWithIdentifiers("{Id} = @Id", new { Id = Tables.eTestFields.Id }, new { Id = 1 });
            stmt5.OrderBy.Add("str");
            log.Debug(stmt5.GetSql());

            var stmt6 = db.CreateSelectStatement<Tables.eTestFields>();
            stmt5.Where.AndWithIdentifiers("{str} = @str", new { str = Tables.eTestFields.str }, new { str = "test123" });
            stmt5.Where.AndWithIdentifiers("{dt} = @dt", new { dt = Tables.eTestFields.dt }, new { dt = DateTime.UtcNow });
            stmt5.OrderBy.Add("str");
            log.Debug(stmt5.GetSql());
        }

        public void MVP()
        {
            var db = CreateDbSqlServer();

            var stmt = db.CreateStatement<DbStatement>();
            stmt.Statement.Append("SELECT * FROM TestFields WHERE Id IN (@0)", new int[] { 100, 101, 102 });
            var objs = db.SelectObjectList<BO.TestFields>(stmt, CreateTestFields);
            log.Debug("Selected {0} objects", objs.Count);

            var stmt2 = db.CreateStatement<DbStatement>();
            stmt2.Statement.Append("SELECT * FROM TestFields WHERE str IN (@0)", new string[] { "insert test 8", "insert test 9", "test3" });
            var objs2 = db.SelectObjectList<BO.TestFields>(stmt2, CreateTestFields);
            log.Debug("Selected {0} objects", objs2.Count);
        }

        public void SelectsOracle()
        {
            var db = CreateDbOracle();

            var cntstmt = db.CreateStatement<DbStatement>();
            cntstmt.Statement.Append("SELECT COUNT(*) FROM TestFields");
            var cnt = db.SelectScalar<decimal>(cntstmt);
            log.Debug("Count: {0}", cnt);

            var stmt = db.CreateSelectStatement<Tables.eTestFieldsOracle>();
            stmt.ResultMax = 2;
            var dt = db.SelectDataTable(stmt);
            log.Debug("Count: {0}", dt.Rows.Count);

            stmt.ResultMax = 1;
            var obj = db.SelectObject<BO.TestFieldsOracle>(stmt, CreateTestFieldsOracle);
            log.Debug("Using CreateTestFields, {0}", XSharper.Core.Dump.ToDump(obj, "result"));

            var obj2 = db.SelectObject<BO.TestFieldsOracle>(stmt);
            log.Debug("Using inferred method, {0}", XSharper.Core.Dump.ToDump(obj2, "result"));

            stmt.ResultMax = 2;
            var objs = db.SelectObjectList<BO.TestFieldsOracle>(stmt);
            log.Debug("Selected {0} objects, {1}", objs.Count, XSharper.Core.Dump.ToDump(objs, "result"));

            stmt.OrderBy.Add("str");
            log.Debug(stmt.GetSql());
            var objs2 = db.SelectObjectList<BO.TestFieldsOracle>(stmt);
            log.Debug("Selected {0} objects, {1}", objs2.Count, XSharper.Core.Dump.ToDump(objs2, "result"));

            stmt.Where.AppendWithIdentifiers("{Id} = @Id", new { Id = Tables.eTestFieldsOracle.Id }, new { Id = 1 });
            log.Debug(stmt.GetSql());
            var objs3 = db.SelectObjectList<BO.TestFieldsOracle>(stmt);
            log.Debug("Selected {0} objects, {1}", objs3.Count, XSharper.Core.Dump.ToDump(objs3, "result"));

            var stmt2 = db.CreateSelectStatement<Tables.eTestFieldsOracle>();
            stmt2.ResultMax = 2;
            stmt2.Where.AndWithIdentifiers("{str} = @str", new { str = Tables.eTestFieldsOracle.str }, new { str = "test123" });
            stmt2.Where.AndWithIdentifiers("{dt} = @dt", new { dt = Tables.eTestFieldsOracle.dt }, new { dt = DateTime.UtcNow });
            stmt2.OrderBy.Add("str");
            log.Debug(stmt2.GetSql());
            var objs4 = db.SelectObjectList<BO.TestFieldsOracle>(stmt2);
            log.Debug("Selected {0} objects, {1}", objs4.Count, XSharper.Core.Dump.ToDump(objs4, "result"));
        }

        public void SelectsSqlCe()
        {
            var db = CreateDbSqlCe();

            var cntstmt = db.CreateStatement<DbStatement>();
            cntstmt.Statement.Append("SELECT COUNT(*) FROM TestFields");
            var cnt = db.SelectScalar<decimal>(cntstmt);
            log.Debug("Count: {0}", cnt);

            var stmt = db.CreateSelectStatement<Tables.eTestFieldsSqlCe>();
            stmt.ResultMax = 2;
            var dt = db.SelectDataTable(stmt);
            log.Debug("Count: {0}", dt.Rows.Count);

            stmt.ResultMax = 1;
            var obj = db.SelectObject<BO.TestFieldsSqlCe>(stmt, CreateTestFieldsSqlCe);
            log.Debug("Using CreateTestFields, {0}", XSharper.Core.Dump.ToDump(obj, "result"));

            var obj2 = db.SelectObject<BO.TestFieldsSqlCe>(stmt);
            log.Debug("Using inferred method, {0}", XSharper.Core.Dump.ToDump(obj2, "result"));

            stmt.ResultMax = 2;
            var objs = db.SelectObjectList<BO.TestFieldsSqlCe>(stmt);
            log.Debug("Selected {0} objects, {1}", objs.Count, XSharper.Core.Dump.ToDump(objs, "result"));

            stmt.OrderBy.Add(Tables.eTestFieldsSqlCe.ustr);
            log.Debug(stmt.GetSql());
            var objs2 = db.SelectObjectList<BO.TestFieldsSqlCe>(stmt);
            log.Debug("Selected {0} objects, {1}", objs2.Count, XSharper.Core.Dump.ToDump(objs2, "result"));

            stmt.Where.AppendWithIdentifiers("{Id} = @Id", new { Id = Tables.eTestFieldsSqlCe.Id }, new { Id = 1 });
            log.Debug(stmt.GetSql());
            var objs3 = db.SelectObjectList<BO.TestFieldsSqlCe>(stmt);
            log.Debug("Selected {0} objects, {1}", objs3.Count, XSharper.Core.Dump.ToDump(objs3, "result"));

            var stmt2 = db.CreateSelectStatement<Tables.eTestFieldsSqlCe>();
            stmt2.ResultMax = 2;
            stmt2.Where.AndWithIdentifiers("{ustr} = @ustr", new { ustr = Tables.eTestFieldsSqlCe.ustr }, new { ustr = "test123" });
            stmt2.Where.AndWithIdentifiers("{dt} = @dt", new { dt = Tables.eTestFieldsSqlCe.dt }, new { dt = DateTime.UtcNow });
            stmt2.OrderBy.Add(Tables.eTestFieldsSqlCe.ustr);
            log.Debug(stmt2.GetSql());
            var objs4 = db.SelectObjectList<BO.TestFieldsSqlCe>(stmt2);
            log.Debug("Selected {0} objects, {1}", objs4.Count, XSharper.Core.Dump.ToDump(objs4, "result"));
        }

        public void AutoGenSql()
        {
            var db = CreateDbSqlServer();

            var obj = new BO.TestFields { str = "insert test 1", ustr = "blah", boo = true, dt = DateTime.UtcNow };

            var ins = db.CreateInsertStatement<Tables.eTestFields>(obj);
            log.Debug(ins.GetSql());

            var sel1 = db.CreateSelectStatement<Tables.eTestFields>();
            log.Debug(sel1.GetSql());

            var upd = db.CreateUpdateStatement<Tables.eTestFields>(obj);
            log.Debug(upd.GetSql());

            var sel2 = db.CreateSelectStatement<Tables.eTestFields>();
            log.Debug(sel2.GetSql());

            var dl = db.CreateDeleteStatement<Tables.eTestFields>(obj);
            log.Debug(dl.GetSql());
        }

        private BO.TestFields CreateTestFields(DbRow r)
        {
            var res = new BO.TestFields();
            res.Id = r.GetInt32("Id");
            res.str = r.GetString("str");
            res.ustr = r.GetString("ustr");
            return res;
        }

        private BO.TestFieldsOracle CreateTestFieldsOracle(DbRow r)
        {
            var res = new BO.TestFieldsOracle();
            res.Id = r.GetInt32("Id");
            res.str = r.GetString("str");
            res.ustr = r.GetString("ustr");
            return res;
        }

        private BO.TestFieldsSqlCe CreateTestFieldsSqlCe(DbRow r)
        {
            var res = new BO.TestFieldsSqlCe();
            res.Id = r.GetInt32("Id");
            res.ustr = r.GetString("ustr");
            return res;
        }



        public void Attributes()
        {
            log.Debug("TableSqlName : {0}", Rational.DB.TableDefinitionAttributes.GetTableSqlName<Tables.eTestFields>());
            log.Debug("ColumnClrName(clrint) : {0}", Rational.DB.TableDefinitionAttributes.GetColumnClrName(Tables.eTestFields.clrint));
            log.Debug("ColumnSqlName(int) : {0}", Rational.DB.TableDefinitionAttributes.GetColumnSqlName(Tables.eTestFields.clrint));
            log.Debug("ColumnSqlName(str) : {0}", Rational.DB.TableDefinitionAttributes.GetColumnSqlName(Tables.eTestFields.str));
            log.Debug("IsPrimaryKey(Id) : {0}", Rational.DB.TableDefinitionAttributes.GetColumnAttribute(Tables.eTestFields.Id).PrimaryKey);
            log.Debug("IsPrimaryKey(str) : {0}", Rational.DB.TableDefinitionAttributes.GetColumnAttribute(Tables.eTestFields.str).PrimaryKey);
        }


        public void Inserts()
        {
            var db = CreateDbSqlServer();
            for (int i = 0; i < 100; i++)
            {
                var obj = new BO.TestFields
                {
                    str = string.Format("insert test {0}", i),
                    ustr = "blah",
                    boo = true,
                    dt = DateTime.UtcNow,
                    dtnn = DateTime.UtcNow
                };
                db.Insert<Tables.eTestFields>(obj);
                log.Debug("Inserted, id: {0}", obj.Id);
            }
        }


        public void InsertsOracle()
        {
            var db = CreateDbOracle();
            var idStmt = db.CreateStatement<DbStatement>();
            idStmt.Statement.Append("SELECT TESTFIELDS_ID.nextval FROM dual");
            for (int i = 0; i < 100; i++)
            {
                var obj = new BO.TestFieldsOracle
                {
                    str = string.Format("insert test {0}", i),
                    ustr = "blah",
                    boo = 'Y',
                    dt = DateTime.UtcNow,
                    dtnn = DateTime.UtcNow
                };
                obj.Id = db.SelectScalar<Int32>(idStmt);
                db.Insert<Tables.eTestFieldsOracle>(obj);
                log.Debug("Inserted, id: {0}", obj.Id);
            }
        }

        public void InsertsSqlCe()
        {
            var db = CreateDbSqlCe();
            using (var trans = db.BeginTransaction())
            {
                for (int i = 0; i < 100; i++)
                {
                    var obj = new BO.TestFieldsSqlCe
                    {
                        ustr = string.Format("insert test {0}", i),
                        boo = true,
                        dt = DateTime.UtcNow,
                        dtnn = DateTime.UtcNow
                    };
                    db.Insert<Tables.eTestFieldsSqlCe>(obj);
                    log.Debug("Inserted, id: {0}", obj.Id);
                }
                trans.Commit();
            }
        }

        public void Updates()
        {
            var db = CreateDbSqlServer();
            var targetid = 0;
            var changestr = string.Format("Updated {0}", DateTime.Now);

            var sel = db.CreateSelectStatement<Tables.eTestFields>();
            sel.ResultMax = 10;
            var objs = db.SelectObjectList<BO.TestFields>(sel);
            if (objs.Count == 0)
            {
                log.Debug("No records found to update");
                return;
            }

            var obj = objs[0];
            targetid = obj.Id;
            obj.str = changestr;
            db.Update<Tables.eTestFields>(obj);

            var sel2 = db.CreateSelectStatement<Tables.eTestFields>();
            sel2.Where.Append("[Id] = @0", targetid);
            var testObj = db.SelectObject<BO.TestFields>(sel2);

            log.Debug("Updated id: {0}, value: {1}, equal: {2}", targetid, changestr, changestr.Equals(testObj.str));

            var testObj2 = db.SelectByKey<Tables.eTestFields, BO.TestFields>(targetid);

            log.Debug("Updated id: {0}, value: {1}, equal: {2}", targetid, changestr, changestr.Equals(testObj2.str));
        }

        public void Deletes()
        {
            var db = CreateDbSqlServer();

            var cntStmt = new DbStatement();
            cntStmt.Statement.Append(string.Format("SELECT COUNT(*) FROM {0}", cntStmt.TableIdentifier<Tables.eTestFields>()));
            var prevCnt = db.SelectScalar<int>(cntStmt);

            var targetid = 0;
            var sel = db.CreateSelectStatement<Tables.eTestFields>();
            sel.ResultMax = 10;
            var objs = db.SelectObjectList<BO.TestFields>(sel);
            if (objs.Count == 0)
            {
                log.Debug("No records found to update");
                return;
            }

            var obj = objs[0];
            targetid = obj.Id;
            db.Delete<Tables.eTestFields>(obj);

            var sel2 = db.CreateSelectStatement<Tables.eTestFields>();
            sel2.Where.Append("[Id] = @0", targetid);
            var testObj = db.SelectObject<BO.TestFields>(sel2);

            log.Debug("Id: {0}, deleted? : {1}", targetid, (testObj == null));

            var newCnt = db.SelectScalar<int>(cntStmt);
            log.Debug("Old count: {0}, new count: {1}", prevCnt, newCnt);

        }


        public void Transactions()
        {
            var db = CreateDbSqlServer();

            var dtstmt = new DbStatement("SELECT top 10 * FROM [TestFields]");
            var dt = db.SelectDataTable(dtstmt);
            log.Debug("Count: {0}", dt.Rows.Count);

            using (var trans = db.BeginTransaction())
            {
                var dt2 = db.SelectDataTable(dtstmt);
                log.Debug("Count: {0}", dt2.Rows.Count);
            }

        }


        public void SimpleTestsClear()
        {
            log.Debug("Start SQL Server");
            DoSimpleTestsClear(CreateDbSqlServer());
            log.Debug("Finish SQL Server");

            log.Debug("Start SQL CE");
            DoSimpleTestsClear(CreateDbSqlCe());
            log.Debug("Finish SQL CE");

            log.Debug("Start Oracle");
            //DoSimpleTestsClear(CreateDbOracle());
            log.Debug("Finish Oracle");
        }

        private void DoSimpleTestsClear(Rational.DB.Database db)
        {
            using (new LogTimer("Delete"))
            {
                var delStmt = db.CreateStatement<Rational.DB.DbStatement>();
                delStmt.Statement.Append("DELETE FROM SimpleTable");
                var deleted = db.Execute(delStmt);
                log.Debug("{0} records deleted", deleted);
            }
        }

        public void SimpleTestsAllDbs()
        {
            log.Debug("Start SQL Server");
            DoSimpleTests(CreateDbSqlServer());
            log.Debug("Finish SQL Server");

            log.Debug("Start SQL CE");
            DoSimpleTests(CreateDbSqlCe());
            log.Debug("Finish SQL CE");

            log.Debug("Start Oracle");
            //DoSimpleTests(CreateDbOracle());
            log.Debug("Finish Oracle");
        }

        private Random _rnd = new Random();
        private void DoSimpleTests(Rational.DB.Database db)
        {
            //insert
            using (new LogTimer("Inserts"))
            {
                using (var trans = db.BeginTransaction())
                {
                    for (int i = 0; i < 200; i++)
                    {
                        var rndNum = _rnd.Next(100000);
                        var obj = new BO.SimpleTable
                        {
                            string1 = string.Format("Insert test {0}", rndNum),
                            int1 = rndNum,
                            date1 = DateTime.UtcNow.Date.AddMinutes(rndNum)
                        };
                        db.Insert<Tables.SimpleTable>(obj);
                    }
                    trans.Commit();
                }
            }


            //select
            using (new LogTimer("Selects"))
            {
                int totalSelected = 0;
                for (int i = 0; i < 100; i++)
                {
                    var rndNum = _rnd.Next(100);
                    var selStmt = db.CreateSelectStatement<Tables.SimpleTable>();
                    selStmt.Where.AppendWithIdentifiers("{0} = @0", Tables.SimpleTable.int1, rndNum);
                    var objs = db.SelectObjectList<BO.SimpleTable>(selStmt);
                    totalSelected += objs.Count;
                }
                log.Debug("Total {0} objects selected", totalSelected);
            }


            //pagers
            using (new LogTimer("Pagers"))
            {
                var selpgStmt = db.CreateSelectStatement<Tables.SimpleTable>();
                selpgStmt.Where.Append("1=1");
                selpgStmt.OrderBy.Add(Tables.SimpleTable.int1);

                selpgStmt.ResultMax = 20;
                var objs1 = db.SelectObjectList<BO.SimpleTable>(selpgStmt);
                log.Debug("Selected {0} rows", objs1.Count);
                selpgStmt.ResultMax = null;

                var selPager = new Rational.DB.DbSelectPager
                {
                    SelectStatement = selpgStmt,
                    ResultPage = 1,
                    ResultMax = 10,
                    CalculateTotals = true
                };
   
                var objs2 = db.SelectObjectList<BO.SimpleTable>(selPager);
                log.Debug("Selected page {0}/{1}, {2} selected, {3} total", selPager.ResultPage, selPager.TotalPages, objs2.Count, selPager.TotalRows);

                selPager.ResultPage = 2;
                var objs3 = db.SelectObjectList<BO.SimpleTable>(selPager);
                log.Debug("Selected page {0}/{1}, {2} selected, {3} total", selPager.ResultPage, selPager.TotalPages, objs3.Count, selPager.TotalRows);

                selPager.ResultPage = 3;
                var objs4 = db.SelectObjectList<BO.SimpleTable>(selPager);
                log.Debug("Selected page {0}/{1}, {2} selected, {3} total", selPager.ResultPage, selPager.TotalPages, objs4.Count, selPager.TotalRows);

                selPager.ResultPage = null;
                selPager.ResultOffset = 5;
                var objs5 = db.SelectObjectList<BO.SimpleTable>(selPager);
                log.Debug("Selected page {0}/{1}, {2} selected, {3} total", selPager.ResultPage, selPager.TotalPages, objs5.Count, selPager.TotalRows);
                selPager.ResultOffset = null;

                selPager.ResultPage = 20000;
                var objs9 = db.SelectObjectList<BO.SimpleTable>(selPager);
                log.Debug("Selected page {0}/{1}, {2} selected, {3} total", selPager.ResultPage, selPager.TotalPages, objs9.Count, selPager.TotalRows);

                selPager.ResultPage = 2;
                var dt1 = db.SelectDataTable(selPager);
                log.Debug("Selected page {0}/{1}, {2} selected, {3} total", selPager.ResultPage, selPager.TotalPages, dt1.Rows.Count, selPager.TotalRows);

            }

        }


        /*
        //it's private now
        public void TestFormatString()
        {
            var mystr = "";
            mystr = Rational.DB.FormatString.Format("my string: {0}", "test");

            var testList = new List<object>();
            testList.Add("val1");
            testList.Add("val2");
            testList.Add(5);
            mystr = string.Format("test 2: {1}, {0}, {2}", testList.ToArray());
            mystr = Rational.DB.FormatString.Format("test 2: {1}, {0}, {2}", testList);
        }
        */

    }
}

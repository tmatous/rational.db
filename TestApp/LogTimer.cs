﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApp
{
    class LogTimer : IDisposable
    {

        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private System.Diagnostics.Stopwatch _sw;
        private string _name;

        public LogTimer(string pName = null)
        {
            _name = pName;
            log.Debug("Timer '{0}' started at {1}", _name, DateTime.Now);
            _sw = System.Diagnostics.Stopwatch.StartNew();
        }


        public void Dispose()
        {
            _sw.Stop();
            log.Debug("Timer '{0}' finished at {1} ({2})", _name, DateTime.Now, _sw.Elapsed);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FormClosing += frmTestApp_FormClosing;
            Shown += frmTestApp_Shown;
            Load += frmTestApp_Load;
        }


        private ITestMethods _myTestMethods = null;
        private Dictionary<string, TestMethodDelegate> _testList = null;

        private void frmTestApp_Load(System.Object sender, System.EventArgs e)
        {
            var logTarget = new InstanceMethodCallTarget();
            logTarget.ClassInstance = this;
            logTarget.MethodName = "NLogLogMethod";
            logTarget.Parameters.Add(new NLog.Targets.MethodCallParameter("${level}"));
            logTarget.Parameters.Add(new NLog.Targets.MethodCallParameter("${message}${newline}"));
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(logTarget, NLog.LogLevel.Debug);

            _myTestMethods = new TestMethods();
            _testList = new Dictionary<string, TestMethodDelegate>();

            lstTestMethods.Items.Add(" -- ALL TESTS -- ");
            System.Reflection.MethodInfo[] meths = _myTestMethods.GetType().GetMethods((System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.DeclaredOnly));
            foreach (System.Reflection.MethodInfo meth in meths)
            {
                if (meth.ReturnType != typeof(void))
                    continue;
                if (meth.GetParameters().Length > 0)
                    continue;
                if (meth.Name.Equals("InitTestEnvironment"))
                    continue;
                if (meth.Name.Equals("CleanupTestEnvironment"))
                    continue;
                _testList[meth.Name] = (TestMethodDelegate)Delegate.CreateDelegate(typeof(TestMethodDelegate), _myTestMethods, meth);
                lstTestMethods.Items.Add(meth.Name);
            }

            //var defMethName = Interaction.GetSetting("TestApp", _myTestMethods.TestCollectionName ?? "", "DefaultTestMethod", "");
            var defMethName = "";
            if (lstTestMethods.Items.Contains(defMethName))
            {
                lstTestMethods.SelectedItem = defMethName;
            }
            else if (lstTestMethods.Items.Count > 1)
            {
                lstTestMethods.SelectedIndex = 1;
            }

        }

        private void frmTestApp_Shown(object sender, System.EventArgs e)
        {
            WriteLineToLog("Start InitTestEnvironment...");
            _myTestMethods.InitTestEnvironment();
            WriteLineToLog("End InitTestEnvironment.");
        }

        private void frmTestApp_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            WriteLineToLog("Start CleanupTestEnvironment...");
            _myTestMethods.CleanupTestEnvironment();
            WriteLineToLog("End CleanupTestEnvironment.");
        }

        private void lstLogLevel_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
        }

        private void btnRunTest_Click(System.Object sender, System.EventArgs e)
        {
            if (lstTestMethods.SelectedIndex == 0)
            {
                ClearLog();
                foreach (string tst in _testList.Keys)
                {
                    DoRunTest(_testList[tst]);
                    WriteLineToLog("---------------------------------------------------------");
                }
            }
            else if (_testList.ContainsKey(lstTestMethods.Text))
            {
                ClearLog();
                DoRunTest(_testList[lstTestMethods.Text]);
                //Interaction.SaveSetting("TestApp", _myTestMethods.TestCollectionName ?? "", "DefaultTestMethod", lstTestMethods.Text);
            }
        }

        private void btnCopyLog_Click(System.Object sender, System.EventArgs e)
        {
            System.Windows.Forms.Clipboard.SetText(txtResults.Text);
        }

        private void btnClearLog_Click(System.Object sender, System.EventArgs e)
        {
            ClearLog();
        }



        private void DoRunTest(TestMethodDelegate pFunc)
        {
            if (pFunc == null)
                return;
            try
            {
                btnRunTest.Enabled = false;
                WriteLineToLog(string.Format("Start '{0}' at {1:HH:mm:ss.ff} ...", pFunc.Method.Name, DateTime.Now));
                WriteLineToLog("");
                pFunc.Invoke();
            }
            catch (Exception ex)
            {
                WriteToLog("Error: " + ex.Message);
            }
            finally
            {
                WriteLineToLog("");
                WriteLineToLog(string.Format("End '{0}' at {1:HH:mm:ss.ff}.", pFunc.Method.Name, DateTime.Now));
                btnRunTest.Enabled = true;
            }
        }

        //target for NLog
        public void NLogLogMethod(string pLevel, string pMessage)
        {
            WriteToLog(pMessage);
        }

        private void WriteToLog(string pMessage)
        {
            if (this.txtResults.InvokeRequired)
            {
                Action<string> meth = new Action<string>(WriteToLog);
                this.txtResults.Invoke(meth, pMessage);
            }
            else
            {
                bool atBottom = (this.txtResults.SelectionStart == this.txtResults.Text.Length);
                this.txtResults.Text += pMessage;
                if (atBottom)
                {
                    this.txtResults.SelectionStart = this.txtResults.Text.Length;
                    this.txtResults.ScrollToCaret();
                }
                this.Refresh();
            }
        }

        private void WriteLineToLog(string pMessage)
        {
            WriteToLog(pMessage + Environment.NewLine);
        }
        private void ClearLog()
        {
            if (this.InvokeRequired)
            {
                Action meth = new Action(ClearLog);
                this.Invoke(meth);
            }
            else
            {
                txtResults.Text = string.Empty;
                this.Refresh();
            }
        }

    }

    public interface ITestMethods
    {
        void InitTestEnvironment();
        void CleanupTestEnvironment();
        string TestCollectionName { get; set; }
    }

    public delegate void TestMethodDelegate();


    public class InstanceMethodCallTarget : NLog.Targets.MethodCallTargetBase
    {
        private System.Reflection.MethodInfo m_MethodInfo;

        public InstanceMethodCallTarget()
            : base()
        {
        }

        #region Property - ClassInstance
        private object m_ClassInstance;
        public object ClassInstance
        {
            get
            {
                return m_ClassInstance;
            }
            set
            {
                m_ClassInstance = value;
                UpdateMethodInfo();
            }
        }
        #endregion


        #region Property - MethodName
        private string m_MethodName;
        public string MethodName
        {
            get
            {
                return m_MethodName;
            }
            set
            {
                m_MethodName = value;
                UpdateMethodInfo();
            }
        }
        #endregion


        private void UpdateMethodInfo()
        {
            if ((this.ClassInstance != null) && (this.MethodName != null))
            {
                m_MethodInfo = this.ClassInstance.GetType().GetMethod(this.MethodName);
            }
            else
            {
                m_MethodInfo = null;
            }
        }

        protected override void DoInvoke(object[] parameters)
        {
            if (m_MethodInfo != null)
            {
                m_MethodInfo.Invoke(this.ClassInstance, parameters);
            }
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Rational.DB;

namespace TestApp
{
    namespace Tables
    {

        [Table(SqlName = "TestFields")]
        public enum eTestFields
        {
            [Column(PrimaryKey = true, AutoIncrement = true)]
            Id,
            str,
            ustr,
            dec,
            dt,
            boo,
            [Column(SqlName = "int")]
            clrint,
            [Column(NotNull = true)]
            strnn,
            [Column(NotNull = true)]
            ustrnn,
            [Column(NotNull = true)]
            decnn,
            [Column(NotNull = true)]
            dtnn,
            [Column(NotNull = true)]
            boonn,
            [Column(NotNull = true)]
            intnn

        }

        [Table(SqlName = "TestFields", UppercaseIdentifiers = true)]
        public enum eTestFieldsOracle
        {
            [Column(PrimaryKey = true)]
            Id,
            str,
            ustr,
            dec,
            dt,
            boo,
            [Column(SqlName = "int")]
            clrint,
            [Column(NotNull = true)]
            strnn,
            [Column(NotNull = true)]
            ustrnn,
            [Column(NotNull = true)]
            decnn,
            [Column(NotNull = true)]
            dtnn,
            [Column(NotNull = true)]
            boonn,
            [Column(NotNull = true)]
            intnn

        }

        [Table(SqlName = "TestFields")]
        public enum eTestFieldsSqlCe
        {
            [Column(PrimaryKey = true, AutoIncrement = true)]
            Id,
            [Column(SqlType="String")]
            ustr,
            dec,
            dt,
            boo,
            [Column(SqlName = "int")]
            clrint,
            [Column(NotNull = true, SqlType="String")]
            ustrnn,
            [Column(NotNull = true)]
            decnn,
            [Column(NotNull = true)]
            dtnn,
            [Column(NotNull = true)]
            boonn,
            [Column(NotNull = true)]
            intnn

        }

        [Table(SqlName = "SimpleTable")]
        public enum SimpleTable
        {
            [Column(SqlType = "String")]
            string1,
            int1,
            date1,
        }

    }
}

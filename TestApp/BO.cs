﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApp
{
    namespace BO
    {

        public class TestFields
        {
            public int Id { get; set; }
            public string str { get; set; }
            public string ustr { get; set; }
            public decimal? dec { get; set; }
            public DateTime? dt { get; set; }
            public Boolean? boo { get; set; }
            public Int32? clrint { get; set; }
            public string strnn { get; set; }
            public string ustrnn { get; set; }
            public decimal decnn { get; set; }
            public DateTime dtnn { get; set; }
            public Boolean boonn { get; set; }
            public Int32 intnn { get; set; }
        }

        public class TestFieldsOracle
        {
            public int Id { get; set; }
            public string str { get; set; }
            public string ustr { get; set; }
            public decimal? dec { get; set; }
            public DateTime? dt { get; set; }
            public char? boo { get; set; }
            public Int32? clrint { get; set; }
            public string strnn { get; set; }
            public string ustrnn { get; set; }
            public decimal decnn { get; set; }
            public DateTime dtnn { get; set; }
            public char boonn { get; set; }
            public Int32 intnn { get; set; }
        }

        public class TestFieldsSqlCe
        {
            public int Id { get; set; }
            public string ustr { get; set; }
            public decimal? dec { get; set; }
            public DateTime? dt { get; set; }
            public Boolean? boo { get; set; }
            public Int32? clrint { get; set; }
            public string ustrnn { get; set; }
            public decimal decnn { get; set; }
            public DateTime dtnn { get; set; }
            public Boolean boonn { get; set; }
            public Int32 intnn { get; set; }
        }

        public class SimpleTable
        {
            public string string1 { get; set; }
            public Int32? int1 { get; set; }
            public DateTime? date1 { get; set; }
        }

    }
}

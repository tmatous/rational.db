﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Rational.DB
{

    /// <summary>
    /// Wraps a .NET DbTransaction object to limit access to database functions
    /// </summary>
    /// <remarks></remarks>
    public class DbTransaction : IDisposable
    {

        /// <summary></summary>
        protected internal DbConnectionInfo _connInfo;

        /// <summary></summary>
        protected internal Guid _guid = Guid.NewGuid();

        /// <summary></summary>
        protected System.Data.IDbTransaction _transaction;

        /// <summary></summary>
        protected System.Data.IDbConnection _connection;

        /// <summary></summary>
        protected bool _completed = false;

        /// <summary></summary>
        protected internal event OnCloseEventHandler OnClose;

        /// <summary></summary>
        protected internal delegate void OnCloseEventHandler(System.Object sender, System.EventArgs e);

        /// <summary></summary>
        protected internal DbTransaction(DbConnectionInfo pConnectionInfo)
        {
            _connInfo = pConnectionInfo;
        }


        /// <summary></summary>
        protected internal System.Data.IDbCommand CreateCommand()
        {
            if (_connection == null)
            {
                _connection = _connInfo.GetFactory().CreateConnection();
                _connection.ConnectionString = _connInfo.ConnectionString;
                _connection.Open();
                _transaction = _connection.BeginTransaction();
                _completed = false;
            }
            System.Data.IDbCommand cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            return cmd;
        }

        /// <summary>Perform rollback of all activity done within the transaction</summary>
        public void Rollback()
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
                _completed = true;
            }
        }

        /// <summary>Commit activity done within the transaction to the database</summary>
        public void Commit()
        {
            if (_transaction != null)
            {
                _transaction.Commit();
                _completed = true;
            }
        }



        #region "Dispose"

        bool disposed = false;

        /// <summary></summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary></summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (_transaction != null)
                {
                    if (!_completed) _transaction.Rollback();
                    _transaction.Dispose();
                    _transaction = null;
                }
                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }

                if (OnClose != null) OnClose(this, new System.EventArgs());
            }

            disposed = true;
        }

        #endregion

    }

}

﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Rational.DB
{
    internal class FormatString
    {


        //modified from FormatWith (http://james.newtonking.com/archive/2008/03/29/formatwith-2-0-string-formatting-with-named-variables.aspx)

        /// <summary>
        /// Replaces keys in a string with the values of matching object properties. Like String.Format but using named placeholders.
        /// <remarks>Uses String.Format() internally; custom formats should match those used for that method.</remarks>
        /// </summary>
        /// <param name="pFormat">The format string, containing keys like {foo} and {foo:SomeFormat}.</param>
        /// <param name="pData">Dictionary of named values that should be injected in the string</param>
        /// <returns>A version of the format string with keys replaced by (formatted) key values.</returns>
        public static string Format(string pFormat, IDictionary<string, object> pData)
        {
            return FormatImpl(pFormat, pData);
        }



        /// <summary>
        /// Replaces keys in a string with the values of matching object properties. Like String.Format but using named placeholders.
        /// <remarks>Uses String.Format() internally; custom formats should match those used for that method.</remarks>
        /// </summary>
        /// <param name="pFormat">The format string, containing keys like {foo} and {foo:SomeFormat}.</param>
        /// <param name="pData">The object whose properties should be injected in the string</param>
        /// <returns>A version of the format string with keys replaced by (formatted) key values.</returns>
        public static string Format(string pFormat, object pData)
        {
            if (pData == null) throw new ArgumentNullException("pData");

            //these don't need rewritten.. should be faster to dump straight to string.Format
            if (IsScalarType(pData.GetType())) return string.Format(pFormat, pData);
            if (pData is IEnumerable) return string.Format(pFormat, ((IEnumerable)pData).Cast<object>().ToArray());

            return FormatImpl(pFormat, ConvertDataToDictionary(pData));
        }



        /// <summary>
        /// Parse a format string and return a list of required placeholder data
        /// </summary>
        /// <param name="pFormat">Format string to examine</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static IList<string> GetPlaceholders(string pFormat)
        {
            List<string> placeholders = null;
            string newFormat = null;
            RewriteFormatString(pFormat, ref  newFormat, ref  placeholders);
            return placeholders;
        }



        protected static string FormatImpl(string pFormat, IDictionary<string, object> pData)
        {
            if (pFormat == null) throw new ArgumentNullException("pFormat");
            if (pData == null) throw new ArgumentNullException("pData");

            List<string> placeholders = null;
            string newFormat = null;
            RewriteFormatString(pFormat, ref  newFormat, ref  placeholders);
            List<object> values = new List<object>();
            foreach (var placeholder in placeholders)
            {
                if ((!pData.ContainsKey(placeholder)))
                    throw new Exception(string.Format("FormatString: Data for placeholder '{0}' not found", placeholder));
                values.Add(pData[placeholder]);
            }

            return string.Format(newFormat, values.ToArray());
        }

        protected static void RewriteFormatString(string pFormat, ref string poNewFormat, ref List<string> poPlaceholders)
        {
            List<string> placeholders = new List<string>();
            string newFormat = System.Text.RegularExpressions.Regex.Replace(pFormat, "(?<start>\\{)+(?<property>[\\w\\.\\[\\]]+)(?<format>:[^}]+)?(?<end>\\})+", (System.Text.RegularExpressions.Match m) =>
            {
                System.Text.RegularExpressions.Group startGroup = m.Groups["start"];
                System.Text.RegularExpressions.Group propertyGroup = m.Groups["property"];
                System.Text.RegularExpressions.Group formatGroup = m.Groups["format"];
                System.Text.RegularExpressions.Group endGroup = m.Groups["end"];
                
                int openings = startGroup.Captures.Count;
                int closings = endGroup.Captures.Count;
                if (((closings % 2 == 1) && (openings % 2 == 1)))
                {
                    var placeholderIndex = -1;
                    if ((!placeholders.Contains(propertyGroup.Value)))
                    {
                        placeholderIndex = placeholders.Count;
                        placeholders.Add(propertyGroup.Value);
                    }
                    else
                    {
                        placeholderIndex = placeholders.IndexOf(propertyGroup.Value);
                    }
                    return new string('{', openings) + (placeholderIndex) + formatGroup.Value + new string('}', closings);
                }
                else
                {
                    //braces not matched, give up
                    return m.Value;
                }
            }, System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            poNewFormat = newFormat;
            poPlaceholders = placeholders;
        }

        protected static bool IsScalarType(Type type)
        {
            return (type == typeof(object) || Type.GetTypeCode(type) != TypeCode.Object);
        }

        protected static IDictionary<string, object> ConvertDataToDictionary(object pData)
        {
            if (pData == null) return null;
            var values = new Dictionary<string, object>();
            if (IsScalarType(pData.GetType()))
            {
                values.Add("0", pData);
            }
            else if (pData is IDictionary)
            {
                //convert the dictionary
                var dict = (IDictionary)pData;
                foreach (var ky in dict.Keys)
                {
                    values.Add(ky.ToString(), dict[ky]);
                }
            }
            else if (pData is IEnumerable)
            {
                //convert the ienumerable, index number as key
                var coll = (IEnumerable)pData;
                var idx = 0;
                foreach (object obj in coll)
                {
                    values.Add(idx.ToString(), obj);
                    idx += 1;
                }
            }
            else
            {
                //just treat it as an object, get the properties
                System.ComponentModel.PropertyDescriptorCollection props = System.ComponentModel.TypeDescriptor.GetProperties(pData);
                foreach (System.ComponentModel.PropertyDescriptor prop in props)
                {
                    values.Add(prop.Name, prop.GetValue(pData));
                }
            }

            return values;
        }


    }
}

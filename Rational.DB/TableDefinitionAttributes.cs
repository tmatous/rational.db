﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;

namespace Rational.DB
{

    /// <summary>
    /// Helper class for reading Table and Column attributes
    /// </summary>
    public class TableDefinitionAttributes
    {

        /// <summary>
        /// Get the defined Table attribute for an enum (or a default)
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static TableAttribute GetTableAttribute<TEnum>() where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum) throw new Exception("TEnum must be an Enum");
            return GetTableAttribute(typeof(TEnum));
        }

        /// <summary>
        /// Get the SqlName for the table enum (or enum name, if missing)
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static string GetTableSqlName<TEnum>() where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum) throw new Exception("TEnum must be an Enum");
            return GetTableAttribute(typeof(TEnum)).SqlName;
        }

        /// <summary>
        /// Get the defined Column attributes for an enum (or a default)
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static Dictionary<string, ColumnAttribute> GetColumnAttributes<TEnum>() where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum) throw new Exception("TEnum must be an Enum");
            return GetColumnAttributes(typeof(TEnum));
        }


        /// <summary>
        /// Get the defined Column attribute for an enum value (or a default)
        /// </summary>
        /// <param name="pEnumVal"></param>
        /// <returns></returns>
        public static ColumnAttribute GetColumnAttribute(System.Enum pEnumVal)
        {
            return GetColumnAttributes(pEnumVal.GetType())[pEnumVal.ToString()];
        }

        /// <summary>
        /// Get the SqlName for the column enum value (or enum value name, if missing)
        /// </summary>
        /// <param name="pEnumVal"></param>
        /// <returns></returns>
        public static string GetColumnSqlName(System.Enum pEnumVal)
        {
            return GetColumnAttribute(pEnumVal).SqlName;
        }

        /// <summary>
        /// Get the ClrName for the column enum value (or enum value name, if missing)
        /// </summary>
        /// <param name="pEnumVal"></param>
        /// <returns></returns>
        public static string GetColumnClrName(System.Enum pEnumVal)
        {
            return GetColumnAttribute(pEnumVal).ClrName;
        }




        #region "Load Attributes"

        /// <summary></summary>
        protected static Dictionary<Type, TableAttribute> _allTableAttributes = new Dictionary<Type, TableAttribute>();

        /// <summary></summary>
        protected static Dictionary<Type, Dictionary<string, ColumnAttribute>> _allColumnAttributes = new Dictionary<Type, Dictionary<string, ColumnAttribute>>();

        /// <summary></summary>
        protected static TableAttribute GetTableAttribute(Type pEnumType)
        {
            if ((!_allTableAttributes.ContainsKey(pEnumType)))
                LoadAttributeData(pEnumType);
            return _allTableAttributes[pEnumType];
        }

        /// <summary></summary>
        protected static Dictionary<string, ColumnAttribute> GetColumnAttributes(Type pEnumType)
        {
            if ((!_allColumnAttributes.ContainsKey(pEnumType)))
                LoadAttributeData(pEnumType);
            return _allColumnAttributes[pEnumType];
        }

        /// <summary></summary>
        protected static object _lockTarget = new object();

        /// <summary></summary>
        protected static void LoadAttributeData(Type pEnumType)
        {
            if ((!_allTableAttributes.ContainsKey(pEnumType)))
            {
                lock (_lockTarget)
                {
                    TableAttribute tbl = new TableAttribute();
                    var tblAttrs = pEnumType.GetCustomAttributes(typeof(Rational.DB.TableAttribute), false);
                    if ((tblAttrs != null) && (tblAttrs.Length > 0))
                    {
                        var attr = (Rational.DB.TableAttribute)tblAttrs[0];
                        tbl.SqlName = attr.SqlName;
                        tbl.UppercaseIdentifiers = attr.UppercaseIdentifiers;
                    }

                    //if SqlName was not available as attributes, set to enum Name
                    if (tbl.SqlName == null) tbl.SqlName = pEnumType.Name;
                    if (tbl.UppercaseIdentifiers) tbl.SqlName = tbl.SqlName.ToUpperInvariant();


                    Dictionary<string, ColumnAttribute> colData = new Dictionary<string, ColumnAttribute>(StringComparer.OrdinalIgnoreCase);

                    foreach (System.Reflection.FieldInfo fld in pEnumType.GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.Public))
                    {
                        ColumnAttribute col = new ColumnAttribute();
                        var attrs = fld.GetCustomAttributes(typeof(Rational.DB.ColumnAttribute), false);
                        if ((attrs != null) && (attrs.Length > 0))
                        {
                            var attr = (Rational.DB.ColumnAttribute)attrs[0];
                            col.SqlName = attr.SqlName;
                            col.ClrName = attr.ClrName;
                            col.SqlType = attr.SqlType;
                            col.NotNull = attr.NotNull;
                            col.PrimaryKey = attr.PrimaryKey;
                            col.AutoIncrement = attr.AutoIncrement;
                            col.Computed = attr.Computed;
                        }

                        //if SqlName or ClrName were not available as attributes, set to enum item Name
                        if (col.ClrName == null) col.ClrName = fld.Name;
                        if (col.SqlName == null) col.SqlName = fld.Name;
                        if (col.AutoIncrement) col.Computed = true;
                        if (tbl.UppercaseIdentifiers) col.SqlName = col.SqlName.ToUpperInvariant();

                        colData.Add(fld.Name, col);
                    }
                    _allTableAttributes[pEnumType] = tbl;
                    _allColumnAttributes[pEnumType] = colData;
                }
            }
        }

        #endregion

    }

}
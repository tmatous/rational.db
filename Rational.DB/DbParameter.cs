﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Rational.DB
{

    /// <summary>
    /// Database-agnostic class to represent a database parameter
    /// </summary>
    public class DbParameter : IDbDataParameter
    {

        /// <summary></summary>
        public DbParameter()
        {
            Direction = ParameterDirection.Input;
        }

        /// <summary></summary>
        public DbParameter(string pName, object pValue)
        {
            ParameterName = pName;
            Value = pValue;
            Direction = ParameterDirection.Input;
        }

        /// <summary></summary>
        public DbParameter(string pName, object pValue, DbType pDbType)
        {
            ParameterName = pName;
            Value = pValue;
            DbType = pDbType;
            Direction = ParameterDirection.Input;
        }

        /// <summary></summary>
        public DbParameter(string pName, object pValue, string pProviderDbType)
        {
            ParameterName = pName;
            Value = pValue;
            ProviderDbType = pProviderDbType;
            Direction = ParameterDirection.Input;
        }

        /// <summary>Name of the parameter</summary>
        public string ParameterName { get; set; }

        /// <summary>Value of the parameter</summary>
        public object Value { get; set; }

        private System.Data.DbType _DbType;
        /// <summary>Database data type of the parameter</summary>
        public System.Data.DbType DbType
        {
            get
            {
                return _DbType;
            }

            set
            {
                _DbType = value;
                DbTypeSpecified = true;
            }
        }

        /// <summary>Indicates whether the DbType value was explictly set</summary>
        public bool DbTypeSpecified { get; set; }

        /// <summary>Specifies whether data is coming from or going to the database through this parameter</summary>
        public System.Data.ParameterDirection Direction { get; set; }

        /// <summary>The size of the parameter</summary>
        public int Size { get; set; }

        /// <summary>Indicates the precision of numeric parameters</summary>
        public byte Precision { get; set; }

        /// <summary>Indicates the scale of numeric parameters</summary>
        public byte Scale { get; set; }

        /// <summary>Provider-specific database data type of the parameter</summary>
        public string ProviderDbType { get; set; }

        /// <summary>If it is possible to determine ClrType of the parameter, store here for possible provider mapping later</summary>
        protected internal System.Type ClrType { get; set; }

        /// <summary>Store the instantiated provider parameter to copy output parameter values</summary>
        protected internal IDbDataParameter ProviderParameter { get; set; }

        /// <summary>Provider parameter. Can be specified in DbParameter creation, to support unsupported features of the DBMS</summary>
        protected internal IDbDataParameter SourceProviderParameter { get; set; }



        bool IDataParameter.IsNullable
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        string IDataParameter.SourceColumn
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        DataRowVersion IDataParameter.SourceVersion
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }



        /// <summary>Get typecast output value of an output parameter</summary>
        public T GetOutputValue<T>()
        {
            if ((this.Direction == ParameterDirection.Input) || (this.ProviderParameter == null)) throw new Exception("Output not available");
            object v = this.ProviderParameter.Value;
            if (v == null) return default(T);
            Type dstType = typeof(T);
            if (Nullable.GetUnderlyingType(dstType) != null) dstType = Nullable.GetUnderlyingType(dstType);
            return (T)Convert.ChangeType(v, dstType);
        }

    }

    /// <summary></summary>
    public class DbParameterCollection : ICollection
    {

        /// <summary></summary>
        protected List<DbParameter> _parameters = new List<DbParameter>();

        /// <summary></summary>
        public Int32 Count
        {
            get { return ((ICollection)_parameters).Count; }
        }

        /// <summary></summary>
        public object SyncRoot
        {
            get
            {
                return ((ICollection)_parameters).SyncRoot;
            }
        }

        /// <summary></summary>
        public bool IsSynchronized
        {
            get
            {
                return ((ICollection)_parameters).IsSynchronized;
            }
        }

        /// <summary></summary>
        public void CopyTo(Array array, int index)
        {
            ((ICollection)_parameters).CopyTo(array, index);
        }

        /// <summary></summary>
        public System.Collections.IEnumerator GetEnumerator()
        {
            return ((ICollection)_parameters).GetEnumerator();
        }

        /// <summary></summary>
        public void Clear()
        {
            _parameters.Clear();
        }

        /// <summary></summary>
        public void Add(string pName, object pValue)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                Value = pValue,
                ProviderDbType = null
            };
            Add(prm);
        }

        /// <summary></summary>
        public void Add(string pName, object pValue, System.Data.DbType pType)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                Value = pValue,
                DbType = pType,
                ProviderDbType = null
            };
            Add(prm);
        }

        /// <summary></summary>
        public void Add(string pName, object pValue, string pProviderDbType)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                Value = pValue,
                ProviderDbType = pProviderDbType
            };
            Add(prm);
        }

        /// <summary>Internal method for when CLR type is known</summary>
        protected internal void Add(string pName, object pValue, System.Type pClrType)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                Value = pValue,
                ClrType = pClrType,
                ProviderDbType = null
            };
            Add(prm);
        }

        /// <summary></summary>
        public void Add(DbParameterCollection pList)
        {
            foreach (DbParameter prm in pList._parameters)
            {
                Add(prm);
            }
        }

        /// <summary></summary>
        public void Add(DbParameter pParam)
        {
            _parameters.Add(pParam);
        }

        /// <summary></summary>
        public void Add(IDbDataParameter pProviderParam)
        {
            if (pProviderParam is DbParameter)
            {
                //wrong one called
                Add((DbParameter)pProviderParam);
                return;
            }
            DbParameter prm = new DbParameter
            {
                SourceProviderParameter = pProviderParam,
                ParameterName = pProviderParam.ParameterName,
                Value = pProviderParam.Value ?? DBNull.Value,
                DbType = pProviderParam.DbType,
                Direction = pProviderParam.Direction,
                Size = pProviderParam.Size,
                Precision = pProviderParam.Precision,
                Scale = pProviderParam.Scale
            };
            Add(prm);
        }


        #region "Output parameters"

        /// <summary>Add output parameter</summary>
        public DbParameter AddOutput(string pName, System.Data.DbType pType, int pSize)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = pType,
                Direction = ParameterDirection.Output,
                Size = pSize
            };
            Add(prm);

            return prm;
        }

        /// <summary>Add output parameter</summary>
        public DbParameter AddOutput(string pName, System.Data.DbType pType, byte pPrecision, byte pScale)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = pType,
                Direction = ParameterDirection.Output,
                Precision = pPrecision,
                Scale = pScale
            };
            Add(prm);

            return prm;
        }

        /// <summary>Add output parameter</summary>
        public DbParameter AddOutput(string pName, System.Data.DbType pType, int pSize, byte pPrecision, byte pScale)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = pType,
                Direction = ParameterDirection.Output,
                Size = pSize,
                Precision = pPrecision,
                Scale = pScale
            };
            Add(prm);

            return prm;
        }


        /// <summary>Add input/output parameter</summary>
        public DbParameter AddInputOutput(string pName, object pValue, System.Data.DbType pType, int pSize)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = pType,
                Value = pValue,
                Direction = ParameterDirection.InputOutput,
                Size = pSize
            };
            Add(prm);

            return prm;
        }

        /// <summary>Add input/output parameter</summary>
        public DbParameter AddInputOutput(string pName, object pValue, System.Data.DbType pType, byte pPrecision, byte pScale)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = pType,
                Value = pValue,
                Direction = ParameterDirection.InputOutput,
                Precision = pPrecision,
                Scale = pScale
            };
            Add(prm);

            return prm;
        }

        /// <summary>Add input/output parameter</summary>
        public DbParameter AddInputOutput(string pName, object pValue, System.Data.DbType pType, int pSize, byte pPrecision, byte pScale)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = pType,
                Value = pValue,
                Direction = ParameterDirection.InputOutput,
                Size = pSize,
                Precision = pPrecision,
                Scale = pScale
            };
            Add(prm);

            return prm;
        }

        /// <summary>Add return value parameter</summary>
        public DbParameter AddReturnValue(string pName)
        {
            DbParameter prm = new DbParameter
            {
                ParameterName = pName,
                DbType = DbType.Int32,
                Direction = ParameterDirection.ReturnValue
            };
            Add(prm);

            return prm;
        }

        #endregion


        #region "Add by type"

        /// <summary></summary>
        public void AddAnsiString(string pName, string pValue, string pConvertNullTo = "")
        {
            if (pConvertNullTo == null)
                throw new Exception("AddString : Null for pConvertNullTo not allowed");
            Add(pName, pValue != null ? pValue : pConvertNullTo, DbType.AnsiString);
        }

        /// <summary></summary>
        public void AddAnsiStringNullable(string pName, string pValue, bool pEmptyMeansNull = true)
        {
            if (pEmptyMeansNull && string.Equals(pValue, string.Empty))
                pValue = null;
            Add(pName, pValue, DbType.AnsiString);
        }

        /// <summary></summary>
        public void AddUString(string pName, string pValue, string pConvertNullTo = "")
        {
            if (pConvertNullTo == null)
                throw new Exception("AddUString : Null for pConvertNullTo not allowed");
            Add(pName, pValue != null ? pValue : pConvertNullTo, DbType.String);
        }

        /// <summary></summary>
        public void AddUStringNullable(string pName, string pValue, bool pEmptyMeansNull = true)
        {
            if (pEmptyMeansNull && string.Equals(pValue, string.Empty))
                pValue = null;
            Add(pName, pValue, DbType.String);
        }


        /// <summary></summary>
        public void AddBoolean(string pName, bool pValue)
        {
            Add(pName, pValue, DbType.Boolean);
        }

        /// <summary></summary>
        public void AddBooleanNullable(string pName, bool? pValue)
        {
            if (pValue.HasValue)
            {
                Add(pName, pValue.Value);
            }
            else
            {
                Add(pName, null);
            }
        }


        /// <summary></summary>
        public void AddInt32(string pName, Int32 pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddInt32(string pName, Int32? pValue, Int32 pConvertNullTo)
        {
            Add(pName, pValue.HasValue ? pValue.Value : pConvertNullTo);
        }

        /// <summary></summary>
        public void AddInt32Nullable(string pName, Int32 pValue, Int32 pValueMeansNull)
        {
            if (pValue == pValueMeansNull)
            {
                Add(pName, null);
            }
            else
            {
                Add(pName, pValue);
            }
        }

        /// <summary></summary>
        public void AddInt32Nullable(string pName, Int32? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddInt64(string pName, Int64 pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddInt64(string pName, Int64? pValue, Int64 pConvertNullTo)
        {
            Add(pName, pValue.HasValue ? pValue.Value : pConvertNullTo);
        }

        /// <summary></summary>
        public void AddInt64Nullable(string pName, Int64 pValue, Int64 pValueMeansNull)
        {
            if (pValue == pValueMeansNull)
            {
                Add(pName, null);
            }
            else
            {
                Add(pName, pValue);
            }
        }

        /// <summary></summary>
        public void AddInt64Nullable(string pName, Int64? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddSingle(string pName, float pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddSingle(string pName, float? pValue, float pConvertNullTo = 0)
        {
            Add(pName, pValue.HasValue ? pValue.Value : pConvertNullTo);
        }

        /// <summary></summary>
        public void AddSingleNullable(string pName, float pValue, float pValueMeansNull = 0)
        {
            if (pValue == pValueMeansNull)
            {
                Add(pName, null);
            }
            else
            {
                Add(pName, pValue);
            }
        }

        /// <summary></summary>
        public void AddSingleNullable(string pName, float? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddDouble(string pName, double pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddDouble(string pName, double? pValue, double pConvertNullTo = 0)
        {
            Add(pName, pValue.HasValue ? pValue.Value : pConvertNullTo);
        }

        /// <summary></summary>
        public void AddDoubleNullable(string pName, double pValue, double pValueMeansNull = 0)
        {
            if (pValue == pValueMeansNull)
            {
                Add(pName, null);
            }
            else
            {
                Add(pName, pValue);
            }
        }

        /// <summary></summary>
        public void AddDoubleNullable(string pName, double? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddDecimal(string pName, decimal pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddDecimal(string pName, decimal? pValue, decimal pConvertNullTo = 0)
        {
            Add(pName, pValue.HasValue ? pValue.Value : pConvertNullTo);
        }

        /// <summary></summary>
        public void AddDecimalNullable(string pName, decimal pValue, decimal pValueMeansNull = 0)
        {
            if (pValue == pValueMeansNull)
            {
                Add(pName, null);
            }
            else
            {
                Add(pName, pValue);
            }
        }

        /// <summary></summary>
        public void AddDecimalNullable(string pName, decimal? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddDateTime(string pName, DateTime pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddDateTimeNullable(string pName, DateTime? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddTimeSpan(string pName, TimeSpan pValue)
        {
            Add(pName, pValue);
        }

        /// <summary></summary>
        public void AddTimeSpanNullable(string pName, TimeSpan? pValue)
        {
            Add(pName, pValue);
        }


        /// <summary></summary>
        public void AddGuid(string pName, Guid pValue)
        {
            Add(pName, pValue, DbType.Guid);
        }

        /// <summary></summary>
        public void AddGuidNullable(string pName, Guid? pValue)
        {
            Add(pName, pValue, DbType.Guid);
        }


        /// <summary></summary>
        public void AddByteArray(string pName, byte[] pValue)
        {
            if (pValue != null)
            {
                Add(pName, pValue, DbType.Binary);
            }
            else
            {
                Add(pName, new byte(), DbType.Binary);
            }
        }

        /// <summary></summary>
        public void AddByteArrayNullable(string pName, byte[] pValue, bool pEmptyMeansNull = true)
        {
            if (pEmptyMeansNull && (pValue != null) && (pValue.Length == 0))
            {
                Add(pName, null, DbType.Binary);
            }
            else
            {
                Add(pName, pValue, DbType.Binary);
            }
        }

        /// <summary></summary>
        public void AddRowVersion(string pName, UInt64 pValue)
        {
            Add(pName, BitConverter.GetBytes(pValue), DbType.Binary);
        }

        #endregion

    }

}

﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 



using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Rational.DB
{

    /// <summary>
    /// Wraps a DataReader object to present a single row of data to a consumer
    /// </summary>
    /// <remarks></remarks>
    public class DbRow : IDataRecord, IDisposable
    {


        /// <summary></summary>
        protected IDataReader _myReader;

        /// <summary></summary>
        protected Database _myDb;

        /// <summary>Keeps track of the field provider types, to support custom mapping</summary>
        protected Dictionary<string, DbType> _fieldDbTypes;

        /// <summary>Keeps track of the field CLR types, to support custom mapping</summary>
        protected Dictionary<string, Type> _fieldTypes;

        /// <summary></summary>
        protected internal DbRow(Database pDb, IDataReader pReader)
        {
            this._myDb = pDb;
            this._myReader = pReader;
            LoadCustomMappings();
        }

        /// <summary></summary>
        protected void LoadCustomMappings()
        {
            _fieldDbTypes = null;
            _fieldTypes = null;
            if (_myDb.HasCustomDbToClrMappings())
            {
                _fieldDbTypes = new Dictionary<string, DbType>(StringComparer.OrdinalIgnoreCase);
                _fieldTypes = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
                var schema = _myReader.GetSchemaTable();
                if ((schema != null) && !schema.Columns.Contains("ProviderType")) schema = null;
                for (int fldIdx = 0; fldIdx < _myReader.FieldCount; fldIdx++)
                {
                    var nm = _myReader.GetName(fldIdx);
                    if (!_fieldTypes.ContainsKey(nm))
                    {
                        _fieldTypes.Add(nm, _myReader.GetFieldType(fldIdx));
                        if (schema != null)
                            _fieldDbTypes.Add(nm, _myDb.ConvertProviderDbType((int)schema.Rows[fldIdx]["ProviderType"]));
                        else
                            _fieldDbTypes.Add(nm, DbType.Object);
                    }
                }
            }
        }

        /// <summary></summary>
        protected internal string GetName(Int32 pColumnNumber)
        {
            return _myReader.GetName(pColumnNumber);
        }

        /// <summary></summary>
        protected internal Type GetFieldType(Int32 pColumnNumber)
        {
            return _myReader.GetFieldType(pColumnNumber);
        }

        /// <summary>Number of fields in this result</summary>
        public Int32 FieldCount
        {
            get { return _myReader.FieldCount; }
        }


        #region "Value"

        /// <summary>gets specified column as Object, null if dbnull</summary>
        protected internal object _GetValue(Int32 pColumnNumber)
        {
            var res = _myReader.GetValue(pColumnNumber);
            if (_myDb.HasCustomDbToClrMappings())
            {
                var nm = _myReader.GetName(pColumnNumber);
                res = _myDb.MapDbToClr(res, _fieldDbTypes[nm], _fieldTypes[nm]);
            }
            if (object.ReferenceEquals(DBNull.Value, res)) res = null;
            return res;
        }
        /// <summary>gets specified column as T, pDefault if dbnull</summary>
        protected internal T _GetValue<T>(Int32 pColumnNumber, T pDefault)
        {
            var res = _GetValue(pColumnNumber);
            if (res == null) return pDefault;
            Type dstType = typeof(T);
            if (Nullable.GetUnderlyingType(dstType) != null) dstType = Nullable.GetUnderlyingType(dstType);
            if (res.GetType().Equals(dstType))
                return (T)res;
            else
                return (T)Convert.ChangeType(res, dstType);
        }
        /// <summary>gets specified column as T, default if dbnull</summary>
        protected internal T _GetValue<T>(Int32 pColumnNumber)
        {
            return _GetValue<T>(pColumnNumber, default(T));
        }


        /// <summary>gets specified column as T, pDefault if dbnull</summary>
        public T GetValue<T>(string pColumnName, T pDefault)
        {
            return _GetValue<T>(_myReader.GetOrdinal(pColumnName), pDefault);
        }
        /// <summary>gets specified column as T, pDefault if dbnull</summary>
        public T GetValue<T>(Enum pColumnNameEnum, T pDefault)
        {
            return _GetValue<T>(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pDefault);
        }

        /// <summary>gets specified column as T, default if dbnull</summary>
        public T GetValue<T>(string pColumnName)
        {
            return _GetValue<T>(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as T, default if dbnull</summary>
        public T GetValue<T>(Enum pColumnNameEnum)
        {
            return _GetValue<T>(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        /// <summary>gets specified column as Object, null if dbnull</summary>
        public object GetValue(string pColumnName)
        {
            return _GetValue(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Object, null if dbnull</summary>
        public object GetValue(Enum pColumnNameEnum)
        {
            return _GetValue(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets all columns as List of Object, nulls if dbnull</summary>
        public IList<object> GetValues()
        {
            var res = new List<object>();
            for (int i = 0; i < _myReader.FieldCount; i++)
            {
                res.Add(_GetValue(i));
            }
            return res;
        }

        /// <summary>gets all columns as Dictionary of Object, nulls if dbnull</summary>
        public IDictionary<string, object> GetValuesDict()
        {
            var res = new Dictionary<string, object>();
            for (int i = 0; i < _myReader.FieldCount; i++)
            {
                res[_myReader.GetName(i)] = _GetValue(i);
            }
            return res;
        }

        /// <summary>gets all column names</summary>
        public IList<string> GetNames()
        {
            List<string> res = new List<string>();
            for (Int32 x = 0; x <= _myReader.FieldCount - 1; x++)
            {
                res.Add(_myReader.GetName(x));
            }
            return res;
        }

        #endregion


        #region "String"

        /// <summary>gets specified column as String, empty string if dbnull</summary>
        protected string _GetString(Int32 pColumnNumber)
        {
            return _GetValue<string>(pColumnNumber);
        }
        /// <summary>gets specified column as String, empty string if dbnull</summary>
        public string GetString(string pColumnName)
        {
            return _GetString(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as String, empty string if dbnull</summary>
        public string GetString(Enum pColumnNameEnum)
        {
            return _GetString(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Byte"

        /// <summary>gets specified column as Byte, 0 if dbnull</summary>
        protected byte _GetByte(Int32 pColumnNumber)
        {
            return _GetValue<byte>(pColumnNumber);
        }
        /// <summary>gets specified column as Byte, 0 if dbnull</summary>
        public byte GetByte(string pColumnName)
        {
            return _GetByte(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Byte, 0 if dbnull</summary>
        public byte GetByte(Enum pColumnNameEnum)
        {
            return _GetByte(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Byte, pNullValue if dbnull</summary>
        protected byte _GetByte(Int32 pColumnNumber, byte pNullValue)
        {
            return _GetValue<byte>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Byte, pNullValue if dbnull</summary>
        public byte GetByte(string pColumnName, byte pNullValue)
        {
            return _GetByte(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Byte, pNullValue if dbnull</summary>
        public byte GetByte(Enum pColumnNameEnum, byte pNullValue)
        {
            return _GetByte(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Byte</summary>
        protected byte? _GetByteNullable(Int32 pColumnNumber)
        {
            return _GetValue<byte?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Byte</summary>
        public byte? GetByteNullable(string pColumnName)
        {
            return _GetByteNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Byte</summary>
        public byte? GetByteNullable(Enum pColumnNameEnum)
        {
            return _GetByteNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Int16"

        /// <summary>gets specified column as Int16, 0 if dbnull</summary>
        protected Int16 _GetInt16(Int32 pColumnNumber)
        {
            return _GetValue<Int16>(pColumnNumber);
        }
        /// <summary>gets specified column as Int16, 0 if dbnull</summary>
        public Int16 GetInt16(string pColumnName)
        {
            return _GetInt16(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Int16, 0 if dbnull</summary>
        public Int16 GetInt16(Enum pColumnNameEnum)
        {
            return _GetInt16(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Int16, pNullValue if dbnull</summary>
        protected Int16 _GetInt16(Int32 pColumnNumber, Int16 pNullValue)
        {
            return _GetValue<Int16>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Int16, pNullValue if dbnull</summary>
        public Int16 GetInt16(string pColumnName, Int16 pNullValue)
        {
            return _GetInt16(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Int16, pNullValue if dbnull</summary>
        public Int16 GetInt16(Enum pColumnNameEnum, Int16 pNullValue)
        {
            return _GetInt16(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Int16</summary>
        protected Int16? _GetInt16Nullable(Int32 pColumnNumber)
        {
            return _GetValue<Int16>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Int16</summary>
        public Int16? GetInt16Nullable(string pColumnName)
        {
            return _GetInt16Nullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Int16</summary>
        public Int16? GetInt16Nullable(Enum pColumnNameEnum)
        {
            return _GetInt16Nullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Int32"

        /// <summary>gets specified column as Int32, 0 if dbnull</summary>
        protected Int32 _GetInt32(Int32 pColumnNumber)
        {
            return _GetValue<Int32>(pColumnNumber);
        }
        /// <summary>gets specified column as Int32, 0 if dbnull</summary>
        public Int32 GetInt32(string pColumnName)
        {
            return _GetInt32(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Int32, 0 if dbnull</summary>
        public Int32 GetInt32(Enum pColumnNameEnum)
        {
            return _GetInt32(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Int32, pNullValue if dbnull</summary>
        protected Int32 _GetInt32(Int32 pColumnNumber, Int32 pNullValue)
        {
            return _GetValue<Int32>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Int32, pNullValue if dbnull</summary>
        public Int32 GetInt32(string pColumnName, Int32 pNullValue)
        {
            return _GetInt32(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Int32, pNullValue if dbnull</summary>
        public Int32 GetInt32(Enum pColumnNameEnum, Int32 pNullValue)
        {
            return _GetInt32(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Int32</summary>
        protected Int32? _GetInt32Nullable(Int32 pColumnNumber)
        {
            return _GetValue<Int32?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Int32</summary>
        public Int32? GetInt32Nullable(string pColumnName)
        {
            return _GetInt32Nullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Int32</summary>
        public Int32? GetInt32Nullable(Enum pColumnNameEnum)
        {
            return _GetInt32Nullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Int64"

        /// <summary>gets specified column as Int64, 0 if dbnull</summary>
        protected Int64 _GetInt64(Int32 pColumnNumber)
        {
            return _GetValue<Int64>(pColumnNumber);
        }
        /// <summary>gets specified column as Int64, 0 if dbnull</summary>
        public Int64 GetInt64(string pColumnName)
        {
            return _GetInt64(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Int64, 0 if dbnull</summary>
        public Int64 GetInt64(Enum pColumnNameEnum)
        {
            return _GetInt64(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Int64, pNullValue if dbnull</summary>
        protected Int64 _GetInt64(Int32 pColumnNumber, Int64 pNullValue)
        {
            return _GetValue<Int64>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Int64, pNullValue if dbnull</summary>
        public Int64 GetInt64(string pColumnName, Int64 pNullValue)
        {
            return _GetInt64(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Int64, pNullValue if dbnull</summary>
        public Int64 GetInt64(Enum pColumnNameEnum, Int64 pNullValue)
        {
            return _GetInt64(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Int64</summary>
        protected Int64? _GetInt64Nullable(Int32 pColumnNumber)
        {
            return _GetValue<Int64?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Int64</summary>
        public Int64? GetInt64Nullable(string pColumnName)
        {
            return _GetInt64Nullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Int64</summary>
        public Int64? GetInt64Nullable(Enum pColumnNameEnum)
        {
            return _GetInt64Nullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Single"

        /// <summary>gets specified column as Single, 0 if dbnull</summary>
        protected float _GetSingle(Int32 pColumnNumber)
        {
            return _GetValue<float>(pColumnNumber);
        }
        /// <summary>gets specified column as Single, 0 if dbnull</summary>
        public float GetSingle(string pColumnName)
        {
            return _GetSingle(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Single, 0 if dbnull</summary>
        public float GetSingle(Enum pColumnNameEnum)
        {
            return _GetSingle(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Single, pNullValue if dbnull</summary>
        protected float _GetSingle(Int32 pColumnNumber, float pNullValue)
        {
            return _GetValue<float>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Single, pNullValue if dbnull</summary>
        public float GetSingle(string pColumnName, float pNullValue)
        {
            return _GetSingle(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Single, pNullValue if dbnull</summary>
        public float GetSingle(Enum pColumnNameEnum, float pNullValue)
        {
            return _GetSingle(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Single</summary>
        protected float? _GetSingleNullable(Int32 pColumnNumber)
        {
            return _GetValue<float?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Single</summary>
        public float? GetSingleNullable(string pColumnName)
        {
            return _GetSingleNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Single</summary>
        public float? GetSingleNullable(Enum pColumnNameEnum)
        {
            return _GetSingleNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Double"

        /// <summary>gets specified column as Double, 0 if dbnull</summary>
        protected double _GetDouble(Int32 pColumnNumber)
        {
            return _GetValue<double>(pColumnNumber);
        }
        /// <summary>gets specified column as Double, 0 if dbnull</summary>
        public double GetDouble(string pColumnName)
        {
            return _GetDouble(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Double, 0 if dbnull</summary>
        public double GetDouble(Enum pColumnNameEnum)
        {
            return _GetDouble(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Double, pNullValue if dbnull</summary>
        protected double _GetDouble(Int32 pColumnNumber, double pNullValue)
        {
            return _GetValue<double>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Double, pNullValue if dbnull</summary>
        public double GetDouble(string pColumnName, double pNullValue)
        {
            return _GetDouble(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Double, pNullValue if dbnull</summary>
        public double GetDouble(Enum pColumnNameEnum, double pNullValue)
        {
            return _GetDouble(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Double</summary>
        protected double? _GetDoubleNullable(Int32 pColumnNumber)
        {
            return _GetValue<double?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Double</summary>
        public double? GetDoubleNullable(string pColumnName)
        {
            return _GetDoubleNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Double</summary>
        public double? GetDoubleNullable(Enum pColumnNameEnum)
        {
            return _GetDoubleNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Decimal"

        /// <summary>gets specified column as Decimal, 0 if dbnull</summary>
        protected decimal _GetDecimal(Int32 pColumnNumber)
        {
            return _GetValue<decimal>(pColumnNumber);
        }
        /// <summary>gets specified column as Decimal, 0 if dbnull</summary>
        public decimal GetDecimal(string pColumnName)
        {
            return _GetDecimal(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Decimal, 0 if dbnull</summary>
        public decimal GetDecimal(Enum pColumnNameEnum)
        {
            return _GetDecimal(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Decimal, pNullValue if dbnull</summary>
        protected decimal _GetDecimal(Int32 pColumnNumber, decimal pNullValue)
        {
            return _GetValue<decimal>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Decimal, pNullValue if dbnull</summary>
        public decimal GetDecimal(string pColumnName, decimal pNullValue)
        {
            return _GetDecimal(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Decimal, pNullValue if dbnull</summary>
        public decimal GetDecimal(Enum pColumnNameEnum, decimal pNullValue)
        {
            return _GetDecimal(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Decimal</summary>
        protected decimal? _GetDecimalNullable(Int32 pColumnNumber)
        {
            return _GetValue<decimal?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Decimal</summary>
        public decimal? GetDecimalNullable(string pColumnName)
        {
            return _GetDecimalNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Decimal</summary>
        public decimal? GetDecimalNullable(Enum pColumnNameEnum)
        {
            return _GetDecimalNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "DateTime"

        /// <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        protected DateTime _GetDateTime(Int32 pColumnNumber)
        {
            return _GetValue<DateTime>(pColumnNumber);
        }
        /// <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        public DateTime GetDateTime(string pColumnName)
        {
            return _GetDateTime(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        public DateTime GetDateTime(Enum pColumnNameEnum)
        {
            return _GetDateTime(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as DateTime, pNullValue if dbnull</summary>
        protected DateTime _GetDateTime(Int32 pColumnNumber, DateTime pNullValue)
        {
            return _GetValue<DateTime>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as DateTime, pNullValue if dbnull</summary>
        public DateTime GetDateTime(string pColumnName, DateTime pNullValue)
        {
            return _GetDateTime(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as DateTime, pNullValue if dbnull</summary>
        public DateTime GetDateTime(Enum pColumnNameEnum, DateTime pNullValue)
        {
            return _GetDateTime(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable DateTime</summary>
        protected DateTime? _GetDateTimeNullable(Int32 pColumnNumber)
        {
            return _GetValue<DateTime?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable DateTime</summary>
        public DateTime? GetDateTimeNullable(string pColumnName)
        {
            return _GetDateTimeNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable DateTime</summary>
        public DateTime? GetDateTimeNullable(Enum pColumnNameEnum)
        {
            return _GetDateTimeNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Time"

        /// <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        protected TimeSpan _GetTimeSpan(Int32 pColumnNumber)
        {
            return _GetValue<TimeSpan>(pColumnNumber);
        }
        /// <summary>gets specified column as TimeSpan, MinValue if dbnull</summary>
        public TimeSpan GetTimeSpan(string pColumnName)
        {
            return _GetTimeSpan(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as TimeSpan, MinValue if dbnull</summary>
        public TimeSpan GetTimeSpan(Enum pColumnNameEnum)
        {
            return _GetTimeSpan(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as TimeSpan, pNullValue if dbnull</summary>
        protected TimeSpan _GetTimeSpan(Int32 pColumnNumber, TimeSpan pNullValue)
        {
            return _GetValue<TimeSpan>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as TimeSpan, pNullValue if dbnull</summary>
        public TimeSpan GetTimeSpan(string pColumnName, TimeSpan pNullValue)
        {
            return _GetTimeSpan(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as TimeSpan, pNullValue if dbnull</summary>
        public TimeSpan GetTimeSpan(Enum pColumnNameEnum, TimeSpan pNullValue)
        {
            return _GetTimeSpan(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable TimeSpan</summary>
        protected TimeSpan? _GetTimeSpanNullable(Int32 pColumnNumber)
        {
            return _GetValue<TimeSpan?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable TimeSpan</summary>
        public TimeSpan? GetTimeSpanNullable(string pColumnName)
        {
            return _GetTimeSpanNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable TimeSpan</summary>
        public TimeSpan? GetTimeSpanNullable(Enum pColumnNameEnum)
        {
            return _GetTimeSpanNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Boolean"

        /// <summary>gets specified column as Boolean, False if dbnull</summary>
        protected bool _GetBoolean(Int32 pColumnNumber)
        {
            return _GetValue<bool>(pColumnNumber);
        }
        /// <summary>gets specified column as Boolean, False if dbnull</summary>
        public bool GetBoolean(string pColumnName)
        {
            return _GetBoolean(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Boolean, False if dbnull</summary>
        public bool GetBoolean(Enum pColumnNameEnum)
        {
            return _GetBoolean(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as nullable Boolean</summary>
        protected bool? _GetBooleanNullable(Int32 pColumnNumber)
        {
            return _GetValue<bool?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Boolean</summary>
        public bool? GetBooleanNullable(string pColumnName)
        {
            return _GetBooleanNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Boolean</summary>
        public bool? GetBooleanNullable(Enum pColumnNameEnum)
        {
            return _GetBooleanNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "Guid"

        /// <summary>gets specified column as Guid, null if dbnull</summary>
        protected Guid _GetGuid(Int32 pColumnNumber)
        {
            return _GetValue<Guid>(pColumnNumber);
        }
        /// <summary>gets specified column as Guid, null if dbnull</summary>
        public Guid GetGuid(string pColumnName)
        {
            return _GetGuid(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Guid, null if dbnull</summary>
        public Guid GetGuid(Enum pColumnNameEnum)
        {
            return _GetGuid(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }


        /// <summary>gets specified column as Guid, pNullValue if dbnull</summary>
        protected Guid _GetGuid(Int32 pColumnNumber, Guid pNullValue)
        {
            return _GetValue<Guid>(pColumnNumber, pNullValue);
        }
        /// <summary>gets specified column as Guid, pNullValue if dbnull</summary>
        public Guid GetGuid(string pColumnName, Guid pNullValue)
        {
            return _GetGuid(_myReader.GetOrdinal(pColumnName), pNullValue);
        }
        /// <summary>gets specified column as Guid, pNullValue if dbnull</summary>
        public Guid GetGuid(Enum pColumnNameEnum, Guid pNullValue)
        {
            return _GetGuid(_myReader.GetOrdinal(pColumnNameEnum.ToString()), pNullValue);
        }


        /// <summary>gets specified column as nullable Guid</summary>
        protected Guid? _GetGuidNullable(Int32 pColumnNumber)
        {
            return _GetValue<Guid?>(pColumnNumber);
        }
        /// <summary>gets specified column as nullable Guid</summary>
        public Guid? GetGuidNullable(string pColumnName)
        {
            return _GetGuidNullable(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as nullable Guid</summary>
        public Guid? GetGuidNullable(Enum pColumnNameEnum)
        {
            return _GetGuidNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "ByteArray"

        /// <summary>gets specified column as Byte array, null if dbnull</summary>
        protected byte[] _GetByteArray(Int32 pColumnNumber)
        {
            if (_myReader.IsDBNull(pColumnNumber))
            {
                return null;
            }
            else
            {
                byte[] tempDat = (byte[])_myReader[pColumnNumber];
                byte[] result = new byte[tempDat.Length];
                tempDat.CopyTo(result, 0);
                return result;
            }
        }
        /// <summary>gets specified column as Byte array, null if dbnull</summary>
        public byte[] GetByteArray(string pColumnName)
        {
            return _GetByteArray(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified column as Byte array, null if dbnull</summary>
        public byte[] GetByteArray(Enum pColumnNameEnum)
        {
            return _GetByteArray(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion


        #region "RowVersion"

        /// <summary>gets specified rowversion column as UInt64</summary>
        protected UInt64 _GetRowVersion(Int32 pColumnNumber)
        {
            byte[] tempDat = (byte[])_myReader[pColumnNumber];
            return BitConverter.ToUInt64(tempDat, 0);
        }
        /// <summary>gets specified rowversion column as UInt64</summary>
        public UInt64 GetRowVersion(string pColumnName)
        {
            return _GetRowVersion(_myReader.GetOrdinal(pColumnName));
        }
        /// <summary>gets specified rowversion column as UInt64</summary>
        public UInt64 GetRowVersion(Enum pColumnNameEnum)
        {
            return _GetRowVersion(_myReader.GetOrdinal(pColumnNameEnum.ToString()));
        }

        #endregion



        #region "Implementation of IDataRecord"
        
        /// <summary></summary>
        public object this[string name]
        {
            get
            {
                return _myReader[name];
            }
        }

        /// <summary></summary>
        public object this[int i]
        {
            get
            {
                return _myReader[i];
            }
        }

        /// <summary></summary>
        string IDataRecord.GetName(int i)
        {
            return _myReader.GetName(i);
        }

        /// <summary></summary>
        public string GetDataTypeName(int i)
        {
            return _myReader.GetDataTypeName(i);
        }

        /// <summary></summary>
        Type IDataRecord.GetFieldType(int i)
        {
            return _myReader.GetFieldType(i);
        }

        /// <summary></summary>
        public object GetValue(int i)
        {
            return _myReader.GetValue(i);
        }

        /// <summary></summary>
        public int GetValues(object[] values)
        {
            return _myReader.GetValues(values);
        }

        /// <summary></summary>
        public int GetOrdinal(string name)
        {
            return _myReader.GetOrdinal(name);
        }

        /// <summary></summary>
        public bool GetBoolean(int i)
        {
            return _myReader.GetBoolean(i);
        }

        /// <summary></summary>
        public byte GetByte(int i)
        {
            return _myReader.GetByte(i);
        }

        /// <summary></summary>
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            return _myReader.GetBytes(i, fieldOffset, buffer, bufferoffset, length);
        }

        /// <summary></summary>
        public char GetChar(int i)
        {
            return _myReader.GetChar(i);
        }

        /// <summary></summary>
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            return _myReader.GetChars(i, fieldoffset, buffer, bufferoffset, length);
        }

        /// <summary></summary>
        public Guid GetGuid(int i)
        {
            return _myReader.GetGuid(i);
        }

        /// <summary></summary>
        public short GetInt16(int i)
        {
            return _myReader.GetInt16(i);
        }

        /// <summary></summary>
        public int GetInt32(int i)
        {
            return _myReader.GetInt32(i);
        }

        /// <summary></summary>
        public long GetInt64(int i)
        {
            return _myReader.GetInt64(i);
        }

        /// <summary></summary>
        public float GetFloat(int i)
        {
            return _myReader.GetFloat(i);
        }

        /// <summary></summary>
        public double GetDouble(int i)
        {
            return _myReader.GetDouble(i);
        }

        /// <summary></summary>
        public string GetString(int i)
        {
            return _myReader.GetString(i);
        }

        /// <summary></summary>
        public decimal GetDecimal(int i)
        {
            return _myReader.GetDecimal(i);
        }

        /// <summary></summary>
        public DateTime GetDateTime(int i)
        {
            return _myReader.GetDateTime(i);
        }

        /// <summary></summary>
        public IDataReader GetData(int i)
        {
            return _myReader.GetData(i);
        }

        /// <summary></summary>
        public bool IsDBNull(int i)
        {
            return _myReader.IsDBNull(i);
        }

        #endregion



        #region "Dispose"

        bool disposed = false;

        /// <summary></summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary></summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (this._myReader != null)
                {
                    this._myReader.Dispose();
                }
            }

            disposed = true;
        }

        #endregion

    }




    /// <summary>
    /// Wraps a DataReader object to present data to a consumer
    /// </summary>
    /// <remarks></remarks>
    public class DbResultSet : DbRow, IDataReader, IDisposable
    {

        /// <summary></summary>
        protected internal DbResultSet(Database pDb, IDataReader pReader) : base(pDb, pReader) { }

        /// <summary>Number of records affected by the query</summary>
        public Int32 RecordsAffected
        {
            get { return _myReader.RecordsAffected; }
        }

        /// <summary>Advance to the first or subsequent records. Returns true if a record was found.</summary>
        public bool Read()
        {
            return _myReader.Read();
        }

        /// <summary>Advance to the second or subsequent result sets. Returns true if the result set was found.</summary>
        public bool NextResult()
        {
            var res = _myReader.NextResult();
            if (res) LoadCustomMappings();
            return res;
        }

        /// <summary></summary>
        protected internal void LoadDataTable(DataTable pioDt)
        {
            pioDt.Load(_myReader);

            if (_myDb.HasCustomDbToClrMappings())
            {
                Utility.RemoveDataTableConstraints(pioDt);
                foreach (DataRow rw in pioDt.Rows)
                {
                    for (int dtColIdx = 0; dtColIdx < pioDt.Columns.Count; dtColIdx++)
                    {
                        var nm = pioDt.Columns[dtColIdx].ColumnName;
                        //DataTable requires DbNull instead of null
                        rw[dtColIdx] = _myDb.MapDbToClr(rw[dtColIdx], _fieldDbTypes[nm], _fieldTypes[nm]) ?? DBNull.Value;
                    }
                }
            }

            if (!_myReader.IsClosed)
            {
                //DataTable.Load() automatically advances to the next result, or closes if done
                //need to load mappings for the new result
                LoadCustomMappings();
            }
        }

        /// <summary></summary>
        protected internal void LoadDataSet(DataSet pioDs)
        {
            var idx = 1;
            do
            {
                var curDt = new DataTable();
                LoadDataTable(curDt);
                curDt.TableName = string.Format("Table{0}", idx);
                pioDs.Tables.Add(curDt);
                idx++;
            } while (!_myReader.IsClosed);   //DataTable.Load() automatically advances to the next result, or closes if done
        }



        #region "Implementation of IDataReader"

        /// <summary></summary>
        public void Close()
        {
            _myReader.Close();
        }

        /// <summary></summary>
        public DataTable GetSchemaTable()
        {
            return _myReader.GetSchemaTable();
        }

        /// <summary></summary>
        public int Depth
        {
            get
            {
                return _myReader.Depth;
            }
        }

        /// <summary></summary>
        public bool IsClosed
        {
            get
            {
                return _myReader.IsClosed;
            }
        }

        #endregion

    }


}

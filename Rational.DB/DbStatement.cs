﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rational.DB
{

    /// <summary>
    /// Represents a database SQL statement
    /// </summary>
    public interface IDbStatement
    {
        /// <summary>Collection of database parameters associated with the statement</summary>
        DbParameterCollection Parameters { get; }

        /// <summary>Flag for stored procedures</summary>
        bool IsStoredProcedure { get; set; }

        /// <summary>Query timeout in seconds</summary>
        Int32 TimeoutSecs { get; set; }

        /// <summary>Type of database for SQL dialect</summary>
        eDbType DbType { get; set; }

        /// <summary>Prefix for SQL named parameters</summary>
        string ParameterPrefix { get; set; }

        /// <summary>Prefix for escaping SQL identifiers</summary>
        string QuotePrefix { get; set; }

        /// <summary>Suffix for escaping SQL identifiers</summary>
        string QuoteSuffix { get; set; }

        /// <summary>Gets the generated SQL for the statement</summary>
        string GetSql();
    }




    #region "SqlFragment"


    /// <summary>
    /// Represents a chunk of SQL within a statement
    /// </summary>
    public class SqlFragment
    {

        /// <summary></summary>
        protected DbStatementBase _parentStatement;

        /// <summary></summary>
        protected StringBuilder Sql = new StringBuilder();

        /// <summary></summary>
        protected internal SqlFragment(DbStatementBase pParentStatement)
        {
            _parentStatement = pParentStatement;
        }

        /// <summary>
        /// Append a string to the SQL fragment.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        public void Append(string pSql)
        {
            Sql.AppendLine(pSql);
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParams">Parameter(s) to associate with the SQL. For numbered parameters, pass a scalar value or a list of scalars. For named parameters, pass an object with a property for each value.</param>
        public void Append(string pSql, object pParams)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParams }, _parentStatement));
        }


        #region "Multiple param arguments"

        //NOTE: The multiple distinct signatures was intentional.
        //  Tried using "params" keyword and optional parameters.
        //  The former did not work properly with a list of strings, the later with nulls.


        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam4">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3, object pParam4)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3, pParam4 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam4">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam5">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3, object pParam4, object pParam5)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3, pParam4, pParam5 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam4">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam5">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam6">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3, object pParam4, object pParam5, object pParam6)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3, pParam4, pParam5, pParam6 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam4">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam5">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam6">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam7">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3, object pParam4, object pParam5, object pParam6, object pParam7)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam4">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam5">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam6">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam7">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam8">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3, object pParam4, object pParam5, object pParam6, object pParam7, object pParam8)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7, pParam8 }, _parentStatement));
        }

        /// <summary>
        /// Append a string to the SQL fragment. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam4">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam5">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam6">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam7">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam8">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam9">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Append(string pSql, object pParam0, object pParam1, object pParam2, object pParam3, object pParam4, object pParam5, object pParam6, object pParam7, object pParam8, object pParam9)
        {
            Sql.AppendLine(DbStatementBase.ResolveParams(pSql, new object[] { pParam0, pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7, pParam8, pParam9 }, _parentStatement));
        }

        #endregion


        /// <summary>
        /// Append a string to the SQL fragment. Can reference and replace identifiers (table or column names). Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql"></param>
        /// <param name="pIdentifiers"></param>
        /// <param name="pParams">Parameter(s) to associate with the SQL. For numbered parameters, pass a scalar value or a list of scalars. For named parameters, pass an object with a property for each value.</param>
        public void AppendWithIdentifiers(string pSql, object pIdentifiers, object pParams)
        {
            Sql.AppendLine(DbStatementBase.ResolveIdentifiersAndParams(pSql, pIdentifiers, pParams, _parentStatement));
        }

        /// <summary>
        /// Append a statement to the SQL, incorporating any existing parameters
        /// </summary>
        /// <param name="pStatement">Statement to append</param>
        public void Append(IDbStatement pStatement)
        {
            var parms = new List<object>();
            foreach (var p in pStatement.Parameters)
            {
                parms.Add(p);
            }

            Sql.AppendLine(DbStatementBase.ResolveParams(pStatement.GetSql(), parms.ToArray(), _parentStatement));
        }

        /// <summary>
        /// Append a raw string to the SQL. Does not look for parameters or identifiers.
        /// </summary>
        /// <param name="pSql"></param>
        public void AppendRaw(string pSql)
        {
            Sql.Append(pSql);
        }

        /// <summary>Length of the SQL fragment</summary>
        public int Length { get { return Sql.Length; } }

        /// <summary>Clear the fragment</summary>
        public void Clear()
        {
            Sql.Clear();
        }

        /// <summary>Get final SQL for the fragment</summary>
        public string GetSql()
        {
            return Sql.ToString();
        }

    }



    /// <summary>
    /// SQL fragment for use in the fields clause of a select statement
    /// </summary>
    public class SqlFragmentFields : SqlFragment
    {

        /// <summary></summary>
        protected internal SqlFragmentFields(DbStatementBase pParentStatement)
            : base(pParentStatement)
        {
        }

        /// <summary>Add field to the list. Automatically includes delimiter.</summary>
        public void Add(string pField, string pPrefix = null, string pAlias = null)
        {
            var delim = "";
            if (Sql.Length > 0) delim = ", ";

            var prefixStr = "";
            if (!string.IsNullOrEmpty(pPrefix)) prefixStr = string.Format("{0}{1}{2}.", _parentStatement.QuotePrefix, pPrefix, _parentStatement.QuoteSuffix);

            var aliasStr = "";
            if (!string.IsNullOrEmpty(pAlias)) aliasStr = string.Format(" AS {0}{1}{2}", _parentStatement.QuotePrefix, pAlias, _parentStatement.QuoteSuffix);
            Sql.AppendFormat("{0}{1}{2}{3}{4}{5}", delim, prefixStr, _parentStatement.QuotePrefix, pField, _parentStatement.QuoteSuffix, aliasStr);
        }

        /// <summary>Add fields to the list. Automatically includes delimiters.</summary>
        public void Add(IEnumerable<string> pFields, string pPrefix = null, IEnumerable<string> pAliases = null)
        {
            var prefixStr = "";
            if (!string.IsNullOrEmpty(pPrefix)) prefixStr = string.Format("{0}{1}{2}.", _parentStatement.QuotePrefix, pPrefix, _parentStatement.QuoteSuffix);

            var delim = "";
            if (Sql.Length > 0) delim = ", ";

            List<string> aliases = null;
            if (pAliases != null) aliases = pAliases.ToList();

            var curIdx = 0;
            foreach (var fld in pFields)
            {
                var aliasStr = "";
                if ((aliases != null) && !string.IsNullOrEmpty(aliases[curIdx])) aliasStr = string.Format(" AS {0}{1}{2}", _parentStatement.QuotePrefix, aliases[curIdx], _parentStatement.QuoteSuffix);
                Sql.AppendFormat("{0}{1}{2}{3}{4}{5}", delim, prefixStr, _parentStatement.QuotePrefix, fld, _parentStatement.QuoteSuffix, aliasStr);
                delim = ", ";
                curIdx++;
            }
        }

        /// <summary>Add all fields defines in specified Table enum.</summary>
        public void Add<TEnum>(string pPrefix = null) where TEnum : struct
        {
            Add(TableDefinitionAttributes.GetColumnAttributes<TEnum>().Select(i => i.Value.SqlName).ToList(), pPrefix, TableDefinitionAttributes.GetColumnAttributes<TEnum>().Select(i => i.Value.ClrName).ToList());
        }

        /// <summary>Add specified fields from a Table enum</summary>
        public void Add(IEnumerable<Enum> pFields, string pPrefix = null)
        {
            Add(pFields.Select(f => TableDefinitionAttributes.GetColumnSqlName(f)).ToList(), pPrefix, pFields.Select(f => TableDefinitionAttributes.GetColumnClrName(f)).ToList());
        }

        /// <summary>Add specified field from a Table enum</summary>
        public void Add(Enum pField, string pPrefix = null, string pAlias = null)
        {
            Add(TableDefinitionAttributes.GetColumnSqlName(pField), pPrefix, pAlias ?? TableDefinitionAttributes.GetColumnClrName(pField));
        }

    }

    /// <summary>
    /// SQL fragment for use in the WHERE clause of a select statement
    /// </summary>
    public class SqlFragmentWhere : SqlFragment
    {

        /// <summary></summary>
        protected internal SqlFragmentWhere(DbStatementBase pParentStatement)
            : base(pParentStatement)
        {
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including AND if necessary. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParams">Parameter(s) to associate with the SQL. For numbered parameters, pass a scalar value or a list of scalars. For named parameters, pass an object with a property for each value.</param>
        public void And(string pSql, object pParams)
        {
            Append(string.Format("{0}{1}", (Sql.Length > 0) ? "AND " : "", pSql), pParams);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including AND if necessary. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void And(string pSql, object pParam0, object pParam1)
        {
            Append(string.Format("{0}{1}", (Sql.Length > 0) ? "AND " : "", pSql), pParam0, pParam1);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including AND if necessary. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void And(string pSql, object pParam0, object pParam1, object pParam2)
        {
            Append(string.Format("{0}{1}", (Sql.Length > 0) ? "AND " : "", pSql), pParam0, pParam1, pParam2);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including AND if necessary. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void And(string pSql, object pParam0, object pParam1, object pParam2, object pParam3)
        {
            Append(string.Format("{0}{1}", (Sql.Length > 0) ? "AND " : "", pSql), pParam0, pParam1, pParam2, pParam3);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including AND if necessary. Can reference and replace identifiers (table or column names). Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql"></param>
        /// <param name="pIdentifiers"></param>
        /// <param name="pParams"></param>
        public void AndWithIdentifiers(string pSql, object pIdentifiers, object pParams)
        {
            AppendWithIdentifiers(string.Format("{0}{1}", (Sql.Length > 0) ? "AND " : "", pSql), pIdentifiers, pParams);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including OR if necessary. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParams">Parameter(s) to associate with the SQL. For numbered parameters, pass a scalar value or a list of scalars. For named parameters, pass an object with a property for each value.</param>
        public void Or(string pSql, object pParams)
        {
            Append(string.Format("{0}{1}", (Sql.Length > 0) ? "OR " : "", pSql), pParams);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including OR if necessary. Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public void Or(string pSql, object pParam0, object pParam1, object pParam2, object pParam3)
        {
            Append(string.Format("{0}{1}", (Sql.Length > 0) ? "OR " : "", pSql), pParam0, pParam1, pParam2, pParam3);
        }

        /// <summary>
        /// Append a condition to the WHERE fragment, including OR if necessary. Can reference and replace identifiers (table or column names). Can reference and include DB parameters.
        /// </summary>
        /// <param name="pSql"></param>
        /// <param name="pIdentifiers"></param>
        /// <param name="pParams"></param>
        public void OrWithIdentifiers(string pSql, object pIdentifiers, object pParams)
        {
            AppendWithIdentifiers(string.Format("{0}{1}", (Sql.Length > 0) ? "OR " : "", pSql), pIdentifiers, pParams);
        }

    }

    /// <summary>
    /// SQL fragment for use in the GROUP BY clause of a select statement
    /// </summary>
    public class SqlFragmentGroupBy : SqlFragment
    {

        /// <summary></summary>
        protected internal SqlFragmentGroupBy(DbStatementBase pParentStatement)
            : base(pParentStatement)
        {
        }

        /// <summary></summary>
        public void Add(string pField, string pPrefix = null)
        {
            var delim = "";
            if (Sql.Length > 0) delim = ", ";

            var prefixStr = "";
            if (!string.IsNullOrEmpty(pPrefix)) prefixStr = string.Format("{0}{1}{2}.", _parentStatement.QuotePrefix, pPrefix, _parentStatement.QuoteSuffix);

            Sql.AppendFormat("{0}{1}{2}{3}{4}", delim, prefixStr, _parentStatement.QuotePrefix, pField, _parentStatement.QuoteSuffix);
        }

        /// <summary></summary>
        public void Add(Enum pFieldEnum, string pPrefix = null)
        {
            Add(TableDefinitionAttributes.GetColumnSqlName(pFieldEnum), pPrefix);
        }

    }

    /// <summary>
    /// SQL fragment for use in the ORDER BY clause of a select statement
    /// </summary>
    public class SqlFragmentOrderBy : SqlFragment
    {
        /// <summary></summary>
        public enum eOrderByDirection
        {
            /// <summary>Ascending</summary>
            ASC,

            /// <summary>Descending</summary>
            DESC
        }

        /// <summary></summary>
        protected internal SqlFragmentOrderBy(DbStatementBase pParentStatement)
            : base(pParentStatement)
        {
        }

        /// <summary></summary>
        public void Add(string pField, eOrderByDirection pDirection = eOrderByDirection.ASC, string pPrefix = null)
        {
            var delim = "";
            if (Sql.Length > 0) delim = ", ";

            var prefixStr = "";
            if (!string.IsNullOrEmpty(pPrefix)) prefixStr = string.Format("{0}{1}{2}.", _parentStatement.QuotePrefix, pPrefix, _parentStatement.QuoteSuffix);

            Sql.AppendFormat("{0}{1}{2}{3}{4} {5}", delim, prefixStr, _parentStatement.QuotePrefix, pField, _parentStatement.QuoteSuffix, pDirection);
        }

        /// <summary></summary>
        public void Add(Enum pFieldEnum, eOrderByDirection pDirection = eOrderByDirection.ASC, string pPrefix = null)
        {
            Add(TableDefinitionAttributes.GetColumnSqlName(pFieldEnum), pDirection, pPrefix);
        }

    }

    #endregion


    #region "CTE"


    /// <summary>
    /// Represents a list of SQL statements used for common table expressions (CTE)
    /// </summary>
    public class SqlCteCollection
    {

        /// <summary></summary>
        protected DbStatementBase _parentStatement;

        /// <summary></summary>
        protected internal List<string> _names;

        /// <summary></summary>
        protected internal List<DbStatement> _statements;

        /// <summary></summary>
        protected internal SqlCteCollection(DbStatementBase pParentStatement)
        {
            _parentStatement = pParentStatement;
            _names = new List<string>();
            _statements = new List<DbStatement>();
        }

        /// <summary>
        /// Add a CTE to the list, incorporating any existing parameters
        /// </summary>
        /// <param name="pName">Name of CTE</param>
        /// <param name="pStatement">Statement to add</param>
        public void Add(string pName, DbStatement pStatement)
        {
            var parms = new List<object>();
            foreach (var p in pStatement.Parameters)
            {
                parms.Add(p);
            }

            var stmt = new DbStatement(DbStatementBase.ResolveParams(pStatement.GetSql(), parms.ToArray(), _parentStatement));
            _names.Add(pName);
            _statements.Add(stmt);
        }

    }

    #endregion




    /// <summary>
    /// Base class for statements
    /// </summary>
    public abstract class DbStatementBase : IDbStatement
    {
        /// <summary></summary>
        protected DbStatementBase()
        {
            ParameterPrefix = DefaultParameterPrefix;
            QuotePrefix = DefaultQuotePrefix;
            QuoteSuffix = DefaultQuoteSuffix;
        }

        /// <summary></summary>
        protected DbParameterCollection _parameters = new DbParameterCollection();
        /// <summary></summary>
        public DbParameterCollection Parameters
        {
            get { return _parameters; }
        }

        /// <summary>Set to true if the SQL is executing a stored procedure</summary>
        public bool IsStoredProcedure { get; set; }

        /// <summary>Timeout in seconds before statement will be terminated</summary>
        public Int32 TimeoutSecs { get; set; }

        /// <summary>Target database type</summary>
        public eDbType DbType { get; set; }

        /// <summary>Prefix character for parameter names</summary>
        public string ParameterPrefix { get; set; }

        /// <summary>Prefix delimiter for escaping identifiers (field names)</summary>
        public string QuotePrefix { get; set; }

        /// <summary>Suffix delimiter for escaping identifiers (field names)</summary>
        public string QuoteSuffix { get; set; }

        /// <summary></summary>
        public string GetSql() { return null; }


        /// <summary>Returns the specified identifier escaped with the defined delimiters</summary>
        public string QuoteIdentifier(string pIdentifier)
        {
            return string.Format("{0}{1}{2}", QuotePrefix, pIdentifier, QuoteSuffix);
        }

        /// <summary>Returns the escaped table name, using the specified Table enum</summary>
        public string TableIdentifier<TEnum>() where TEnum : struct
        {
            return string.Format("{0}{1}{2}", QuotePrefix, TableDefinitionAttributes.GetTableSqlName<TEnum>(), QuoteSuffix);
        }

        /// <summary>Returns the escaped field name, using the specified Table enum value</summary>
        public string ColumnIdentifier(Enum pColumnEnum)
        {
            return string.Format("{0}{1}{2}", QuotePrefix, TableDefinitionAttributes.GetColumnSqlName(pColumnEnum), QuoteSuffix);
        }


        #region "ResolveParams"

        /// <summary>prefix used in specifying statements. will be converted to db prefix on run</summary>
        protected internal const string DefaultParameterPrefix = "@";

        /// <summary></summary>
        protected internal const string DefaultQuotePrefix = "";

        /// <summary></summary>
        protected internal const string DefaultQuoteSuffix = "";

        static System.Text.RegularExpressions.Regex regexParams = new System.Text.RegularExpressions.Regex(@"(?<!@)@\w+", System.Text.RegularExpressions.RegexOptions.Compiled);
        /// <summary></summary>
        protected internal static string ResolveParams(string pToAdd, object[] pParamData, IDbStatement pStatement)
        {
            //parameters can be: 
            //  positional array of data values, with numbered placeholders
            //  single object, with properties matching named placeholders
            //  single dictionary, with items matching named placeholders
            //a value can be:
            //  scalar (int, string, datetime, boolean, etc)
            //  enumerable of scalars (for multivalue parameters)
            //  DbParameter or IDbDataParameter object
            //  enumerable of DbParameter or IDbDataParameter objects (for multivalue parameters)

            return regexParams.Replace(pToAdd, match =>
            {
                string curParamName = match.Value.Substring(1);

                object curParamData;
                Type curParamType = null;

                int paramIdx;
                if (int.TryParse(curParamName, out paramIdx))
                {
                    // numbered parameter
                    if (paramIdx < 0 || paramIdx >= pParamData.Length) throw new ArgumentException(string.Format("Could not find data for parameter '@{0}'", curParamName));
                    curParamData = pParamData[paramIdx];
                    curParamType = (curParamData != null) ? curParamData.GetType() : null;
                }
                else
                {
                    // named parameter
                    if (pParamData.Length != 1) throw new ArgumentException("Named parameters require a single data object");
                    if (pParamData[0] is IDictionary)
                    {
                        var paramDataDict = pParamData[0] as IDictionary;
                        if (paramDataDict.Contains(curParamName))
                        {
                            curParamData = paramDataDict[curParamName];
                        }
                        else
                        {
                            //try to find in another case
                            var matchKey = paramDataDict.Keys.Cast<string>().FirstOrDefault(i => i.Equals(curParamName, StringComparison.OrdinalIgnoreCase));
                            if (matchKey != null)
                            {
                                curParamData = paramDataDict[matchKey];
                            }
                            else
                            {
                                throw new ArgumentException(string.Format("Could not find data for parameter '@{0}'", curParamName));
                            }
                        }
                    }
                    else
                    {
                        // single object with named properties
                        var dataObj = pParamData[0];
                        var prop = Utility.GetPropertyInfoCaseInsensitive(dataObj.GetType(), curParamName);
                        if (prop != null)
                        {
                            curParamData = prop.GetValue(dataObj, null);
                            curParamType = prop.PropertyType;
                        }
                        else
                        {
                            var fld = Utility.GetFieldInfoCaseInsensitive(dataObj.GetType(), curParamName);
                            if (fld != null)
                            {
                                curParamData = fld.GetValue(dataObj);
                                curParamType = fld.FieldType;
                            }
                            else
                            {
                                throw new ArgumentException(string.Format("Could not find data for parameter '@{0}'", curParamName));
                            }
                        }
                    }
                }

                // Expand collections to parameter lists (not string or byte array)
                if ((curParamData is System.Collections.IEnumerable) &&
                    (!(curParamData is string)) &&
                    (!(curParamData is byte[])))
                {
                    var sb = new StringBuilder();
                    var delim = "";
                    curParamType = Utility.GetEnumerableType(curParamData);
                    foreach (var curParamIdxData in curParamData as System.Collections.IEnumerable)
                    {
                        var newParamName = pStatement.Parameters.Count.ToString();
                        sb.AppendFormat("{0}{1}{2}", delim, pStatement.ParameterPrefix, newParamName);
                        delim = ",";
                        if (curParamIdxData is DbParameter)
                        {
                            var p = curParamIdxData as DbParameter;
                            p.ParameterName = newParamName;
                            pStatement.Parameters.Add(p);
                        }
                        else if (curParamIdxData is System.Data.IDbDataParameter)
                        {
                            var p = curParamIdxData as System.Data.IDbDataParameter;
                            p.ParameterName = newParamName;
                            pStatement.Parameters.Add(p);
                        }
                        else
                        {
                            pStatement.Parameters.Add(newParamName, curParamIdxData, curParamType);
                        }
                    }
                    return sb.ToString();
                }
                else
                {
                    var newParamName = pStatement.Parameters.Count.ToString();
                    if (curParamData is DbParameter)
                    {
                        var p = curParamData as DbParameter;
                        p.ParameterName = newParamName;
                        pStatement.Parameters.Add(p);
                    }
                    else if (curParamData is System.Data.IDbDataParameter)
                    {
                        var p = curParamData as System.Data.IDbDataParameter;
                        p.ParameterName = newParamName;
                        pStatement.Parameters.Add(p);
                    }
                    else
                    {
                        pStatement.Parameters.Add(newParamName, curParamData, curParamType);
                    }
                    return string.Format("{0}{1}", pStatement.ParameterPrefix, newParamName);
                }
            }
            );
        }

        /// <summary></summary>
        protected internal static string ResolveIdentifiersAndParams(string pToAdd, object pIdentifiers, object pParamData, IDbStatement pStatement)
        {
            var newSql = FormatString.Format(pToAdd, pIdentifiers);
            object[] newParams = { pParamData };
            return ResolveParams(newSql, new object[] { pParamData }, pStatement);
        }

        #endregion


    }

    /// <summary>
    /// Simple statement wrapper
    /// </summary>
    public class DbStatement : DbStatementBase, IDbStatement
    {

        /// <summary></summary>
        public SqlFragment Statement { get; protected set; }

        /// <summary></summary>
        public DbStatement()
        {
            Init();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        public DbStatement(string pSql)
        {
            Init();
            Statement.Append(pSql);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParams">Parameter(s) to associate with the SQL. For numbered parameters, pass a scalar value or a list of scalars. For named parameters, pass an object with a property for each value.</param>
        public DbStatement(string pSql, object pParams)
        {
            Init();
            Statement.Append(pSql, pParams);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public DbStatement(string pSql, object pParam0, object pParam1)
        {
            Init();
            Statement.Append(pSql, pParam0, pParam1);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public DbStatement(string pSql, object pParam0, object pParam1, object pParam2)
        {
            Init();
            Statement.Append(pSql, pParam0, pParam1, pParam2);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public DbStatement(string pSql, object pParam0, object pParam1, object pParam2, object pParam3)
        {
            Init();
            Statement.Append(pSql, pParam0, pParam1, pParam2, pParam3);
        }

        /// <summary></summary>
        protected void Init()
        {
            Statement = new SqlFragment(this);
        }

        /// <summary></summary>
        public new string GetSql()
        {
            return Statement.GetSql();
        }

    }

    /// <summary>
    /// Wrapper class for a stored procedure statement
    /// </summary>
    public class DbProcedureCall : DbStatementBase, IDbStatement
    {

        /// <summary></summary>
        public new bool IsStoredProcedure
        {
            get { return true; }
            set
            {
                if (value == false)
                    throw new ArgumentException("DbProcedureCall: Stored procedures are required");
            }
        }

        /// <summary></summary>
        public string SpName { get; set; }


        /// <summary></summary>
        public DbProcedureCall()
        {
        }

        /// <summary></summary>
        public DbProcedureCall(string pSpName)
        {
            SpName = pSpName;
        }

        /// <summary></summary>
        public new string GetSql()
        {
            return SpName;
        }

    }


    /// <summary>
    /// Builder for a SQL statement with a WHERE condition (e.g. UPDATE, DELETE). Use DbSelectStatement for a SELECT.
    /// </summary>
    public class DbConditionalStatement : DbStatementBase, IDbStatement
    {

        /// <summary></summary>
        public SqlFragment Action { get; protected set; }

        /// <summary></summary>
        public SqlFragmentWhere Where { get; protected set; }

        /// <summary></summary>
        public DbConditionalStatement()
        {
            Init();
        }

        /// <summary></summary>
        public DbConditionalStatement(string pActionSql)
        {
            Init();
            Action.Append(pActionSql);
        }

        /// <summary></summary>
        protected void Init()
        {
            Action = new SqlFragment(this);
            Where = new SqlFragmentWhere(this);
        }

        /// <summary></summary>
        public new string GetSql()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Action.GetSql());
            if (Where.Length > 0)
            {
                sb.AppendLine(" WHERE ");
                sb.AppendLine(Where.GetSql());
            }
            else
            {
                throw new Exception("DbConditionalStatementBuilder: Conditional statement must include a condition");
            }

            return sb.ToString();
        }

    }


    /// <summary>
    /// Builder for a SQL SELECT statement.
    /// </summary>
    public class DbSelectStatement : DbStatementBase, IDbStatement
    {

        /// <summary>Fields clause of statement</summary>
        public SqlFragmentFields Fields { get; protected set; }

        /// <summary>FROM clause of statement</summary>
        public SqlFragment From { get; protected set; }

        /// <summary>WHERE clause of statement</summary>
        public SqlFragmentWhere Where { get; protected set; }

        /// <summary>GROUP BY clause of statement</summary>
        public SqlFragmentGroupBy GroupBy { get; protected set; }

        /// <summary>ORDER BY clause of statement</summary>
        public SqlFragmentOrderBy OrderBy { get; protected set; }

        /// <summary>Limit for query (e.g. TOP)</summary>
        public Int32? ResultMax { get; set; }

        /// <summary>Include DISTINCT modifier</summary>
        public bool Distinct { get; set; }

        /// <summary>WITH clauses for common table expressions (CTE)</summary>
        public SqlCteCollection WithCte { get; protected internal set; }

        /// <summary></summary>
        public DbSelectStatement()
        {
            Init();
        }

        /// <summary></summary>
        public DbSelectStatement(string pFrom, string pFields)
        {
            Init();
            From.Append(pFrom);
            Fields.Append(pFields);
        }

        /// <summary></summary>
        protected void Init()
        {
            Fields = new SqlFragmentFields(this);
            From = new SqlFragment(this);
            Where = new SqlFragmentWhere(this);
            GroupBy = new SqlFragmentGroupBy(this);
            OrderBy = new SqlFragmentOrderBy(this);
            WithCte = new SqlCteCollection(this);
        }

        /// <summary></summary>
        public new string GetSql()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if ((this.WithCte != null) && (this.WithCte._names.Count > 0))
            {
                for (int cteIdx = 0; cteIdx < this.WithCte._names.Count; cteIdx++)
                {
                    sb.AppendLine((cteIdx == 0) ? "WITH " : ", ");
                    sb.AppendLine(string.Format(" {0} AS (", this.QuoteIdentifier(this.WithCte._names[cteIdx])));
                    sb.AppendLine(this.WithCte._statements[cteIdx].GetSql());
                    sb.AppendLine(")");
                }
            }

            sb.Append("SELECT");
            if (this.Distinct) sb.Append(" DISTINCT");

            //handle ResultMax for different DBs
            if (ResultMax.HasValue)
            {
                switch (this.DbType)
                {
                    case eDbType.Oracle:
                        break;
                    case eDbType.MySql:
                        break;
                    default:
                        sb.AppendFormat(" TOP {0}", ResultMax.Value);
                        break;
                }
            }

            sb.AppendLine();
            sb.AppendLine(Fields.GetSql());
            sb.AppendLine("FROM ");
            sb.AppendLine(From.GetSql());

            if (Where.Length > 0)
            {
                sb.AppendLine("WHERE ");
                sb.AppendLine(Where.GetSql());
            }

            if (ResultMax.HasValue && (this.DbType == eDbType.Oracle))
            {
                //handle ResultMax for Oracle
                if (Where.Length > 0)
                {
                    sb.AppendFormat("AND ROWNUM <= {0}", ResultMax);
                }
                else
                {
                    sb.AppendFormat("WHERE ROWNUM <= {0}", ResultMax);
                }
                sb.AppendLine();
            }

            if (GroupBy.Length > 0)
            {
                sb.AppendLine("GROUP BY ");
                sb.AppendLine(GroupBy.GetSql());
            }
            if (OrderBy.Length > 0)
            {
                sb.AppendLine("ORDER BY ");
                sb.AppendLine(OrderBy.GetSql());
            }

            if (ResultMax.HasValue && (this.DbType == eDbType.MySql))
            {
                //handle ResultMax for MySQL
                sb.AppendFormat("LIMIT {0}", ResultMax);
                sb.AppendLine();
            }

            return sb.ToString();
        }

    }


    /// <summary>
    /// Wrapper for a paged SELECT statement.
    /// </summary>
    public class DbSelectPager
    {

        /// <summary></summary>
        public DbSelectPager()
        {
            PagingMethod = ePagingMethod.OFFSET;
        }

        /// <summary></summary>
        public DbSelectPager(DbSelectStatement pSelectStatement)
        {
            SelectStatement = pSelectStatement;
            PagingMethod = ePagingMethod.OFFSET;
        }

        /// <summary></summary>
        public enum ePagingMethod
        {
            /// <summary>Paging not supported</summary>
            NONE = 0,

            /// <summary>Implement paging using OFFSET and FETCH clauses</summary>
            OFFSET = 10,

            /// <summary>Implement paging with a wrapper common table expression</summary>
            CTE = 20
        }

        /// <summary>Base statement to be paged</summary>
        public DbSelectStatement SelectStatement { get; set; }

        /// <summary>Target page to be retrieved. ResultPage or ResultOffset must be defined.</summary>
        public Int32? ResultPage { get; set; }

        /// <summary>Zero-based offset of first record to be returned. ResultPage or ResultOffset must be defined.</summary>
        public Int32? ResultOffset { get; set; }

        /// <summary>Maximum rows to be retrieved</summary>
        public Int32 ResultMax { get; set; }

        /// <summary>Whether to calculate total row/page counts. Requires an additional query.</summary>
        public bool CalculateTotals { get; set; }

        /// <summary>Total rows available, if CalculateTotals was selected</summary>
        public Int32? TotalRows { get; set; }

        /// <summary>Total pages available, if using ResultPage and CalculateTotals was selected</summary>
        public Int32? TotalPages { get; set; }

        /// <summary>SQL method to be used for paging</summary>
        public ePagingMethod PagingMethod { get; set; }


        /// <summary></summary>
        public string GetSql()
        {
            if (SelectStatement == null) throw new ArgumentException("DbSelectPager: SelectStatement required");
            if (SelectStatement.OrderBy.Length == 0) throw new ArgumentException("DbSelectPager: ORDER clause required");
            if (this.ResultMax <= 0) throw new ArgumentException("DbSelectPager: Invalid ResultMax");

            var skipRows = 0;
            if (this.ResultOffset.HasValue)
            {
                if (this.ResultOffset < 0) throw new ArgumentException("DbSelectPager: Invalid ResultOffset");
                skipRows = this.ResultOffset.Value;
            }
            else if (this.ResultPage.HasValue)
            {
                if (this.ResultPage <= 0) throw new ArgumentException("DbSelectPager: Invalid ResultPage");
                skipRows = (this.ResultPage.Value - 1) * this.ResultMax;
            }
            else
            {
                throw new ArgumentException("DbSelectPager: ResultOffset or ResultPage are required");
            }

            SelectStatement.ResultMax = null;
            if (skipRows == 0)
            {
                SelectStatement.ResultMax = ResultMax;
                System.Text.StringBuilder sb = new System.Text.StringBuilder(SelectStatement.GetSql());
                SelectStatement.ResultMax = null;

                return sb.ToString();
            }
            else
            {
                //modify builder to support paging
                if (PagingMethod == ePagingMethod.OFFSET)
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.AppendLine(SelectStatement.GetSql());
                    sb.AppendFormat("OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", skipRows, ResultMax);
                    sb.AppendLine();

                    return sb.ToString();
                }
                else if (PagingMethod == ePagingMethod.CTE)
                {
                    //construct a common table expression around the query
                    if (SelectStatement.Distinct) throw new ArgumentException("DbSelectPager: DISTINCT not supported");
                    var origOrderClause = SelectStatement.OrderBy.GetSql();
                    SelectStatement.OrderBy.Clear();

                    var origFieldsClause = SelectStatement.Fields.GetSql();
                    SelectStatement.Fields.AppendRaw(string.Format(", ROW_NUMBER() OVER (ORDER BY {0}) AS __PAGING_ROWNUMBER__", origOrderClause));

                    var origWithCte = SelectStatement.WithCte;
                    SelectStatement.WithCte = null;


                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.AppendLine("WITH");

                    for (int cteIdx = 0; cteIdx < origWithCte._names.Count; cteIdx++)
                    {
                        sb.AppendLine(string.Format("{0} AS (", SelectStatement.QuoteIdentifier(origWithCte._names[cteIdx])));
                        sb.AppendLine(origWithCte._statements[cteIdx].GetSql());
                        sb.AppendLine("),");
                    }

                    sb.AppendLine("__PAGING_CTE__ AS (");

                    sb.AppendLine(SelectStatement.GetSql());

                    sb.AppendLine(") SELECT * FROM __PAGING_CTE__");
                    sb.AppendFormat("WHERE __PAGING_ROWNUMBER__ BETWEEN {0} AND {1}", (skipRows + 1), (skipRows + ResultMax));
                    sb.AppendLine();

                    //reset builder back
                    SelectStatement.Fields.Clear();
                    SelectStatement.Fields.AppendRaw(origFieldsClause);
                    SelectStatement.OrderBy.Clear();
                    SelectStatement.OrderBy.AppendRaw(origOrderClause);
                    SelectStatement.WithCte = origWithCte;

                    return sb.ToString();
                }
                else
                {
                    throw new NotImplementedException("DbSelectPager: Paging not supported");
                }
            }
        }

        /// <summary></summary>
        protected internal string GetRowCountSql()
        {
            //modify builder to select the count
            var origFields = SelectStatement.Fields.GetSql();
            SelectStatement.Fields.Clear();
            SelectStatement.Fields.AppendRaw("COUNT(*)");
            var origOrderClause = SelectStatement.OrderBy.GetSql();
            SelectStatement.OrderBy.Clear();

            var rowCountSql = SelectStatement.GetSql();

            //reset builder back
            SelectStatement.Fields.Clear();
            SelectStatement.Fields.AppendRaw(origFields);
            SelectStatement.OrderBy.Clear();
            SelectStatement.OrderBy.AppendRaw(origOrderClause);

            return rowCountSql;
        }

    }

}

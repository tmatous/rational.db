﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Rational.DB
{
    /// <summary></summary>
    public static class Utility
    {


        /// <summary>
        /// Utility function to translate a range of dates or date/times to a consistent format, including timezone. Result should be used in a *left-inclusive* range comparison (i.e. (dt GTE d1 AND dt LT d2))
        /// </summary>
        /// <param name="pDateFrom">The start date/time of the range</param>
        /// <param name="pDateTo">The start date/time of the range</param>
        /// <param name="pDateOnly">Interpret source date/times as date-only</param>
        /// <param name="pSourceTimeZone">If timezone conversion is required, source timezone. Defaults to UTC</param>
        /// <param name="pDestTimeZone">If timezone conversion is required, destination timezone. Defaults to UTC</param>
        /// <remarks></remarks>
        public static Tuple<DateTime?, DateTime?> TranslateDateRange(DateTime? pDateFrom, DateTime? pDateTo, bool pDateOnly = false, TimeZoneInfo pSourceTimeZone = null, TimeZoneInfo pDestTimeZone = null)
        {
            if (pSourceTimeZone == null)
                pSourceTimeZone = TimeZoneInfo.Utc;
            if (pDestTimeZone == null)
                pDestTimeZone = TimeZoneInfo.Utc;

            if (pDateFrom.HasValue && pDateTo.HasValue && (pDateFrom.Value > pDateTo.Value)) { var tmp = pDateFrom; pDateFrom = pDateTo; pDateTo = tmp; }

            if (pDateFrom.HasValue)
            {
                if (pDateOnly)
                    pDateFrom = pDateFrom.Value.Date;
                if (!pSourceTimeZone.Equals(pDestTimeZone))
                {
                    pDateFrom = TimeZoneInfo.ConvertTime(pDateFrom.Value, pSourceTimeZone, pDestTimeZone);
                }
            }
            if (pDateTo.HasValue)
            {
                if (pDateOnly)
                    pDateTo = pDateTo.Value.Date.AddDays(1);
                if (!pSourceTimeZone.Equals(pDestTimeZone))
                {
                    pDateTo = TimeZoneInfo.ConvertTime(pDateTo.Value, pSourceTimeZone, pDestTimeZone);
                }
            }

            return new Tuple<DateTime?, DateTime?>(pDateFrom, pDateTo);
        }


        /// <summary>
        /// Compares the columns and types defined in each datatable, returning errors on any differences
        /// </summary>
        public static string CompareSchema(DataTable pDtActual, DataTable pDtExpected)
        {
            System.Text.StringBuilder res = new System.Text.StringBuilder();

            //If (pDtActual.Columns.Count <> pDtExpected.Columns.Count) Then
            //    Return String.Format("Incorrect column count, found {0}, expected {1}", pDtActual.Columns.Count, pDtExpected.Columns.Count)
            //End If

            Dictionary<string, DataColumn> actCols = new Dictionary<string, DataColumn>();
            foreach (DataColumn col in pDtActual.Columns)
            {
                actCols.Add(col.ColumnName, col);
            }

            foreach (DataColumn colex in pDtExpected.Columns)
            {
                if (pDtActual.Columns.Contains(colex.ColumnName))
                {
                    DataColumn colact = pDtActual.Columns[colex.ColumnName];
                    actCols.Remove(colact.ColumnName);
                    if (colex.DataType.FullName != colact.DataType.FullName)
                    {
                        res.AppendLine(string.Format("Incorrect data type on column '{0}', found {1}, expected {2}", colex.ColumnName, colact.DataType.FullName, colex.DataType.FullName));
                    }
                }
                else
                {
                    res.AppendLine(string.Format("Missing column, expected '{0}'", colex.ColumnName));
                }
            }

            foreach (DataColumn col in actCols.Values)
            {
                res.AppendLine(string.Format("Extra column, '{0}'", col.ColumnName));
            }

            return res.ToString();
        }

        /// <summary>
        /// Remove any constraints (unique, readonly, etc) from a DataTable
        /// </summary>
        public static void RemoveDataTableConstraints(DataTable pioDt)
        {
            pioDt.Constraints.Clear();
            foreach (DataColumn col in pioDt.Columns)
            {
                if (col.DataType == typeof(string)) col.MaxLength = Int32.MaxValue;
                col.ReadOnly = false;
                col.Unique = false;
                col.AllowDBNull = true;
            }
        }


                /// <summary>
        /// Check if a type is one of the simple types (numbers, dates, strings, boolean)
        /// </summary>
        public static bool IsSimpleType(Type type)
        {
            return
                type.IsValueType ||
                type.IsPrimitive ||
                type.Equals(typeof(string)) ||
                Convert.GetTypeCode(type) != TypeCode.Object;
        }


        /// <summary>
        /// Get a property from an object, ignoring case
        /// </summary>
        public static System.Reflection.PropertyInfo GetPropertyInfoCaseInsensitive(Type type, string name)
        {
            var prop = type.GetProperty(name);
            if (prop == null)
            {
                prop = type.GetProperty(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.IgnoreCase);
            }

            return prop;
        }


        /// <summary>
        /// Get a field from an object, ignoring case
        /// </summary>
        public static System.Reflection.FieldInfo GetFieldInfoCaseInsensitive(Type type, string name)
        {
            var fld = type.GetField(name);
            if (fld == null)
            {
                fld = type.GetField(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.IgnoreCase);
            }

            return fld;
        }






        internal static void SetObjectProperty(object pObject, string pPropertyName, object pValue)
        {
            System.Reflection.PropertyInfo pinfo = pObject.GetType().GetProperty(pPropertyName);
            if ((pinfo != null))
            {
                if ((pinfo.PropertyType != pValue.GetType()))
                    throw new Exception(string.Format("SetObjectProperty: Type of Property '{0}' does not match value", pPropertyName));
                pinfo.SetValue(pObject, pValue, null);
            }
            else
            {
                //failsafe for member variables
                System.Reflection.FieldInfo finfo = pObject.GetType().GetField(pPropertyName);
                if ((finfo == null))
                    throw new Exception(string.Format("SetObjectProperty: Property or Field '{0}' not found", pPropertyName));
                if ((finfo.FieldType != pValue.GetType()))
                    throw new Exception(string.Format("SetObjectProperty: Type of Field '{0}' does not match value", pPropertyName));
                finfo.SetValue(pObject, pValue);
            }
        }

        internal static void SetProviderDbType(IDataParameter pParam, string pProviderDbType)
        {
            try
            {
                var ptyp = pParam.GetType();
                if (ptyp.FullName == "System.Data.SqlClient.SqlParameter")
                {
                    SetEnumProperty(pParam, "SqlDbType", pProviderDbType);
                }
                else if (ptyp.FullName == "Oracle.DataAccess.Client.OracleParameter")
                {
                    SetEnumProperty(pParam, "OracleDbType", pProviderDbType);
                }
                else if (ptyp.FullName == "System.Data.OracleClient.OracleParameter")
                {
                    SetEnumProperty(pParam, "OracleType", pProviderDbType);
                }
                else if (ptyp.FullName == "MySql.Data.MySqlClient.MySqlParameter")
                {
                    SetEnumProperty(pParam, "MySqlDbType", pProviderDbType);
                }
                else
                {
                    throw new NotImplementedException("Unknown parameter type");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to set ProviderDbType", ex);
            }
        }

        internal static void SetEnumProperty(object pTarget, string pPropName, string pPropValue)
        {
            var typ = pTarget.GetType();
            var prop = typ.GetProperty(pPropName);
            var propTyp = prop.PropertyType;
            var val = System.Enum.Parse(propTyp, pPropValue, ignoreCase: true);
            prop.SetValue(pTarget, val, null);
        }

        internal static void SetEnumProperty(object pTarget, string pPropName, int pPropValue)
        {
            var typ = pTarget.GetType();
            var prop = typ.GetProperty(pPropName);
            prop.SetValue(pTarget, pPropValue, null);
        }

        private static Dictionary<DbType, Type> _dbToClrTypeMap = null;
        internal static Type GetDefaultClrType(DbType pDbType)
        {
            if (_dbToClrTypeMap == null)
            {
                var typeMap = new Dictionary<DbType, Type>();
                typeMap[DbType.AnsiString] = typeof(string);
                typeMap[DbType.AnsiStringFixedLength] = typeof(string);
                typeMap[DbType.Binary] = typeof(byte[]);
                typeMap[DbType.Boolean] = typeof(bool);
                typeMap[DbType.Byte] = typeof(byte);
                typeMap[DbType.Currency] = typeof(decimal);
                typeMap[DbType.Date] = typeof(DateTime);
                typeMap[DbType.DateTime] = typeof(DateTime);
                typeMap[DbType.DateTime2] = typeof(DateTime);
                typeMap[DbType.DateTimeOffset] = typeof(DateTimeOffset);
                typeMap[DbType.Decimal] = typeof(decimal);
                typeMap[DbType.Double] = typeof(double);
                typeMap[DbType.Guid] = typeof(Guid);
                typeMap[DbType.Int16] = typeof(Int16);
                typeMap[DbType.Int32] = typeof(Int32);
                typeMap[DbType.Int64] = typeof(Int64);
                typeMap[DbType.Object] = typeof(object);
                typeMap[DbType.SByte] = typeof(byte);
                typeMap[DbType.Single] = typeof(float);
                typeMap[DbType.String] = typeof(string);
                typeMap[DbType.StringFixedLength] = typeof(string);
                typeMap[DbType.Time] = typeof(TimeSpan);
                typeMap[DbType.UInt16] = typeof(Int16);
                typeMap[DbType.UInt32] = typeof(Int32);
                typeMap[DbType.UInt64] = typeof(Int64);
                typeMap[DbType.VarNumeric] = typeof(double);
                typeMap[DbType.Xml] = typeof(string);
                _dbToClrTypeMap = typeMap;
            }

            if (_dbToClrTypeMap.ContainsKey(pDbType)) return _dbToClrTypeMap[pDbType];

            throw new NotImplementedException(string.Format("Unmapped type: {0}", pDbType));
        }

        private static Dictionary<Type, DbType> _clrToDbTypeMap = null;
        internal static DbType GetDefaultDbType(Type pClrType)
        {
            if (_clrToDbTypeMap == null)
            {
                var typeMap = new Dictionary<Type, DbType>();
                typeMap[typeof(byte)] = DbType.Byte;
                typeMap[typeof(sbyte)] = DbType.SByte;
                typeMap[typeof(short)] = DbType.Int16;
                typeMap[typeof(ushort)] = DbType.UInt16;
                typeMap[typeof(int)] = DbType.Int32;
                typeMap[typeof(uint)] = DbType.UInt32;
                typeMap[typeof(long)] = DbType.Int64;
                typeMap[typeof(ulong)] = DbType.UInt64;
                typeMap[typeof(float)] = DbType.Single;
                typeMap[typeof(double)] = DbType.Double;
                typeMap[typeof(decimal)] = DbType.Decimal;
                typeMap[typeof(bool)] = DbType.Boolean;
                typeMap[typeof(string)] = DbType.String;
                typeMap[typeof(char)] = DbType.StringFixedLength;
                typeMap[typeof(Guid)] = DbType.Guid;
                typeMap[typeof(DateTime)] = DbType.DateTime;
                typeMap[typeof(DateTimeOffset)] = DbType.DateTimeOffset;
                typeMap[typeof(byte[])] = DbType.Binary;
                typeMap[typeof(byte?)] = DbType.Byte;
                typeMap[typeof(sbyte?)] = DbType.SByte;
                typeMap[typeof(short?)] = DbType.Int16;
                typeMap[typeof(ushort?)] = DbType.UInt16;
                typeMap[typeof(int?)] = DbType.Int32;
                typeMap[typeof(uint?)] = DbType.UInt32;
                typeMap[typeof(long?)] = DbType.Int64;
                typeMap[typeof(ulong?)] = DbType.UInt64;
                typeMap[typeof(float?)] = DbType.Single;
                typeMap[typeof(double?)] = DbType.Double;
                typeMap[typeof(decimal?)] = DbType.Decimal;
                typeMap[typeof(bool?)] = DbType.Boolean;
                typeMap[typeof(char?)] = DbType.StringFixedLength;
                typeMap[typeof(Guid?)] = DbType.Guid;
                typeMap[typeof(DateTime?)] = DbType.DateTime;
                typeMap[typeof(DateTimeOffset?)] = DbType.DateTimeOffset;
                _clrToDbTypeMap = typeMap;
            }

            if (_clrToDbTypeMap.ContainsKey(pClrType)) return _clrToDbTypeMap[pClrType];

            throw new NotImplementedException(string.Format("Unmapped type: {0}", pClrType.Name));
        }



        internal static Type GetEnumerableType(object pTarget)
        {
            if (pTarget == null) return null;
            Type type = null;
            foreach (Type iType in pTarget.GetType().GetInterfaces())
            {
                if (iType.IsGenericType && (iType.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                {
                    type = iType.GetGenericArguments()[0];
                    break;
                }
            }

            return type;
        }


    }
}

﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 



using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Rational.DB
{

    /// <summary>
    /// Provides database access functions
    /// </summary>
    public class Database
    {

        /// <summary></summary>
        protected DbConnectionInfo _connInfo;


        /// <summary></summary>
        public Database(DbConnectionInfo pConnInfo)
        {
            _connInfo = pConnInfo;
            /*
            try
            {
                using (var cb = _connInfo.Factory.CreateCommandBuilder())
                {
                    QuotePrefix = cb.QuotePrefix;
                    QuoteSuffix = cb.QuoteSuffix;
                    cb.Dispose();
                }
            }
            catch (Exception)
            {
                //do nothing
            }
            */
            if (string.IsNullOrEmpty(QuotePrefix))
            {
                switch (_connInfo.DbType)
                {
                    case eDbType.SqlServer:
                        QuotePrefix = "[";
                        QuoteSuffix = "]";
                        ParameterPrefix = "@";
                        break;
                    case eDbType.SqlServerCe:
                        QuotePrefix = "[";
                        QuoteSuffix = "]";
                        ParameterPrefix = "@";
                        break;
                    case eDbType.Oracle:
                        QuotePrefix = "\"";
                        QuoteSuffix = "\"";
                        ParameterPrefix = ":";
                        break;
                    case eDbType.MySql:
                        QuotePrefix = "`";
                        QuoteSuffix = "`";
                        ParameterPrefix = "@";
                        break;
                    default:
                        QuotePrefix = "\"";
                        QuoteSuffix = "\"";
                        ParameterPrefix = ":";
                        break;
                }
            }
        }

        /// <summary>Prefix for escaping SQL identifiers</summary>
        public string QuotePrefix { get; set; }

        /// <summary>Suffix for escaping SQL identifiers</summary>
        public string QuoteSuffix { get; set; }

        /// <summary>Prefix for a named parameter</summary>
        public string ParameterPrefix { get; set; }


        #region "OnValidationWarning"

        /// <summary>Event that fires when a validation warning is detected</summary>
        public event ValidationWarningEventHandler OnValidationWarning;

        /// <summary>Delegate for ValidationWarning event</summary>
        public delegate void ValidationWarningEventHandler(System.Object sender, ValidationWarningEventArgs e);

        /// <summary></summary>
        public class ValidationWarningEventArgs : System.EventArgs
        {
            /// <summary>Type of the validation warning</summary>
            public eWarningType WarningType;

            /// <summary>Summary of the warning</summary>
            public string Summary;


            /// <summary>Type of the validation warning</summary>
            public enum eWarningType
            {
                /// <summary>Unknown or unassigned status</summary>
                Unknown = 0,

                /// <summary>DB has multiple fields of the same name</summary>
                DuplicateDbField = 10,

                /// <summary>CLR has multiple fields of the same name (could be differing case)</summary>
                DuplicateClrField = 20,

                /// <summary>DB field and CLR field are different types</summary>
                DifferentDataType = 30,

                /// <summary>Field exists in CLR but not DB</summary>
                MissingDbField = 40,

                /// <summary>Field exists in DB but not CLR</summary>
                MissingClrField = 50
            }
        }

        #endregion


        #region "OnBeforeExecute"

        /// <summary>Event that fires before a statement is executed</summary>
        public event BeforeExecuteEventHandler OnBeforeExecute;

        /// <summary>Delegate for BeforeExecute event</summary>
        public delegate void BeforeExecuteEventHandler(System.Object sender, BeforeExecuteEventArgs e);

        /// <summary></summary>
        public class BeforeExecuteEventArgs : System.EventArgs
        {
            /// <summary></summary>
            public string Sql;

            /// <summary></summary>
            public IDictionary<string, object> Parameters;

            /// <summary></summary>
            public string ParameterSummary;
        }

        #endregion


        #region "OnExecuteComplete"

        /// <summary>Event that fires after a statement has finished executing</summary>
        public event ExecuteCompleteEventHandler OnExecuteComplete;

        /// <summary></summary>
        public delegate void ExecuteCompleteEventHandler(System.Object sender, ExecuteCompleteEventArgs e);

        /// <summary></summary>
        public class ExecuteCompleteEventArgs : System.EventArgs
        {
            /// <summary></summary>
            public string Sql;

            /// <summary></summary>
            public IDictionary<string, object> Parameters;

            /// <summary></summary>
            public string ParameterSummary;

            /// <summary></summary>
            public DateTime StartTime;

            /// <summary></summary>
            public Int32 DurationMs;

            /// <summary></summary>
            public eStatus Status;

            /// <summary></summary>
            public Exception Exception;

            /// <summary>Status of the SQL execution</summary>
            public enum eStatus
            {
                /// <summary>Unknown or unassigned status</summary>
                Unknown = 0,

                /// <summary>Execution generated an error</summary>
                Error = 10,

                /// <summary>Execution was successful</summary>
                Success = 20
            }

        }

        /// <summary>Wrapper class to track all data for handling onExecuteComplete event</summary>
        protected class DbEventWrapper : IDisposable
        {

            /// <summary></summary>
            protected Database _parent;
            /// <summary></summary>
            protected IDbCommand _cmd;
            /// <summary></summary>
            protected System.Diagnostics.Stopwatch _timer;
            /// <summary></summary>
            protected ExecuteCompleteEventArgs.eStatus _completeStatus;
            /// <summary></summary>
            protected Exception _completeException;

            /// <summary>Constructor, initiates timer</summary>
            public DbEventWrapper(Database pParent, IDbCommand pCmd)
            {
                _parent = pParent;
                _cmd = pCmd;
                _timer = System.Diagnostics.Stopwatch.StartNew();
                _completeStatus = ExecuteCompleteEventArgs.eStatus.Unknown;
                _completeException = null;
            }

            /// <summary></summary>
            public void Success()
            {
                _completeStatus = ExecuteCompleteEventArgs.eStatus.Success;
                _completeException = null;
            }

            /// <summary></summary>
            public void Fail(Exception pEx)
            {
                _completeStatus = ExecuteCompleteEventArgs.eStatus.Error;
                _completeException = pEx;
            }

            /// <summary></summary>
            protected void Close()
            {
                _timer.Stop();
                if (_parent.OnExecuteComplete != null)
                {
                    //if any subscribers
                    var evt = new ExecuteCompleteEventArgs();
                    evt.Status = _completeStatus;
                    evt.Exception = _completeException;
                    evt.Sql = _cmd.CommandText;
                    _parent.GetParameterSummary(_cmd, out evt.ParameterSummary, out evt.Parameters);
                    evt.DurationMs = Convert.ToInt32(_timer.ElapsedMilliseconds);
                    evt.StartTime = DateTime.UtcNow.Subtract(_timer.Elapsed);
                    _parent.OnExecuteComplete.Invoke(_parent, evt);
                }
                _cmd = null;
                _parent = null;
                _timer = null;
            }

            #region "Dispose"

            bool disposed = false;

            /// <summary></summary>
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary></summary>
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                if (disposing)
                {
                    Close();
                }

                disposed = true;
            }

            #endregion

        }

        #endregion


        #region "Utility"

        /// <summary>
        /// Configures a native SQL command object with the SQL and parameters stored in the statement
        /// </summary>
        protected void ConfigureCommand(IDbCommand pCmd, IDbStatement pStmt)
        {
            pCmd.CommandTimeout = (pStmt.TimeoutSecs * 1000);
            pCmd.CommandType = pStmt.IsStoredProcedure ? CommandType.StoredProcedure : CommandType.Text;
            pCmd.CommandText = pStmt.GetSql();
            AddCommandParameters(pCmd, pStmt);

            //call BeforeExecute event
            if (OnBeforeExecute != null)
            {
                //if any subscribers
                var evt = new BeforeExecuteEventArgs();
                evt.Sql = pCmd.CommandText;
                GetParameterSummary(pCmd, out evt.ParameterSummary, out evt.Parameters);
                OnBeforeExecute.Invoke(this, evt);
            }
        }

        /// <summary>
        /// Configures a native SQL command object with the SQL and parameters stored in the statement
        /// </summary>
        protected void ConfigureCommand(IDbCommand pCmd, DbSelectPager pPager)
        {
            pCmd.CommandTimeout = (pPager.SelectStatement.TimeoutSecs * 1000);
            pCmd.CommandType = CommandType.Text;
            pCmd.CommandText = pPager.GetSql();
            AddCommandParameters(pCmd, pPager.SelectStatement);

            //call BeforeExecute event
            if (OnBeforeExecute != null)
            {
                //if any subscribers
                var evt = new BeforeExecuteEventArgs();
                evt.Sql = pCmd.CommandText;
                GetParameterSummary(pCmd, out evt.ParameterSummary, out evt.Parameters);
                OnBeforeExecute.Invoke(this, evt);
            }
        }

        /// <summary>
        /// Configures a native SQL command object with the SQL and parameters stored in the statement
        /// </summary>
        protected void ConfigureRowCountCommand(IDbCommand pCmd, DbSelectPager pPager)
        {
            pCmd.CommandTimeout = (pPager.SelectStatement.TimeoutSecs * 1000);
            pCmd.CommandType = CommandType.Text;
            pCmd.CommandText = pPager.GetRowCountSql();
            AddCommandParameters(pCmd, pPager.SelectStatement);
        }

        /// <summary></summary>
        protected void AddCommandParameters(IDbCommand pCmd, IDbStatement pStmt)
        {
            pCmd.Parameters.Clear();
            if ((pStmt.Parameters != null) && (pStmt.Parameters.Count > 0))
            {
                foreach (DbParameter curP in pStmt.Parameters)
                {
                    if (curP.SourceProviderParameter == null)
                    {
                        curP.ProviderParameter = pCmd.CreateParameter();
                        curP.ProviderParameter.ParameterName = curP.ParameterName;
                        curP.ProviderParameter.Value = curP.Value ?? DBNull.Value;
                        curP.ProviderParameter.Direction = curP.Direction;
                        curP.ProviderParameter.Size = curP.Size;
                        curP.ProviderParameter.Precision = curP.Precision;
                        curP.ProviderParameter.Scale = curP.Scale;
                        if (curP.DbTypeSpecified)
                        {
                            curP.ProviderParameter.DbType = curP.DbType;
                        }
                        else if (curP.ProviderDbType != null)
                        {
                            Utility.SetProviderDbType(curP.ProviderParameter, curP.ProviderDbType);
                        }
                        else if (curP.ClrType != null)
                        {
                            curP.ProviderParameter.DbType = GetDbType(curP.ClrType);
                        }
                        else if (!object.ReferenceEquals(curP.ProviderParameter.Value, DBNull.Value))
                        {
                            curP.ProviderParameter.DbType = GetDbType(curP.ProviderParameter.Value.GetType());
                        }
                    }
                    else
                    {
                        if (curP.ProviderParameter != null) throw new Exception("A DbParameter instantiated with a provider parameter can only be used once");
                        curP.ProviderParameter = curP.SourceProviderParameter;
                        curP.ProviderParameter.ParameterName = curP.ParameterName;
                    }
                    if (this.ParameterPrefix != DbStatementBase.DefaultParameterPrefix)
                    {
                        //convert to db syntax
                        var nameOnly = curP.ProviderParameter.ParameterName.Replace(DbStatementBase.DefaultParameterPrefix, "").Replace(ParameterPrefix, "");
                        var defaultPlaceholder = string.Format("{0}{1}", DbStatementBase.DefaultParameterPrefix, nameOnly);
                        var newPlaceholder = string.Format("{0}{1}", ParameterPrefix, nameOnly);
                        curP.ProviderParameter.ParameterName = nameOnly;
                        pCmd.CommandText = pCmd.CommandText.Replace(defaultPlaceholder, newPlaceholder);
                    }
                    pCmd.Parameters.Add(curP.ProviderParameter);
                }
            }
        }

        /// <summary></summary>
        protected IDbConnection CreateConnection()
        {
            var conn = _connInfo.GetFactory().CreateConnection();
            conn.ConnectionString = _connInfo.ConnectionString;
            return conn;
        }

        /// <summary></summary>
        protected IDbDataAdapter CreateDataAdapter(IDbCommand pCmd)
        {
            IDbDataAdapter da = _connInfo.GetFactory().CreateDataAdapter();
            da.SelectCommand = pCmd;
            return da;
        }

        /// <summary>
        /// Wraps all disposable objects as necessary to run a command
        /// </summary>
        /// <param name="pBehavior"></param>
        protected void HandleCommand(Action<IDbCommand> pBehavior)
        {
            if (Transaction != null)
            {
                using (IDbCommand cmd = Transaction.CreateCommand())
                {
                    using (var wrapper = new DbEventWrapper(this, cmd))
                    {
                        try
                        {
                            pBehavior(cmd);
                            wrapper.Success();
                        }
                        catch (Exception ex)
                        {
                            wrapper.Fail(ex);
                            throw;
                        }
                    }
                }
            }
            else if (_persistentConnection != null)
            {
                using (IDbCommand cmd = _persistentConnection.CreateCommand())
                {
                    using (var wrapper = new DbEventWrapper(this, cmd))
                    {
                        try
                        {
                            pBehavior(cmd);
                            wrapper.Success();
                        }
                        catch (Exception ex)
                        {
                            wrapper.Fail(ex);
                            throw;
                        }
                    }
                }
            }
            else
            {
                using (IDbConnection conn = CreateConnection())
                {
                    conn.Open();
                    using (IDbCommand cmd = conn.CreateCommand())
                    {
                        using (var wrapper = new DbEventWrapper(this, cmd))
                        {
                            try
                            {
                                pBehavior(cmd);
                                wrapper.Success();
                            }
                            catch (Exception ex)
                            {
                                wrapper.Fail(ex);
                                throw;
                            }
                        }
                    }
                    conn.Close();
                }
            }
        }

        #endregion


        #region "Mapping"

        /// <summary>Mapping functions by DbType</summary>
        protected Dictionary<DbType, Func<object, object>> _dbToClrMapByDbType = null;

        /// <summary>Mapping functions by Type</summary>
        protected Dictionary<Type, Func<object, object>> _dbToClrMapByType = null;

        /// <summary></summary>
        protected internal object MapDbToClr(object pDbData, DbType pDbType, Type pType)
        {
            object result = pDbData;
            if ((_dbToClrMapByDbType != null) && _dbToClrMapByDbType.ContainsKey(pDbType))
                result = _dbToClrMapByDbType[pDbType].Invoke(pDbData);
            else if ((_dbToClrMapByType != null) && _dbToClrMapByType.ContainsKey(pType))
                result = _dbToClrMapByType[pType].Invoke(pDbData);

            //if mapping function returns DBNull, convert back to null
            if (result == DBNull.Value) result = null;
            return result;
        }

        /// <summary></summary>
        protected internal bool HasCustomDbToClrMappings()
        {
            return ((_dbToClrMapByDbType != null) || (_dbToClrMapByType != null));
        }


        /// <summary>Change the mapping of DB value (by DbType) into a CLR field</summary>
        public void MapDbToClrType(DbType pDbType, Func<object, object> pTypeMapFunc)
        {
            if (_dbToClrMapByDbType == null) _dbToClrMapByDbType = new Dictionary<DbType, Func<object, object>>();
            _dbToClrMapByDbType[pDbType] = pTypeMapFunc;
        }

        /// <summary>Change the mapping of DB value (by System.Type) into a CLR field</summary>
        public void MapDbToClrType(Type pDbType, Func<object, object> pTypeMapFunc)
        {
            if (_dbToClrMapByType == null) _dbToClrMapByType = new Dictionary<Type, Func<object, object>>();
            _dbToClrMapByType[pDbType] = pTypeMapFunc;
        }

        /// <summary></summary>
        protected internal DbType ConvertProviderDbType(int pProviderDbType)
        {
            //setting the provider type on a parameter causes DbType to be set accordingly
            System.Data.Common.DbParameter prm = null;
            switch (_connInfo.DbType)
            {
                case eDbType.Unknown:
                    return DbType.Object;
                case eDbType.SqlServer:
                    prm = _connInfo.GetFactory().CreateParameter();
                    Utility.SetEnumProperty(prm, "SqlDbType", pProviderDbType);
                    return prm.DbType;
                case eDbType.SqlServerCe:
                    return DbType.Object;
                case eDbType.Oracle:
                    return DbType.Object;
                case eDbType.MySql:
                    prm = _connInfo.GetFactory().CreateParameter();
                    Utility.SetEnumProperty(prm, "MySqlDbType", pProviderDbType);
                    return prm.DbType;
                default:
                    return DbType.Object;
            }
        }



        /// <summary></summary>
        protected Dictionary<Type, DbType> _clrToDbTypeMap = null;
        /// <summary></summary>
        protected DbType GetDbType(Type pClrType)
        {
            if ((_clrToDbTypeMap != null) && _clrToDbTypeMap.ContainsKey(pClrType))
                return _clrToDbTypeMap[pClrType];
            else
                return Utility.GetDefaultDbType(pClrType);
        }


        /// <summary>Change the mapping of a CLR type to a DbType, for parameters</summary>
        public void MapClrToDbType(Type pClrType, DbType pDbType)
        {
            if (_clrToDbTypeMap == null) _clrToDbTypeMap = new Dictionary<Type, DbType>();
            _clrToDbTypeMap[pClrType] = pDbType;
        }

        #endregion


        #region "Transaction"

        /// <summary>Transaction object, if database is currently executing within a transaction</summary>
        public DbTransaction Transaction { get; protected set; }

        /// <summary>
        /// Begins a new transaction for this Database object. All database activity while this transaction is active will be in the transaction. Transaction handle must be disposed.
        /// </summary>
        /// <returns></returns>
        public DbTransaction BeginTransaction()
        {
            lock (this)
            {
                if (Transaction != null) throw new Exception("Duplicate transaction");

                Transaction = new DbTransaction(_connInfo);
                Transaction.OnClose += _transaction_OnClose;
                return Transaction;
            }
        }

        /// <summary>Join this database object to another existing transaction</summary>
        public void JoinTransaction(DbTransaction pTransaction)
        {
            lock (this)
            {
                if (pTransaction == null) throw new Exception("Missing transaction");
                if (pTransaction == Transaction) return; //already in specified transaction
                if (Transaction != null) throw new Exception("Duplicate transaction");
                if (pTransaction._connInfo.ConnectionString != _connInfo.ConnectionString) throw new Exception("Transaction must be in the same database");

                Transaction = pTransaction;
                Transaction.OnClose += _transaction_OnClose;
            }
        }


        /// <summary></summary>
        protected void _transaction_OnClose(object sender, EventArgs e)
        {
            if (sender == Transaction)
            {
                Transaction.OnClose -= _transaction_OnClose;
                Transaction = null;
            }
        }

        #endregion


        #region "PersistentConnection"

        /// <summary></summary>
        protected DbConnectionHandle _persistentConnection = null;

        /// <summary>
        /// Establish a persistent connection for this Database. All database activity while this connection is active will be on the connection. Connection handle must be disposed.
        /// </summary>
        /// <returns></returns>
        public DbConnectionHandle PersistentConnection()
        {
            if (_persistentConnection != null) throw new Exception("Already in a persistent connection");
            _persistentConnection = new DbConnectionHandle(_connInfo);
            _persistentConnection.OnClose += _persistentConnection_OnClose;
            return _persistentConnection;
        }

        /// <summary></summary>
        protected void _persistentConnection_OnClose(object sender, EventArgs e)
        {
            if (sender == _persistentConnection)
            {
                _persistentConnection.OnClose -= _persistentConnection_OnClose;
                _persistentConnection = null;
            }
        }

        #endregion


        #region "CRUD"

        /// <summary>
        /// Create a new statement of the specified type with properties appropriate for this database
        /// </summary>
        /// <returns></returns>
        public TStatement CreateStatement<TStatement>() where TStatement : IDbStatement, new()
        {
            var stmt = new TStatement();
            stmt.DbType = this._connInfo.DbType;
            stmt.QuotePrefix = this.QuotePrefix;
            stmt.QuoteSuffix = this.QuoteSuffix;
            stmt.ParameterPrefix = this.ParameterPrefix;
            return stmt;
        }

        /// <summary>
        /// Create a new DbStatement for this database
        /// </summary>
        /// <returns></returns>
        public DbStatement CreateStatement()
        {
            return CreateStatement<DbStatement>();
        }

        /// <summary>
        /// Create a new DbStatement for this database with the specified SQL
        /// </summary>
        /// <returns></returns>
        public DbStatement CreateStatement(string pSql)
        {
            var stmt = CreateStatement<DbStatement>();
            stmt.Statement.AppendRaw(pSql);
            return stmt;
        }

        /// <summary>
        /// Create a new DbStatement for this database with the specified SQL and parameter data
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParams">Parameter(s) to associate with the SQL. For numbered parameters, pass a scalar value or a list of scalars. For named parameters, pass an object with a property for each value.</param>
        public DbStatement CreateStatement(string pSql, object pParams)
        {
            var stmt = CreateStatement<DbStatement>();
            stmt.Statement.Append(pSql, pParams);
            return stmt;
        }

        /// <summary>
        /// Create a new DbStatement for this database with the specified SQL and parameter data
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public DbStatement CreateStatement(string pSql, object pParam0, object pParam1)
        {
            var stmt = CreateStatement<DbStatement>();
            stmt.Statement.Append(pSql, pParam0, pParam1);
            return stmt;
        }

        /// <summary>
        /// Create a new DbStatement for this database with the specified SQL and parameter data
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public DbStatement CreateStatement(string pSql, object pParam0, object pParam1, object pParam2)
        {
            var stmt = CreateStatement<DbStatement>();
            stmt.Statement.Append(pSql, pParam0, pParam1, pParam2);
            return stmt;
        }

        /// <summary>
        /// Create a new DbStatement for this database with the specified SQL and parameter data
        /// </summary>
        /// <param name="pSql">SQL string to append</param>
        /// <param name="pParam0">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam1">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam2">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        /// <param name="pParam3">Numbered parameter to associate with the SQL, scalar value or a list of scalars.</param>
        public DbStatement CreateStatement(string pSql, object pParam0, object pParam1, object pParam2, object pParam3)
        {
            var stmt = CreateStatement<DbStatement>();
            stmt.Statement.Append(pSql, pParam0, pParam1, pParam2, pParam3);
            return stmt;
        }

        /// <summary>
        /// Create a new DbProcedureCall for this database with the specified stored proc name
        /// </summary>
        /// <returns></returns>
        public DbProcedureCall CreateProcedureCall(string pProcName)
        {
            var stmt = CreateStatement<DbProcedureCall>();
            stmt.SpName = pProcName;
            return stmt;
        }

        /// <summary>
        /// Create a new SELECT statement with properties appropriate for this database
        /// </summary>
        /// <returns></returns>
        public DbSelectStatement CreateSelectStatement()
        {
            return CreateStatement<DbSelectStatement>();
        }

        /// <summary>
        /// Create a new SELECT statement with properties appropriate for this database
        /// </summary>
        /// <returns></returns>
        public DbSelectStatement CreateSelectStatement(string pFrom, string pFields)
        {
            var stmt = CreateStatement<DbSelectStatement>();
            stmt.From.Append(pFrom);
            stmt.Fields.Append(pFields);
            return stmt;
        }

        /// <summary>
        /// Creates a SELECT statement for the table specified by the enum. Includes FROM and fields clauses.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public DbSelectStatement CreateSelectStatement<TEnum>() where TEnum : struct
        {
            var stmt = CreateSelectStatement();
            stmt.From.Append(stmt.TableIdentifier<TEnum>());
            stmt.Fields.Add<TEnum>();
            return stmt;
        }

        /// <summary>
        /// Creates an INSERT statement of the specified object for the table specified by the enum.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="pObjectToInsert"></param>
        /// <returns></returns>
        public DbStatement CreateInsertStatement<TEnum>(object pObjectToInsert) where TEnum : struct
        {
            var stmt = CreateStatement<DbStatement>();
            stmt.Statement.Append(string.Format("INSERT INTO {0}", stmt.TableIdentifier<TEnum>()));
            var allFields = TableDefinitionAttributes.GetColumnAttributes<TEnum>().Values;
            var sbFields = new System.Text.StringBuilder();
            var sbValues = new System.Text.StringBuilder();
            var delim = "";
            foreach (var fld in allFields)
            {
                if (fld.Computed) continue;
                sbFields.AppendFormat("{0}{1}", delim, stmt.QuoteIdentifier(fld.SqlName));
                sbValues.AppendFormat("{0}@{1}", delim, fld.ClrName);
                delim = ", ";
                var fldProp = pObjectToInsert.GetType().GetProperty(fld.ClrName);
                if (fldProp == null) throw new ArgumentException(string.Format("Could not find field '{0}'", fld.ClrName));
                var fldData = fldProp.GetValue(pObjectToInsert, null);
                if (fld.NotNull && (fldData == null) && (fldProp.PropertyType == typeof(string)))
                {
                    //bit of a hack, but makes working with NN strings easier
                    fldData = string.Empty;
                }
                if (fld.SqlTypeEnum.HasValue)
                    stmt.Parameters.Add(string.Format("@{0}", fld.ClrName), fldData, fld.SqlTypeEnum.Value);
                else
                    stmt.Parameters.Add(string.Format("@{0}", fld.ClrName), fldData, fldProp.PropertyType);
            }
            stmt.Statement.Append(string.Format("( {0} ) VALUES ( {1} )", sbFields, sbValues));

            return stmt;
        }

        /// <summary>
        /// Creates an UPDATE statement of the specified object for the table specified by the enum. Requires a primary key.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="pObjectToUpdate"></param>
        /// <returns></returns>
        public DbConditionalStatement CreateUpdateStatement<TEnum>(object pObjectToUpdate) where TEnum : struct
        {
            var stmt = CreateStatement<DbConditionalStatement>();
            stmt.Action.Append(string.Format("UPDATE {0} SET", stmt.TableIdentifier<TEnum>()));
            var allFields = TableDefinitionAttributes.GetColumnAttributes<TEnum>().Values;
            var foundKey = false;
            var delim = "";
            foreach (var fld in allFields)
            {
                if (fld.PrimaryKey)
                {
                    stmt.Where.And(string.Format("{0} = @{1}", stmt.QuoteIdentifier(fld.SqlName), fld.ClrName), pObjectToUpdate);
                    foundKey = true;
                }
                else if (fld.Computed)
                {
                    continue;
                }
                else
                {
                    stmt.Action.Append(string.Format("{0}{1} = @{2}", delim, stmt.QuoteIdentifier(fld.SqlName), fld.ClrName));
                    delim = ", ";
                    var fldProp = pObjectToUpdate.GetType().GetProperty(fld.ClrName);
                    if (fldProp == null) throw new ArgumentException(string.Format("Could not find field '{0}'", fld.ClrName));
                    var fldData = fldProp.GetValue(pObjectToUpdate, null);
                    if (fld.NotNull && (fldData == null) && (fldProp.PropertyType == typeof(string)))
                    {
                        //bit of a hack, but makes working with NN strings easier
                        fldData = string.Empty;
                    }
                    if (fld.SqlTypeEnum.HasValue)
                        stmt.Parameters.Add(string.Format("@{0}", fld.ClrName), fldData, fld.SqlTypeEnum.Value);
                    else
                        stmt.Parameters.Add(string.Format("@{0}", fld.ClrName), fldData, fldProp.PropertyType);
                }
            }
            if (!foundKey) throw new Exception("CreateUpdateStatement: Unable to determine key fields");

            return stmt;
        }

        /// <summary>
        /// Creates a DELETE statement of the specified object for the table specified by the enum. Requires a primary key.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="pObjectToDelete"></param>
        /// <returns></returns>
        public DbConditionalStatement CreateDeleteStatement<TEnum>(object pObjectToDelete) where TEnum : struct
        {
            var stmt = CreateStatement<DbConditionalStatement>();
            stmt.Action.Append(string.Format("DELETE FROM {0}", stmt.TableIdentifier<TEnum>()));
            var keyFields = (from f in TableDefinitionAttributes.GetColumnAttributes<TEnum>().Values where f.PrimaryKey select f).ToList();
            if (keyFields.Count == 0) throw new Exception("CreateDeleteStatement: Unable to determine key fields");
            foreach (var fld in keyFields)
            {
                stmt.Where.And(string.Format("{0} = @{1}", stmt.QuoteIdentifier(fld.SqlName), fld.ClrName), pObjectToDelete);
            }

            return stmt;
        }

        /// <summary>
        /// Select a single object by key from the table specified by the enum.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="pKeyData">Key value to select. For composite keys, pass an object with properties for the different columns.</param>
        /// <returns></returns>
        public T SelectByKey<TEnum, T>(object pKeyData)
            where TEnum : struct
            where T : new()
        {
            if (pKeyData == null) throw new Exception("SelectByKey: Key field can not be null");
            var stmt = CreateSelectStatement<TEnum>();
            var keyFields = (from f in TableDefinitionAttributes.GetColumnAttributes<TEnum>().Values where f.PrimaryKey select f).ToList();
            if (keyFields.Count == 0) throw new Exception("SelectByKey: Unable to determine key fields");
            if (Utility.IsSimpleType(pKeyData.GetType()))
            {
                if (keyFields.Count != 1) throw new Exception("SelectByKey: Only one key field specified");
                stmt.Where.Append(string.Format("{0} = @0", stmt.QuoteIdentifier(keyFields[0].SqlName)), pKeyData);
            }
            else
            {
                foreach (var fld in keyFields)
                {
                    stmt.Where.And(string.Format("{0} = @{1}", stmt.QuoteIdentifier(fld.SqlName), fld.ClrName), pKeyData);
                }
            }

            return SelectObject<T>(stmt);
        }

        /// <summary>
        /// Inserts the specified object into the table specified by the enum.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="pObjectToInsert"></param>
        public void Insert<TEnum>(object pObjectToInsert) where TEnum : struct
        {
            var stmt = CreateInsertStatement<TEnum>(pObjectToInsert);
            var autoIncKey = (from c in TableDefinitionAttributes.GetColumnAttributes<TEnum>().Values where c.PrimaryKey && c.AutoIncrement select c).FirstOrDefault();
            if (autoIncKey != null)
            {
                int newId = 0;
                switch (_connInfo.DbType)
                {
                    case eDbType.SqlServer:
                        stmt.Statement.Append("; SELECT CAST(SCOPE_IDENTITY() AS int);");
                        newId = SelectScalar<Int32>(stmt);
                        break;
                    case eDbType.SqlServerCe:
                        var selStmt = CreateStatement<DbStatement>();
                        selStmt.Statement.Append("SELECT CAST(@@IDENTITY AS int);");
                        newId = (Int32)ExecuteAndSelectScalar(stmt, selStmt);
                        break;
                    case eDbType.MySql:
                        stmt.Statement.Append("; SELECT CAST(@@IDENTITY AS unsigned integer);");
                        newId = SelectScalar<Int32>(stmt);
                        break;
                    default:
                        //TODO: others?
                        throw new NotImplementedException();
                }
                Utility.SetObjectProperty(pObjectToInsert, autoIncKey.ClrName, newId);
            }
            else
            {
                var rowsAffected = Execute(stmt);
                if (rowsAffected != 1) throw new Exception(string.Format("Database.Insert: Unexpected rows affected ({0})", rowsAffected));
            }
        }

        /// <summary>
        /// Updates the specified object in the table specified by the enum. Requires a primary key.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="pObjectToUpdate"></param>
        public void Update<TEnum>(object pObjectToUpdate) where TEnum : struct
        {
            var stmt = CreateUpdateStatement<TEnum>(pObjectToUpdate);
            var rowsAffected = Execute(stmt);
            if (rowsAffected != 1) throw new Exception(string.Format("Database.Update: Unexpected rows affected ({0})", rowsAffected));
        }

        /// <summary>
        /// Deletes the specified object from the table specified by the enum. Requires a primary key.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="pObjectToDelete"></param>
        public void Delete<TEnum>(object pObjectToDelete) where TEnum : struct
        {
            var stmt = CreateDeleteStatement<TEnum>(pObjectToDelete);
            var rowsAffected = Execute(stmt);
            if (rowsAffected != 1) throw new Exception(string.Format("Database.Delete: Unexpected rows affected ({0})", rowsAffected));
        }

        #endregion




        #region "DataTable"

        /// <summary>
        /// Runs the statement, returning the result as a datatable
        /// </summary>
        public DataTable SelectDataTable(IDbStatement pStmt)
        {
            DataTable result = new DataTable();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    dbRes.LoadDataTable(result);
                }
            });
            return result;
        }

        /// <summary>
        /// Runs the pager, returning the result as a datatable. The pager specifies the SQL statement and page information for paged results.
        /// </summary>
        public DataTable SelectDataTable(DbSelectPager pPager)
        {
            DataTable result = new DataTable();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pPager);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    dbRes.LoadDataTable(result);
                }
                if (pPager.CalculateTotals)
                {
                    using (IDbCommand rowCountCmd = cmd.Connection.CreateCommand())
                    {
                        ConfigureRowCountCommand(rowCountCmd, pPager);
                        pPager.TotalRows = (int)rowCountCmd.ExecuteScalar();
                        pPager.TotalPages = Convert.ToInt32(Math.Ceiling(pPager.TotalRows.Value * 1.0 / pPager.ResultMax));
                    }
                }
            });
            return result;
        }

        /// <summary>
        /// Fills the datatable with results from execution of the statement.
        /// Used for strongly typed datatables.
        /// </summary>
        public void SelectIntoDataTable(IDbStatement pStmt, DataTable poDt)
        {
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    if (OnValidationWarning != null) ValidateDataTableMapping(dbRes, poDt);
                    dbRes.LoadDataTable(poDt);
                }
            });
        }

        #endregion



        #region "DataSet"

        /// <summary>
        /// Runs the statement, returning the result as a dataset
        /// </summary>
        public DataSet SelectDataSet(IDbStatement pStmt)
        {
            DataSet result = new DataSet();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader()))
                {
                    dbRes.LoadDataSet(result);
                }
            });
            return result;
        }

        #endregion



        #region "Object"

        /// <summary>
        /// Execute the statement, returning a single result row as a strongly typed object.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pStmt"></param>
        /// <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public T SelectObject<T>(IDbStatement pStmt, Func<DbRow, T> pCopyFunc)
        {
            T result = default(T);
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleRow)))
                {
                    if (OnValidationWarning != null) ValidateObjectMapping(dbRes, typeof(T));
                    if (dbRes.Read())
                    {
                        result = pCopyFunc.Invoke(dbRes);
                        if (dbRes.Read()) throw new Exception("Database.SelectObject: Multiple results returned");
                    }
                }
            });
            return result;
        }

        /// <summary>
        /// Execute the statement, returning a single result row as a strongly typed object. Copies values using reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pStmt"></param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public T SelectObject<T>(IDbStatement pStmt) where T : new()
        {
            var copier = new DbRowToObject<T>();
            return SelectObject<T>(pStmt, copier.CreateObject);
        }

        /// <summary>
        /// Execute the statement, returning a single result row as a strongly typed object. Copies values using the table enum TEnum, along with reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <typeparam name="TEnum">Table enum for translation</typeparam>
        /// <param name="pStmt"></param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public T SelectObject<T, TEnum>(IDbStatement pStmt)
            where T : new()
            where TEnum : struct
        {
            var copier = new DbRowToObject<T>();
            copier.SetDbTranslation<TEnum>();
            return SelectObject<T>(pStmt, copier.CreateObject);
        }

        #endregion



        #region "ObjectList"

        /// <summary>
        /// Execute the statement, returning the results as a list of strongly typed objects.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pStmt"></param>
        /// <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public IList<T> SelectObjectList<T>(IDbStatement pStmt, Func<DbRow, T> pCopyFunc)
        {
            List<T> result = new List<T>();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    if (OnValidationWarning != null) ValidateObjectMapping(dbRes, typeof(T));
                    while (dbRes.Read())
                    {
                        result.Add(pCopyFunc.Invoke(dbRes));
                    }
                }
            });
            return result;
        }

        /// <summary>
        /// Execute the pager, returning the results as a list of strongly typed objects.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pPager">Pager to execute. Includes statement and page information for paged results.</param>
        /// <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public IList<T> SelectObjectList<T>(DbSelectPager pPager, Func<DbRow, T> pCopyFunc)
        {
            List<T> result = new List<T>();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pPager);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    if (OnValidationWarning != null) ValidateObjectMapping(dbRes, typeof(T));
                    while (dbRes.Read())
                    {
                        result.Add(pCopyFunc.Invoke(dbRes));
                    }
                }
                if (pPager.CalculateTotals)
                {
                    using (IDbCommand rowCountCmd = cmd.Connection.CreateCommand())
                    {
                        ConfigureRowCountCommand(rowCountCmd, pPager);
                        pPager.TotalRows = (int)rowCountCmd.ExecuteScalar();
                        pPager.TotalPages = Convert.ToInt32(Math.Ceiling(pPager.TotalRows.Value * 1.0 / pPager.ResultMax));
                    }
                }
            });
            return result;
        }



        /// <summary>
        /// Execute the statement, returning the results as a list of strongly typed objects. Copies values using reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pStmt"></param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public IList<T> SelectObjectList<T>(IDbStatement pStmt) where T : new()
        {
            var copier = new DbRowToObject<T>();
            return SelectObjectList<T>(pStmt, copier.CreateObject);
        }


        /// <summary>
        /// Execute the statement, returning the results as a list of strongly typed objects. Copies values using the table enum TEnum, along with reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <typeparam name="TEnum">Table enum for translation</typeparam>
        /// <param name="pStmt"></param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public IList<T> SelectObjectList<T, TEnum>(IDbStatement pStmt)
            where T : new()
            where TEnum : struct
        {
            var copier = new DbRowToObject<T>();
            copier.SetDbTranslation<TEnum>();
            return SelectObjectList<T>(pStmt, copier.CreateObject);
        }


        /// <summary>
        /// Execute the pager, returning the results as a list of strongly typed objects. Copies values using reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pPager">Pager to execute. Includes statement and page information for paged results.</param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public IList<T> SelectObjectList<T>(DbSelectPager pPager) where T : new()
        {
            var copier = new DbRowToObject<T>();
            return SelectObjectList<T>(pPager, copier.CreateObject);
        }


        /// <summary>
        /// Execute the pager, returning the results as a list of strongly typed objects. Copies values using the table enum TEnum, along with reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <typeparam name="TEnum">Table enum for translation</typeparam>
        /// <param name="pPager">Pager to execute. Includes statement and page information for paged results.</param>
        /// <returns>An object of the specified type</returns>
        /// <remarks></remarks>
        public IList<T> SelectObjectList<T, TEnum>(DbSelectPager pPager)
            where T : new()
            where TEnum : struct
        {
            var copier = new DbRowToObject<T>();
            copier.SetDbTranslation<TEnum>();
            return SelectObjectList<T>(pPager, copier.CreateObject);
        }



        #endregion


        #region "Action"

        /// <summary>
        /// Executes the statement, executing an action on each row returned.
        /// </summary>
        /// <param name="pStmt"></param>
        /// <param name="pAction">Action to execute on each row</param>
        /// <remarks></remarks>
        public void SelectIntoAction(IDbStatement pStmt, Action<DbRow> pAction)
        {
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    while (dbRes.Read())
                    {
                        pAction.Invoke(dbRes);
                    }
                }
            });
        }


        /// <summary>
        /// Executes the statement, executing an action on the resultset returned.
        /// </summary>
        /// <param name="pStmt"></param>
        /// <param name="pAction">Action to execute</param>
        /// <remarks></remarks>
        public void SelectIntoAction(IDbStatement pStmt, Action<DbResultSet> pAction)
        {
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (DbResultSet dbRes = new DbResultSet(this, cmd.ExecuteReader()))
                {
                    pAction.Invoke(dbRes);
                }
            });
        }


        /// <summary>
        /// Executes the statement, executing an action on each object returned. Copies values using reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <param name="pStmt"></param>
        /// <param name="pAction">Action to execute on each object</param>
        /// <remarks></remarks>
        public void SelectObjectsIntoAction<T>(IDbStatement pStmt, Action<T> pAction) where T : new()
        {
            var copier = new DbRowToObject<T>();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    if (OnValidationWarning != null) ValidateObjectMapping(dbRes, typeof(T));
                    while (dbRes.Read())
                    {
                        pAction.Invoke(copier.CreateObject(dbRes));
                    }
                }
            });
        }


        /// <summary>
        /// Executes the statement, executing an action on each object returned. Copies values using the table enum TEnum, along with reflection of the referenced type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        /// <typeparam name="TEnum">Table enum for translation</typeparam>
        /// <param name="pStmt"></param>
        /// <param name="pAction">Action to execute on each object</param>
        /// <remarks></remarks>
        public void SelectObjectsIntoAction<T, TEnum>(IDbStatement pStmt, Action<T> pAction)
            where T : new()
            where TEnum : struct
        {
            var copier = new DbRowToObject<T>();
            copier.SetDbTranslation<TEnum>();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    if (OnValidationWarning != null) ValidateObjectMapping(dbRes, typeof(T));
                    while (dbRes.Read())
                    {
                        pAction.Invoke(copier.CreateObject(dbRes));
                    }
                }
            });
        }

        #endregion


        #region "Scalar"

        /// <summary>
        /// Execute the statement with a single return value. Casts result to the specified type. Throws an error if the type is incompatible.
        /// </summary>
        public T SelectScalar<T>(IDbStatement pStmt)
        {
            Type dstType = typeof(T);
            if (Nullable.GetUnderlyingType(dstType) != null) dstType = Nullable.GetUnderlyingType(dstType);

            T result = default(T);
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleRow)))
                {
                    if (dbRes.FieldCount != 1) throw new Exception(string.Format("Database.SelectScalar: Unexpected columns returned ({0})", dbRes.FieldCount));
                    if (OnValidationWarning != null)
                    {
                        if (dbRes.GetFieldType(0) != dstType)
                        {
                            RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.DifferentDataType, string.Format("Column '{0}' recordset type ({1}) does not match object type ({2})", "0", dbRes.GetFieldType(0).Name, dstType.Name));
                        }
                    }

                    if (dbRes.Read())
                    {
                        var data = dbRes._GetValue(0);
                        if (dbRes.Read()) throw new Exception("Database.SelectScalar: Multiple results returned");
                        if (data == null)
                            result = default(T);
                        else if (data.GetType().Equals(dstType))
                            result = (T)data;
                        else
                            result = (T)Convert.ChangeType(data, dstType);
                    }
                    else
                    {
                        throw new Exception("Database.SelectScalar: No results returned");
                    }
                }
            });

            return result;
        }

        /// <summary>
        /// Execute the statement with a single return column.
        /// </summary>
        public IList<T> SelectScalarList<T>(IDbStatement pStmt)
        {
            Type dstType = typeof(T);
            if (Nullable.GetUnderlyingType(dstType) != null) dstType = Nullable.GetUnderlyingType(dstType);

            var result = new List<T>();
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                using (var dbRes = new DbResultSet(this, cmd.ExecuteReader(CommandBehavior.SingleResult)))
                {
                    if (dbRes.FieldCount != 1) throw new Exception(string.Format("Database.SelectScalarList: Unexpected columns returned ({0})", dbRes.FieldCount));
                    if (OnValidationWarning != null)
                    {
                        if (dbRes.GetFieldType(0) != dstType)
                        {
                            RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.DifferentDataType, string.Format("Column '{0}' recordset type ({1}) does not match object type ({2})", "0", dbRes.GetFieldType(0).Name, dstType.Name));
                        }
                    }
                    while (dbRes.Read())
                    {
                        var data = dbRes._GetValue(0);
                        if (data == null)
                            result.Add(default(T));
                        else if (data.GetType().Equals(dstType))
                            result.Add((T)data);
                        else
                            result.Add((T)Convert.ChangeType(data, dstType));
                    }
                }
            });

            return result;
        }

        /// <summary>
        /// Execute the non-select statement, and then execute the select statement with a single return value.
        /// Used to get calculated fields for DBMS that does not support multi-statement.
        /// </summary>
        protected object ExecuteAndSelectScalar(IDbStatement pExStmt, IDbStatement pSelStmt)
        {
            Int32 rowsAffected;
            object result = null;
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pExStmt);
                rowsAffected = cmd.ExecuteNonQuery();

                //repurpose the cmd
                ConfigureCommand(cmd, pSelStmt);
                result = cmd.ExecuteScalar();
                if (object.ReferenceEquals(result, DBNull.Value)) result = null;
            });
            return result;
        }

        #endregion


        #region "Execute"

        /// <summary>
        /// Execute the non-select statement. Returns the number of rows affected
        /// </summary>
        public Int32 Execute(IDbStatement pStmt)
        {
            Int32 result = 0;
            HandleCommand((cmd) =>
            {
                ConfigureCommand(cmd, pStmt);
                result = cmd.ExecuteNonQuery();
            });
            return result;
        }

        #endregion




        #region "Utility"


        /// <summary>Get summary of parameters for events</summary>
        protected void GetParameterSummary(IDbCommand pCmd, out string poSummary, out IDictionary<string, object> poList)
        {
            poList = new Dictionary<string, object>();
            System.Text.StringBuilder summ = new System.Text.StringBuilder();
            foreach (IDataParameter param in pCmd.Parameters)
            {
                poList.Add(param.ParameterName, object.ReferenceEquals(param.Value, DBNull.Value) ? null : param.Value);
                summ.AppendFormat("{0} : {1}", param.ParameterName, param.Value);
                summ.AppendLine();
            }
            poSummary = summ.ToString();
        }

        /// <summary>Validate a result against an object schema</summary>
        protected void ValidateObjectMapping(DbRow pDbRes, Type pCompareType)
        {
            var clrFieldList = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
            foreach (var prop in pCompareType.GetProperties())
            {
                if (prop.CanWrite)
                {
                    var nm = prop.Name;
                    if (clrFieldList.ContainsKey(nm))
                    {
                        RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.DuplicateClrField, string.Format("Duplicate property name '{0}' on '{1}'", nm, pCompareType.FullName));
                        continue;
                    }
                    var typ = prop.PropertyType;
                    if (Nullable.GetUnderlyingType(typ) != null) typ = Nullable.GetUnderlyingType(typ);
                    clrFieldList.Add(nm, typ);
                }
            }
            ValidateMapping(pDbRes, clrFieldList);
        }

        /// <summary>Validate a result against a DataTable schema</summary>
        protected void ValidateDataTableMapping(DbRow pDbRes, DataTable pTable)
        {
            var clrFieldList = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
            foreach (DataColumn col in pTable.Columns)
            {
                var nm = col.ColumnName;
                if (clrFieldList.ContainsKey(nm))
                {
                    RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.DuplicateClrField, string.Format("Duplicate column name '{0}' in table", nm));
                    continue;
                }
                var typ = col.DataType;
                if (Nullable.GetUnderlyingType(typ) != null) typ = Nullable.GetUnderlyingType(typ);
                clrFieldList.Add(nm, typ);
            }
            ValidateMapping(pDbRes, clrFieldList);
        }

        /// <summary>Validate a result against a DataTable schema</summary>
        protected void ValidateMapping(DbRow pDbRes, Dictionary<string, Type> pClrFieldList)
        {
            var dbFieldList = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
            for (int colIdx = 0; colIdx < pDbRes.FieldCount; colIdx++)
            {
                var nm = pDbRes.GetName(colIdx);
                if (dbFieldList.ContainsKey(nm))
                {
                    RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.DuplicateDbField, string.Format("Duplicate column name '{0}' in recordset", nm));
                    continue;
                }
                var typ = pDbRes.GetFieldType(colIdx);
                if (Nullable.GetUnderlyingType(typ) != null) typ = Nullable.GetUnderlyingType(typ);
                dbFieldList.Add(nm, typ);
            }

            var inBoth = dbFieldList.Keys.Intersect(pClrFieldList.Keys, StringComparer.OrdinalIgnoreCase).ToList();

            foreach (var fldName in inBoth)
            {
                if (dbFieldList[fldName] != pClrFieldList[fldName])
                {
                    RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.DifferentDataType, string.Format("Column '{0}' recordset type ({1}) does not match object type ({2})", fldName, dbFieldList[fldName].Name, pClrFieldList[fldName].Name));
                }
            }

            foreach (var fldName in dbFieldList.Keys.Except(inBoth, StringComparer.OrdinalIgnoreCase))
            {
                RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.MissingDbField, string.Format("Column for property '{0}' not found in recordset", fldName));
            }

            foreach (var fldName in pClrFieldList.Keys.Except(inBoth, StringComparer.OrdinalIgnoreCase))
            {
                RaiseValidationWarning(ValidationWarningEventArgs.eWarningType.MissingClrField, string.Format("Property for column '{0}' not found on object", fldName));
            }
        }


        /// <summary></summary>
        protected void RaiseValidationWarning(ValidationWarningEventArgs.eWarningType pWarningType, string pMessage)
        {
            if (OnValidationWarning != null)
            {
                OnValidationWarning.Invoke(this, new ValidationWarningEventArgs { WarningType = pWarningType, Summary = pMessage });
            }
        }


        /// <summary>
        /// Class to create an object from a DbRow using reflection. Should only be used for one query, as it caches the field list.
        /// </summary>
        protected class DbRowToObject<T> where T : new()
        {

            private class PropMap
            {
                public Type PropType;
                public string SqlName;
                public System.Reflection.PropertyInfo Prop;
            }

            //check field list against property list and save results to speed up subsequent runs
            private IEnumerable<PropMap> _propMapList = null;
            private IDictionary<string, string> _clrToSqlTranslation = null;
            private void SetupPropMapList(DbRow pRes)
            {
                var pmlist = new List<PropMap>();
                var typ = typeof(T);
                var flds = pRes.GetNames();

                foreach (var prop in typ.GetProperties())
                {
                    var sqlName = prop.Name;
                    if ((_clrToSqlTranslation != null) && _clrToSqlTranslation.ContainsKey(prop.Name)) sqlName = _clrToSqlTranslation[prop.Name];
                    if (prop.CanWrite && flds.Contains(sqlName, StringComparer.OrdinalIgnoreCase))
                    {
                        var pm = new PropMap();
                        pm.SqlName = sqlName;
                        pm.Prop = prop;
                        if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                            pm.PropType = Nullable.GetUnderlyingType(prop.PropertyType);
                        else
                            pm.PropType = prop.PropertyType;

                        pmlist.Add(pm);
                    }
                }
                _propMapList = pmlist;
            }

            /// <summary></summary>
            public void SetDbTranslation<TEnum>() where TEnum : struct
            {
                _clrToSqlTranslation = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                foreach (var item in TableDefinitionAttributes.GetColumnAttributes<TEnum>().Values)
                {
                    if (!_clrToSqlTranslation.ContainsKey(item.ClrName)) _clrToSqlTranslation.Add(item.ClrName, item.SqlName);
                }
            }

            /// <summary></summary>
            public T CreateObject(DbRow pRes)
            {
                if (_propMapList == null) SetupPropMapList(pRes);
                T result = new T();
                foreach (var pmap in _propMapList)
                {
                    object data = pRes.GetValue(pmap.SqlName);
                    if ((data != null) && !data.GetType().Equals(pmap.PropType))
                    {
                        data = Convert.ChangeType(data, pmap.PropType);
                    }
                    pmap.Prop.SetValue(result, data, null);
                }

                return result;
            }
        }


        #endregion


    }

}

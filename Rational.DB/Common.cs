﻿//  Copyright Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace Rational.DB
{

    /// <summary>
    /// Enum representing the supported database types
    /// </summary>
    public enum eDbType
    {
        /// <summary>Database type is unknown</summary>
        Unknown=0,

        /// <summary>Microsoft SQL Server</summary>
        SqlServer = 10,
        
        /// <summary>Microsoft SQL Server CE</summary>
        SqlServerCe = 20,
        
        /// <summary>Oracle</summary>
        Oracle = 30,
        
        /// <summary>MySQL</summary>
        MySql = 40
    }

    /// <summary>
    /// Class storing the information required to connect to a database.
    /// </summary>
    public class DbConnectionInfo
    {

        /// <summary>Connection string for database</summary>
        public string ConnectionString { get; set; }

        /// <summary>Type of database</summary>
        public eDbType DbType { get; set; }

        /// <summary>.NET provider class, if different from default for DbType</summary>
        public string Provider { get; set; }

        static DbConnectionInfo()
        {
            InitProviders();
        }

        /// <summary></summary>
        protected static Dictionary<string, eDbType> _providerToType = new Dictionary<string, eDbType>(StringComparer.OrdinalIgnoreCase);

        /// <summary></summary>
        protected static List<string> _providers = new List<string>();

        /// <summary></summary>
        protected static void InitProviders()
        {
            _providerToType.Add("System.Data.SqlClient", eDbType.SqlServer);
            _providerToType.Add("System.Data.SqlServerCe.4.0", eDbType.SqlServerCe);
            _providerToType.Add("System.Data.SqlServerCe.3.5", eDbType.SqlServerCe);
            _providerToType.Add("Oracle.ManagedDataAccess.Client", eDbType.Oracle);
            _providerToType.Add("Oracle.DataAccess.Client", eDbType.Oracle);
            _providerToType.Add("System.Data.OracleClient", eDbType.Oracle);
            _providerToType.Add("MySql.Data.MySqlClient", eDbType.MySql);
            //todo: add more

            try
            {
                //try to add the registered providers
                var dt = DbProviderFactories.GetFactoryClasses();
                foreach (DataRow row in dt.Rows)
                {
                    _providers.Add(row["InvariantName"].ToString());
                }
            }
            catch (Exception)
            {
                //ignore error, but might cause unsupported databases
            }
        }

        /// <summary></summary>
        public DbConnectionInfo(string pConnectionString, eDbType pDbType)
	    {
		    ConnectionString = pConnectionString;
		    DbType = pDbType;
            Provider = (from p in _providerToType where p.Value == pDbType && _providers.Contains(p.Key) select p.Key).FirstOrDefault();
            if (string.IsNullOrEmpty(Provider)) throw new Exception(string.Format("DbType not supported, or missing provider for type '{0}'", pDbType));
        }

        /// <summary></summary>
        public DbConnectionInfo(string pConnectionString, eDbType pDbType, string pProvider)
        {
            ConnectionString = pConnectionString;
            DbType = pDbType;
            Provider = pProvider;
        }

        /// <summary></summary>
        public DbConnectionInfo(string pConnectionString, string pProvider)
        {
            ConnectionString = pConnectionString;
            if (_providerToType.ContainsKey(pProvider))
                DbType = _providerToType[pProvider];
            else
                throw new Exception("DbType could not be determined");
            Provider = pProvider;
        }

        /// <summary></summary>
        protected DbProviderFactory _factory;
        /// <summary></summary>
        public DbProviderFactory GetFactory()
        {
            if (_factory == null) _factory = DbProviderFactories.GetFactory(Provider);
            return _factory;
        }
        
    }

    /// <summary>
    /// Attribute for an Enum representing a database table definition
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum)]
    public class TableAttribute : Attribute
    {
        /// <summary>Name of the table in the database</summary>
        public string SqlName { get; set; }

        /// <summary>Automatically convert table and column names to upper case</summary>
        public bool UppercaseIdentifiers { get; set; }
    }

    /// <summary>
    /// Attribute for an Enum value representing a database column definition
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class ColumnAttribute : Attribute
    {
        /// <summary>Name of the column in the database</summary>
        public string SqlName { get; set; }

        /// <summary>Name of the column in code</summary>
        public string ClrName { get; set; }

        /// <summary>Datatype of the column in the database. String value of a member of the System.Data.DbType enum</summary>
        public string SqlType
        {
            get { return SqlTypeEnum.ToString(); }
            set 
            { 
                if (!string.IsNullOrEmpty(value))
                {
                    DbType valEnum;
                    if (System.Enum.TryParse(value, out valEnum) == false) throw new ArgumentException("SqlType");
                    SqlTypeEnum = valEnum;
                }
                else
                {
                    SqlTypeEnum = null;
                }
            }
        }
       


        /// <summary>Datatype of the column in the database</summary>
        public DbType? SqlTypeEnum { get; set; }

        /// <summary>Whether column is not nullable in the database</summary>
        public bool NotNull { get; set; }
        
        /// <summary>Whether column participates in the primary key of the table</summary>
        public bool PrimaryKey { get; set; }
        
        /// <summary>Whether column value is computed by the database</summary>
        public bool Computed { get; set; }

        /// <summary>Whether column is an auto increment key field</summary>
        public bool AutoIncrement { get; set; }

    }

}

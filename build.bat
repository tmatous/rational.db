@echo off
set config=Debug
set destdir=.\Build\
echo Building %config% ...

rem set msbuild_exe="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe"
rem set msbuild_exe="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe"
rem %msbuild_exe% /t:Clean Rational.DB.sln
rem %msbuild_exe% /t:Build /p:Configuration=%config% Rational.DB.sln
dotnet build Rational.DB.sln
if ERRORLEVEL 1 goto Error

echo Copying files to the %destdir% directory ...
md %destdir%
robocopy /S Rational.DB\bin\%config%\ %destdir% *.dll
robocopy /S Rational.DB\bin\%config%\ %destdir% *.xml
robocopy /S Rational.DB\bin\%config%\ %destdir% *.pdb

echo Done.
pause

goto End
:Error
echo Error!
pause
:End
